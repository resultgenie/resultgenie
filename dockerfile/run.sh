#!/bin/bash

VOLUME_HOME="/var/lib/mysql"

if [[ ! -d $VOLUME_HOME/mysql ]]; then
    echo "=> An empty or uninitialized MySQL volume is detected in $VOLUME_HOME"
    echo "=> Installing MySQL ..."
    mysqld --initialize-insecure --user=mysql > /dev/null 2>&1
    echo "=> Done!"
    service mysql start
    mysql -u root < /home/rg/all.sql
    mysql_upgrade -uroot
    # Fix Group Issue
    mysql -u root -e "SET GLOBAL sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''));"
    # Set Passowrd
    mysqladmin -u root password root
    # Run Backend
    cd /tmp && nohup java -jar /home/rg/ResultGenie.jar &
    # Run Front End
    cd /home/rg/ui && node purifycss && node_modules/.bin/webpack --progress --profile --colors && webpack-dev-server --inline --hot --content-base ./build/public/ --host 0.0.0.0 --port $PORT
    while true; do sleep 1000; done
#    /create_mysql_admin_user.sh
else
    echo "=> Using an existing volume of MySQL"
fi

#exec supervisord -n