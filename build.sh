#!/bin/bash
git pull
kill -9 $(ps aux | grep [R]esultGenie.jar | awk '{print $2}')
mvn clean install
cd target
nohup java -jar ResultGenie.jar | tee build.out
