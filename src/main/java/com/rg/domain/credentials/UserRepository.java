package com.rg.domain.credentials;

import org.springframework.data.jpa.repository.JpaRepository;


public interface UserRepository extends JpaRepository<User,Integer>{

	User findByUsernameAndPassword( String userName, String password );


    User findByUsername(String username);

    User findByEmailId(String emailId);

    User findByEmailIdAndTenant(String emailId, Tenant tenant);
}
