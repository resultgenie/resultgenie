package com.rg.domain.credentials;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by shreeshan on 09/04/17.
 */
public interface RoleRepository extends JpaRepository<Role, Integer> {
    Role findByRole(String role);
}
