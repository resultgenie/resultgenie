package com.rg.domain.credentials;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by shreeshan on 02/04/17.
 */
public interface BranchesRepository extends JpaRepository<Branches,Integer> {
    Branches findByBranchAbbreviation(String s);
}
