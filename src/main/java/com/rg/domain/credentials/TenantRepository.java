package com.rg.domain.credentials;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;


@Transactional
public interface TenantRepository extends CrudRepository<Tenant, String>{
	

	Tenant getById(String id);

	Tenant findByUsername(String tenantName);

	Tenant findByLoginUsernameAndLoginPassword(String loginUsername, String loginPassword);

	Tenant findByLoginUsername(String loginUsername);

    Tenant findByKey(String key);
}
