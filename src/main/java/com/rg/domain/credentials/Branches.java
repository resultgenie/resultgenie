package com.rg.domain.credentials;

import javax.persistence.*;

/**
 * Created by shreeshan on 02/04/17.
 */
@Entity
@Table(name = "branches")
public class Branches {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "branch_id")
    private Integer branchId;

    @Column(name = "branch_fullname")
    private String branchFullform;

    @Column(name = "branch_abb")
    private String branchAbbreviation;

    public Integer getBranchId() {
        return branchId;
    }

    public void setBranchId(Integer branchId) {
        this.branchId = branchId;
    }

    public String getBranchFullform() {
        return branchFullform;
    }

    public void setBranchFullform(String branchFullform) {
        this.branchFullform = branchFullform;
    }

    public String getBranchAbbreviation() {
        return branchAbbreviation;
    }

    public void setBranchAbbreviation(String branchAbbreviation) {
        this.branchAbbreviation = branchAbbreviation;
    }
}
