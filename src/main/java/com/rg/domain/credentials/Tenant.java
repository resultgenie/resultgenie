package com.rg.domain.credentials;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tenant")
public class Tenant {

    @Id
    @Column(name = "t_id")
    private String id;

    @Column(name = "t_url")
    private String url;

    @Column(name = "t_username")
    private String username;

    @Column(name = "t_password")
    private String password;

    @Column(name = "t_login_username")
    private String loginUsername;

    @Column(name = "t_login_password")
    private String loginPassword;

    @Column(name = "t_key")
    private String key;


    public Tenant(String id, String url, String username, String password, String loginUsername, String loginPassword,String key) {
        super();
        this.id = id;
        this.url = url;
        this.username = username;
        this.password = password;
        this.loginUsername = loginUsername;
        this.loginPassword = loginPassword;
        this.key = key;
    }

    public Tenant() {
    }


    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getLoginUsername() {
        return loginUsername;
    }

    public void setLoginUsername(String loginUsername) {
        this.loginUsername = loginUsername;
    }

    public String getLoginPassword() {
        return loginPassword;
    }

    public void setLoginPassword(String loginPassword) {
        this.loginPassword = loginPassword;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
