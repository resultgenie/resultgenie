package com.rg.domain.data;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
@Table(name = "theory_practical")
public class TheoryPractical {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "tp_id")
    private Integer theoryPracticalId;

    @Column(name = "tp_internal_score")
    private Integer internalScore;

    @Column(name = "tp_external_score")
    private Integer externalScore;

    @Column(name = "tp_total_score")
    private Integer totalScore;

    @Column(name = "tp_is_practical")
    private Boolean isPractical;


    @Column(name = "tp_result_class")
    private String result;

    @ManyToOne
    @JoinColumn(name = "tp_frn_sr_id", nullable = false, referencedColumnName = "sr_id")
    @JsonIgnore
    private StudentResult studentResult;

    @OneToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "tp_frn_s_id")
    private Subject subject;

    @OneToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "tp_frn_sm_id")
    @JsonIgnore
    private Semester semester;

    @OneToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "tp_frn_b_id")
    @JsonIgnore
    private Branch branch;

    public Integer getTotalScore() {
        return totalScore;
    }

    public void setTotalScore(Integer totalScore) {
        this.totalScore = totalScore;
    }

    public Semester getSemester() {
        return semester;
    }

    public void setSemester(Semester semester) {
        this.semester = semester;
    }

    public Branch getBranch() {
        return branch;
    }

    public void setBranch(Branch branch) {
        this.branch = branch;
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    public Integer getTheoryPracticalId() {
        return theoryPracticalId;
    }

    public void setTheoryPracticalId(Integer theoryPracticalId) {
        this.theoryPracticalId = theoryPracticalId;
    }

    public Integer getInternalScore() {
        return internalScore;
    }

    public void setInternalScore(Integer internalScore) {
        this.internalScore = internalScore;
    }

    public Integer getExternalScore() {
        return externalScore;
    }

    public void setExternalScore(Integer externalScore) {
        this.externalScore = externalScore;
    }

    public Boolean getIsPractical() {
        return isPractical;
    }

    public void setIsPractical(Boolean isPractical) {
        this.isPractical = isPractical;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public Boolean getPractical() {
        return isPractical;
    }

    public void setPractical(Boolean practical) {
        isPractical = practical;
    }

    public StudentResult getStudentResult() {
        return studentResult;
    }

    public void setStudentResult(StudentResult studentResult) {
        this.studentResult = studentResult;
    }
}
