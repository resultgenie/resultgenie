package com.rg.domain.data;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "student")
public class Student {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "st_id")
    private Integer studentId;

    @Column(name = "st_student_usn")
    private String studentUSN;

    @Column(name = "st_student_name")
    private String studentName;

    @Column(name = "st_joining_year")
    private String joiningYear;

    @Column(name = "st_is_active")
    private Integer isActive;

    @ManyToOne
    @JoinColumn(name = "st_frn_c_id")
    @JsonIgnore
    private College college;

    @ManyToOne
    @JoinColumn(name = "st_frn_b_id")
    @JsonIgnore
    private Branch branch;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "sr_frn_st_id")
    private List<StudentResult> studentResult = new ArrayList<>();

    public Integer getIsActive() {
        return isActive;
    }

    public void setIsActive(Integer isActive) {
        this.isActive = isActive;
    }

    public String getJoiningYear() {
        return joiningYear;
    }

    public void setJoiningYear(String joiningYear) {
        this.joiningYear = joiningYear;
    }

    public Integer getStudentId() {
        return studentId;
    }

    public void setStudentId(Integer studentId) {
        this.studentId = studentId;
    }

    public String getStudentUSN() {
        return studentUSN;
    }

    public void setStudentUSN(String studentUSN) {
        this.studentUSN = studentUSN;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public College getCollege() {
        return college;
    }

    public void setCollege(College college) {
        this.college = college;
    }

    public Branch getBranch() {
        return branch;
    }

    public void setBranch(Branch branch) {
        this.branch = branch;
    }

    public List<StudentResult> getStudentResult() {
        return studentResult;
    }

    public void setStudentResult(List<StudentResult> studentResult) {
        this.studentResult = studentResult;
    }

}
