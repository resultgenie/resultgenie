package com.rg.domain.data;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by shreeshan on 01/09/17.
 */
public interface SubjectWiseRankingRepository extends JpaRepository<SubjectWiseRanking,Integer>{
    SubjectWiseRanking findBySubjectCodeAndBranch(String subjectCode, String branchAbbreviation);
}
