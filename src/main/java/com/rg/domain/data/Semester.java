package com.rg.domain.data;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "semester")
public class Semester {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "sm_id")
	private Integer semesterId;

	@Column(name = "sm_semester_value")
	private String semesterValue;

	@OneToMany
	@JoinColumn(name = "s_frn_sm_id")
	@JsonIgnore
	private List<Subject> subject = new ArrayList<>();

	@ManyToMany(mappedBy="semester")
	@JsonIgnore
	private List<YearData> yearData = new ArrayList<>();

	public List<YearData> getYearData() {
		return yearData;
	}

	public void setYearData(List<YearData> yearData) {
		this.yearData = yearData;
	}

	public List<Subject> getSubject() {
		return subject;
	}

	public void setSubject(List<Subject> subject) {
		this.subject = subject;
	}

	public Integer getSemesterId() {
		return semesterId;
	}

	public void setSemesterId(Integer semesterId) {
		this.semesterId = semesterId;
	}

	public String getSemesterValue() {
		return semesterValue;
	}

	public void setSemesterValue(String semesterValue) {
		this.semesterValue = semesterValue;
	}

}
