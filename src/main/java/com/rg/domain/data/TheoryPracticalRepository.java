package com.rg.domain.data;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface TheoryPracticalRepository extends JpaRepository<TheoryPractical, Integer> {

    @Query(value = "select tp_external_score,st.st_student_name from theory_practical as tp inner join subject as s " +
            "on s.s_id= tp.tp_frn_s_id inner join student_result as sr on tp.tp_frn_sr_id=sr.sr_id inner join student" +
            " as st on st.st_id=sr.sr_frn_st_id where s.s_subject_code=?1 order by tp_external_score desc limit 1",
            nativeQuery = true)
    Object[][] getMaxMarksForSubject(String subjectCode);

    @Query(value = "select tp_external_score,st.st_student_name from theory_practical as tp inner join subject as s " +
            "on s.s_id= tp.tp_frn_s_id inner join student_result as sr on tp.tp_frn_sr_id=sr.sr_id inner join student" +
            " as st on st.st_id=sr.sr_frn_st_id where s.s_subject_code=?1 order by tp_external_score asc limit 1",
            nativeQuery = true)
    Object[][] getMinMarksForSubject(String subjectCode);

    @Query(value = "select avg(tp.tp_external_score),avg(tp.tp_internal_score) from theory_practical as tp inner join" +
            " subject as s on s.s_id= tp.tp_frn_s_id where s.s_subject_code=?1", nativeQuery = true)
    Object[] getAvgExternalAndInternalForSubject(String subjectCode);



    @Query(value = "SELECT\n" +
            "  Avg(results.marks) AS marks,\n" +
            "\n" +
            "  max_marks\n" +
            "FROM (SELECT\n" +
            "        (tp_total_score)                              AS marks,\n" +
            "        (s_external_max_marks + s_internal_max_marks) AS max_marks,\n" +
            "        tp_id,\n" +
            "        s.s_subject_code\n" +
            "      FROM theory_practical tp\n" +
            "        INNER JOIN student_result sr\n" +
            "          ON tp.tp_frn_sr_id = sr.sr_id\n" +
            "        INNER JOIN subject s\n" +
            "          ON s.s_id = tp.tp_frn_s_id\n" +
            "        INNER JOIN branch b ON b.b_id = tp.tp_frn_b_id\n" +
            "      WHERE sr.sr_result_month = (SELECT DISTINCT (sr_result_month)\n" +
            "                                  FROM student_result sr\n" +
            "                                    INNER JOIN theory_practical tp\n" +
            "                                      ON tp.tp_frn_sr_id =\n" +
            "                                         sr.sr_id\n" +
            "                                    INNER JOIN subject s\n" +
            "                                      ON s.s_id = tp.tp_frn_s_id\n" +
            "                                    INNER JOIN branch b ON b.b_id = tp.tp_frn_b_id\n" +
            "                                  WHERE s_subject_code = ?1 AND b.b_branch_abbreviation = ?2\n" +
            "                                  ORDER BY sr_result_month DESC\n" +
            "                                  LIMIT 1)\n" +
            "            AND s_subject_code = ?1 AND b_branch_abbreviation = ?2\n" +
            "      GROUP BY tp_id) AS results"
            , nativeQuery = true)
    @Transactional
    List<Object[]> getCurrentSemsAvgSubjectScore(String subjectCode, String branchAbb);


    @Query(value = "SELECT\n" +
            "  Avg(results.marks) AS marks,\n" +
            "\n" +
            "  max_marks\n" +
            "FROM (SELECT\n" +
            "        (tp_total_score)                              AS marks,\n" +
            "        (s_external_max_marks + s_internal_max_marks) AS max_marks,\n" +
            "        tp_id,\n" +
            "        s.s_subject_code\n" +
            "      FROM theory_practical tp\n" +
            "        INNER JOIN student_result sr\n" +
            "          ON tp.tp_frn_sr_id = sr.sr_id\n" +
            "        INNER JOIN subject s\n" +
            "          ON s.s_id = tp.tp_frn_s_id\n" +
            "        INNER JOIN branch b ON b.b_id = tp.tp_frn_b_id\n" +
            "      WHERE sr.sr_result_month = (SELECT DISTINCT (sr_result_month)\n" +
            "                                  FROM student_result sr\n" +
            "                                    INNER JOIN theory_practical tp\n" +
            "                                      ON tp.tp_frn_sr_id =\n" +
            "                                         sr.sr_id\n" +
            "                                    INNER JOIN subject s\n" +
            "                                      ON s.s_id = tp.tp_frn_s_id\n" +
            "                                    INNER JOIN branch b ON b.b_id = tp.tp_frn_b_id\n" +
            "                                  WHERE s_subject_code = ?1 AND b.b_branch_abbreviation = ?2\n" +
            "                                  ORDER BY sr_result_month DESC\n" +
            "                                  LIMIT 1,1)\n" +
            "            AND s_subject_code = ?1 AND b_branch_abbreviation = ?2\n" +
            "      GROUP BY tp_id) AS results"
            , nativeQuery = true)
    List<Object[]> getPreviousSemSubjectAvgScore(String subjectCode, String branchAbb);

    @Transactional
    @Query(value = "SELECT\n" +
            "  final_res.rank,\n" +
            "  final_res.avg_score,\n" +
            "  final_res.total_marks\n" +
            "FROM (\n" +
            "       SELECT\n" +
            "         @s \\:= @s + 1 AS rank,\n" +
            "         results.avg_score,\n" +
            "         results.total_marks,\n" +
            "         results.s_subject_code,\n" +
            "         results.s_subject_name,\n" +
            "         results.b_branch_abbreviation\n" +
            "       FROM\n" +
            "         (SELECT\n" +
            "            avg(tp.tp_external_score + tp.tp_internal_score)  AS avg_score,\n" +
            "            (s.s_external_max_marks + s.s_internal_max_marks) AS total_marks,\n" +
            "            s.s_subject_code,\n" +
            "            s.s_subject_name,\n" +
            "            b.b_branch_abbreviation\n" +
            "          FROM subject s INNER JOIN theory_practical tp ON tp.tp_frn_s_id = s.s_id\n" +
            "            INNER JOIN branch b ON tp.tp_frn_b_id = b.b_id\n" +
            "            INNER JOIN student_result sr ON sr.sr_id = tp.tp_frn_sr_id\n" +
            "            ,\n" +
            "            (SELECT @s \\:= 0) AS r\n" +
            "          WHERE s.s_is_regular_subject = 1\n" +
            "          GROUP BY s_id\n" +
            "          ORDER BY avg_score DESC) AS results) AS final_res\n" +
            "WHERE s_subject_code = ?1 AND b_branch_abbreviation = ?2", nativeQuery = true)
    List<Object[]> getOverallSubjectRanking(String subjectCode, String branchAbb);

    @Transactional
    @Query(value = "SELECT\n" +
            "  final_res.rank,\n" +
            "  final_res.avg_score,\n" +
            "  final_res.total_marks\n" +
            "FROM (\n" +
            "       SELECT\n" +
            "         @s \\:= @s + 1 AS rank,\n" +
            "         results.avg_score,\n" +
            "         results.total_marks,\n" +
            "         results.s_subject_code,\n" +
            "         results.s_subject_name,\n" +
            "         results.b_branch_abbreviation\n" +
            "       FROM\n" +
            "         (SELECT\n" +
            "            avg(tp.tp_external_score + tp.tp_internal_score)  AS avg_score,\n" +
            "            (s.s_external_max_marks + s.s_internal_max_marks) AS total_marks,\n" +
            "            s.s_subject_code,\n" +
            "            s.s_subject_name,\n" +
            "            b.b_branch_abbreviation\n" +
            "          FROM subject s INNER JOIN theory_practical tp ON tp.tp_frn_s_id = s.s_id\n" +
            "            INNER JOIN branch b ON tp.tp_frn_b_id = b.b_id\n" +
            "            INNER JOIN student_result sr ON sr.sr_id = tp.tp_frn_sr_id\n" +
            "            ,\n" +
            "            (SELECT @s \\:= 0) AS r\n" +
            "          WHERE s.s_is_regular_subject = 1\n" +
            "AND\n" +
            "                sr_result_month != (SELECT DISTINCT sr_result_month\n" +
            "                                    FROM student_result sr\n" +
            "                                      INNER JOIN theory_practical tp ON tp.tp_frn_sr_id = sr.sr_id\n" +
            "                                      INNER JOIN subject s ON s.s_id = tp.tp_frn_s_id\n" +
            "                                      INNER JOIN branch b ON b.b_id = tp.tp_frn_b_id\n" +
            "                                    WHERE s.s_subject_code = ?1 AND b.b_branch_abbreviation = ?2" +
            "                                    ORDER BY sr_result_month DESC\n" +
            "                                    LIMIT 1)" +
            "          GROUP BY s_id\n" +
            "          ORDER BY avg_score DESC) AS results) AS final_res\n" +
            "WHERE s_subject_code = ?1 AND b_branch_abbreviation = ?2", nativeQuery = true)
    List<Object[]> getOverallSubjectRankingButPreviousResult(String subjectCode, String branchAbb);

    @Query(value = "SELECT count(*)\n" +
            "            FROM theory_practical tp\n" +
            "              INNER JOIN student_result sr\n" +
            "                ON tp.tp_frn_sr_id = sr.sr_id\n" +
            "              INNER JOIN subject s\n" +
            "                ON s.s_id = tp.tp_frn_s_id\n" +
            "              INNER JOIN branch b\n" +
            "                ON b.b_id = tp.tp_frn_b_id\n" +
            "              inner join student st on st.st_id=sr.sr_frn_st_id\n" +
            "            WHERE\n" +
            "              s.s_subject_code = ?1\n" +
            "              AND\n" +
            "              tp_result_class = 'FAIL' AND b_branch_abbreviation = ?2 and st_is_active=1\n"
            , nativeQuery = true)
    @Transactional
    Integer subjectBacklogCount(String subjectCode, String branchAbb);

    @Query(value = "SELECT count(*)\n" +
            "            FROM theory_practical tp\n" +
            "              INNER JOIN student_result sr\n" +
            "                ON tp.tp_frn_sr_id = sr.sr_id\n" +
            "              INNER JOIN subject s\n" +
            "                ON s.s_id = tp.tp_frn_s_id\n" +
            "              INNER JOIN branch b\n" +
            "                ON b.b_id = tp.tp_frn_b_id\n" +
            "              inner join student st on st.st_id=sr.sr_frn_st_id\n" +
            "            WHERE\n" +
            "              s.s_subject_code = ?1\n" +
            "              AND\n" +
            "              tp_result_class = 'FAIL' AND b_branch_abbreviation = ?2 and st_is_active=1\n" +
            "and                 sr_result_month != (SELECT DISTINCT sr_result_month\n" +
            "                                                FROM student_result sr\n" +
            "                                                  INNER JOIN theory_practical tp ON tp.tp_frn_sr_id = sr" +
            ".sr_id\n" +
            "                                                  INNER JOIN subject s ON s.s_id = tp.tp_frn_s_id\n" +
            "                                                  INNER JOIN branch b ON b.b_id = tp.tp_frn_b_id\n" +
            "                                                WHERE s.s_subject_code = ?1 AND b" +
            ".b_branch_abbreviation = ?2\n" +
            "                                                ORDER BY sr_result_month DESC\n" +
            "                                                LIMIT 1)", nativeQuery = true)
    @Transactional
    Integer getPreviousSemSubjectBacklogCount(String subjectCode, String branchAbb);


    @Query(value = "SELECT * \n" +
            "FROM   theory_practical tp \n" +
            "       INNER JOIN student_result sr \n" +
            "               ON sr.sr_id = tp.tp_frn_sr_id \n" +
            "       INNER JOIN student st \n" +
            "               ON st.st_id = sr.sr_frn_st_id \n" +
            "WHERE  tp_result_class = ?1 \n" +
            "       AND st_student_usn = ?2",
            nativeQuery = true)
    List<TheoryPractical> getBacklogSubjects(String fail, String usn);


    @Transactional
    @Query(value = "select count(*) from theory_practical tp inner join student_result sr on sr.sr_id=tp" +
            ".tp_frn_sr_id\n" +
            "inner join student st on st.st_id=sr.sr_frn_st_id\n" +
            "where st_student_usn=?1 and tp_result_class='FAIL'", nativeQuery = true)
    Integer getStudentActiveBacklogs(String usn);
}
