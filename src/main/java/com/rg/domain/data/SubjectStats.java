package com.rg.domain.data;

import javax.persistence.*;

/**
 * Created by shreeshan on 31/05/17.
 */
@Entity
@Table(name = "subject_stats")
public class SubjectStats {

    @Id
    @Column(name = "sbs_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer subjectStatsId;

    @OneToOne
    @JoinColumn(name = "sbs_frn_s_id")
    private Subject subject;

    @Column(name = "sbs_highest_marks")
    private Double highestMarks;

    @OneToOne
    @JoinColumn(name = "sbs_frn_st_id_highest_scorer")
    private Student highestScorer;

    @Column(name = "sbs_change_in_highest_marks")
    private Double changeInHighestMarks;

    @Column(name = "sbs_least_marks")
    private Double leastMarks;

    @OneToOne
    @JoinColumn(name = "sbs_frn_st_id_least_scorer")
    private Student leastScorer;

    @OneToOne
    @JoinColumn(name = "sbs_frn_b_id")
    private Branch branch;

    @Column(name = "sbs_change_in_least_marks")
    private Double changeInLeastScore;

    public Branch getBranch() {
        return branch;
    }

    public void setBranch(Branch branch) {
        this.branch = branch;
    }

    public Integer getSubjectStatsId() {
        return subjectStatsId;
    }

    public void setSubjectStatsId(Integer subjectStatsId) {
        this.subjectStatsId = subjectStatsId;
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    public Double getHighestMarks() {
        return highestMarks;
    }

    public void setHighestMarks(Double highestMarks) {
        this.highestMarks = highestMarks;
    }

    public Student getHighestScorer() {
        return highestScorer;
    }

    public void setHighestScorer(Student highestScorer) {
        this.highestScorer = highestScorer;
    }

    public Double getChangeInHighestMarks() {
        return changeInHighestMarks;
    }

    public void setChangeInHighestMarks(Double changeInHighestMarks) {
        this.changeInHighestMarks = changeInHighestMarks;
    }

    public Double getLeastMarks() {
        return leastMarks;
    }

    public void setLeastMarks(Double leastMarks) {
        this.leastMarks = leastMarks;
    }

    public Student getLeastScorer() {
        return leastScorer;
    }

    public void setLeastScorer(Student leastScorer) {
        this.leastScorer = leastScorer;
    }

    public Double getChangeInLeastScore() {
        return changeInLeastScore;
    }

    public void setChangeInLeastScore(Double changeInLeastScore) {
        this.changeInLeastScore = changeInLeastScore;
    }

    //    private Integer branchRank;
//
//    private Integer semesterRank;
//
//    private Integer collegeRank;
//
//    private Integer subjectBacklogCount;
//
//    private float subjectBacklogCountChnage;
//
//    private Double avgInternalScore;
//
//    private Double avgExternalScore;
//


}
