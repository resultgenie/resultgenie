package com.rg.domain.data;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CollegeRepository extends JpaRepository<College,Integer>{

	College findByCollegeCode( String substring );

    College findByCollegeKey(String key);
}
