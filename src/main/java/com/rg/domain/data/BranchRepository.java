package com.rg.domain.data;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface BranchRepository extends JpaRepository<Branch,Integer>{

	Branch findByBranchValue( String substring );

	@Query(value="select b.b_branch_value from branch as b where b.b_branch_value like %?1% or b.b_branch_abbreviation like %?1%",nativeQuery = true)
	List<Object>[] getBranchesForString(String query);

    Branch findByBranchAbbreviation(String branchAbbreviation);


}
