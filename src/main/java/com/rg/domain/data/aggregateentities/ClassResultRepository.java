package com.rg.domain.data.aggregateentities;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by shreeshan on 01/07/17.
 */
public interface ClassResultRepository extends JpaRepository<ClassResult,Integer> {

    @Query(value="SELECT\n" +
            "  @s \\:= @s + 1      AS cs_id,\n" +
            "  st_student_name   AS cs_student_name,\n" +
            "  st_student_usn    AS cs_student_usn,\n" +
            "  s_subject_name    AS cs_subject_name,\n" +
            "  s_subject_code    AS cs_subject_code,\n" +
            "  tp_external_score AS cs_external_score,\n" +
            "  sr_sem_aggregate  AS cs_sem_aggregate,\n" +
            "  sr_result_class   AS cs_student_result_class,\n" +
            "  sr_total_score    AS cs_sem_total,\n" +
            "  tp_internal_score AS cs_internal_score,\n" +
            "  tp_total_score    AS cs_total_score,\n" +
            "  tp_is_practical   AS cs_is_practical,\n" +
            "  tp_result_class   AS cs_subject_result_class\n" +
            "FROM student st\n" +
            "  INNER JOIN student_result sr\n" +
            "    ON\n" +
            "      sr.sr_frn_st_id = st.st_id\n" +
            "  INNER JOIN theory_practical tp\n" +
            "    ON\n" +
            "      tp.tp_frn_sr_id = sr.sr_id\n" +
            "  INNER JOIN semester sm\n" +
            "    ON\n" +
            "      sm.sm_id = sr.sr_frn_sm_id\n" +
            "  INNER JOIN branch b\n" +
            "    ON\n" +
            "      b.b_id = tp.tp_frn_b_id\n" +
            "  INNER JOIN subject s\n" +
            "    ON\n" +
            "      s.s_id = tp.tp_frn_s_id\n" +
            "  , (SELECT @s \\:= 0) AS ss\n" +
            "WHERE\n" +
            "  sm_semester_value = ?3\n" +
            "  AND\n" +
            "  b_branch_abbreviation = ?2\n" +
            "  AND\n" +
            "  st_joining_year = ?1\n" +
            "ORDER BY cs_sem_aggregate DESC",nativeQuery = true)
    @Transactional
    List<ClassResult> getClassResults(String year, String branchAbb, String semesterValue);
}
