package com.rg.domain.data.aggregateentities;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * Created by shreeshan on 25/05/17.
 */
public interface BranchwiseResultRepository extends JpaRepository<BranchwiseResult,Integer> {

    @Query(value = "SELECT @s \\:= @s + 1          bw_id, \n" +
            "       sr.sr_frn_sm_id       AS bw_sem_id, \n" +
            "       st_frn_b_id           AS bw_branch_id, \n" +
            "       b_branch_abbreviation AS bw_branch_abb, \n" +
            "       Avg(sr_sem_aggregate) AS bw_avg_result, \n" +
            "       sr.sr_result_month    AS bw_result_month \n"+
            "FROM   student_result sr \n" +
            "       INNER JOIN student st \n" +
            "               ON sr.sr_frn_st_id = st.st_id \n" +
            "       INNER JOIN branch b \n" +
            "               ON b.b_id = st.st_frn_b_id , \n" +
            "       (SELECT @s \\:= 0) AS s \n" +
            "GROUP  BY sr.sr_frn_sm_id, \n" +
            "          st_frn_b_id, \n" +
            "          sr.sr_result_month ",nativeQuery = true)
    List<BranchwiseResult> getBranchWiseResults();
}
