package com.rg.domain.data.aggregateentities;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by shreeshan on 01/09/17.
 */
public interface SubjectWiseRankingEntityRepository extends JpaRepository<SubjectWiseRankingEntity,Integer> {

//    @Query(value = "SELECT @s \\:= @s + 1 as swre_rank,\n" +
//            "                 res.s_subject_code as swre_subject_code,\n" +
//            "                 res.s_subject_name as swre_subject_name,\n" +
//            "                 res.sub_performance as swre_subject_performance,\n" +
//            "                 subjects.total_count as swre_total_count,\n" +
//            "                 res.b_branch_abbreviation as swre_branch\n" +
//            "   FROM\n" +
//            "     (SELECT Count(DISTINCT s_subject_code) AS total_count\n" +
//            "      FROM subject s\n" +
//            "      INNER JOIN theory_practical tp ON tp.tp_frn_s_id = s.s_id\n" +
//            "      INNER JOIN student_result sr ON sr.sr_id = tp.tp_frn_sr_id\n" +
//            "      INNER JOIN student st ON st.st_id = sr.sr_frn_st_id\n" +
//            "      WHERE s_is_regular_subject = 1\n" +
//            "        AND st_is_active = 1 ) AS subjects,\n" +
//            "\n" +
//            "     (SELECT s.s_subject_code,\n" +
//            "             s.s_subject_name,\n" +
//            "             Avg(tp.tp_external_score +tp.tp_internal_score) AS sub_performance,\n" +
//            "             b_branch_abbreviation\n" +
//            "      FROM subject s\n" +
//            "      INNER JOIN theory_practical tp ON tp.tp_frn_s_id = s.s_id\n" +
//            "      INNER JOIN student_result sr ON sr.sr_id = tp.tp_frn_sr_id\n" +
//            "      INNER JOIN student st ON st.st_id = sr.sr_frn_st_id\n" +
//            "      INNER JOIN branch b ON b.b_id = tp.tp_frn_b_id\n" +
//            "      INNER JOIN\n" +
//            "        (SELECT Max(sr_result_month) AS sr_result_month,\n" +
//            "                b_id,\n" +
//            "                sr_id,\n" +
//            "                sr_frn_sm_id,\n" +
//            "                s_subject_code,\n" +
//            "                s_subject_name\n" +
//            "         FROM student_result sr\n" +
//            "         INNER JOIN theory_practical tp ON sr.sr_id = tp.tp_frn_sr_id\n" +
//            "         INNER JOIN student AS st ON st.st_id = sr.sr_frn_st_id\n" +
//            "         INNER JOIN branch AS b ON b.b_id = tp.tp_frn_b_id\n" +
//            "         INNER JOIN subject AS s ON s.s_id = tp.tp_frn_s_id\n" +
//            "         GROUP BY s_subject_code,b_branch_abbreviation\n" +
//            "         ORDER BY b_id) AS res_months ON sr.sr_result_month = res_months.sr_result_month\n" +
//            "      AND sr.sr_frn_sm_id = res_months.sr_frn_sm_id\n" +
//            "      AND b.b_id = res_months.b_id\n" +
//            "      WHERE s_is_regular_subject = 1\n" +
//            "        AND st_is_active = 1\n" +
//            "      GROUP BY s_subject_code\n" +
//            "      ORDER BY sub_performance DESC) AS res,\n" +
//            "     (SELECT @s \\:= 0) AS s", nativeQuery = true)
    @Query(value="SELECT \n" +
            "    *, @s\\:=@s + 1 AS swre_rank\n" +
            "FROM\n" +
            "    (SELECT \n" +
            "        s_subject_code AS swre_subject_code,\n" +
            "            s_subject_name AS swre_subject_name,\n" +
            "            AVG(tp_total_score) AS swre_subject_performance,\n" +
            "            b_branch_abbreviation AS swre_branch,\n" +
            "            subjects.total_count AS swre_total_count\n" +
            "    FROM\n" +
            "        (SELECT \n" +
            "        COUNT(*) AS total_count\n" +
            "    FROM\n" +
            "        subject s\n" +
            "    WHERE\n" +
            "        s_is_regular_subject = 1) AS subjects, theory_practical tp\n" +
            "    INNER JOIN student_result sr ON sr.sr_id = tp.tp_frn_sr_id\n" +
            "    INNER JOIN subject s ON s.s_id = tp.tp_frn_s_id\n" +
            "    INNER JOIN branch b ON b.b_id = tp.tp_frn_b_id, (SELECT @s\\:=0) AS s\n" +
            "    WHERE\n" +
            "        s.s_is_regular_subject = 1\n" +
            "    GROUP BY s_id\n" +
            "    ORDER BY swre_subject_performance DESC) AS res",nativeQuery = true)
    @Transactional
    List<SubjectWiseRankingEntity> getSubjectRankingWrtCollege();


//    @Query(value = "SELECT \n" +
//            "    @s\\:=@s + 1 AS swre_rank,\n" +
//            "    res.s_subject_code AS swre_subject_code,\n" +
//            "    res.s_subject_name AS swre_subject_name,\n" +
//            "    res.sub_performance AS swre_subject_performance,\n" +
//            "    subjects.total_count AS swre_total_count,\n" +
//            "    res.b_branch_abbreviation AS swre_branch\n" +
//            "FROM\n" +
//            "    (SELECT \n" +
//            "        COUNT(DISTINCT s_subject_code) AS total_count\n" +
//            "    FROM\n" +
//            "        subject s\n" +
//            "    INNER JOIN theory_practical tp ON tp.tp_frn_s_id = s.s_id\n" +
//            "    INNER JOIN student_result sr ON sr.sr_id = tp.tp_frn_sr_id\n" +
//            "    INNER JOIN student st ON st.st_id = sr.sr_frn_st_id\n" +
//            "    INNER JOIN semester sm ON sm.sm_id = sr.sr_frn_sm_id\n" +
//            "    WHERE\n" +
//            "        s_is_regular_subject = 1\n" +
//            "            AND CASE\n" +
//            "            WHEN sm.sm_semester_value != 8 THEN st_is_active = 1\n" +
//            "            ELSE st_is_active = 0\n" +
//            "        END\n" +
//            "            AND sm.sm_semester_value = ?1) AS subjects,\n" +
//            "    (SELECT \n" +
//            "        s.s_subject_code,\n" +
//            "            s.s_subject_name,\n" +
//            "            AVG(tp.tp_external_score + tp.tp_internal_score) AS sub_performance,\n" +
//            "            s_frn_sm_id,\n" +
//            "            s_frn_b_id,\n" +
//            "            b.b_branch_abbreviation\n" +
//            "    FROM\n" +
//            "        subject s\n" +
//            "    INNER JOIN theory_practical tp ON tp.tp_frn_s_id = s.s_id\n" +
//            "    INNER JOIN student_result sr ON sr.sr_id = tp.tp_frn_sr_id\n" +
//            "    INNER JOIN student st ON st.st_id = sr.sr_frn_st_id\n" +
//            "    INNER JOIN branch AS b ON b.b_id = tp.tp_frn_b_id\n" +
//            "    INNER JOIN semester sm ON sm.sm_id = sr.sr_frn_sm_id\n" +
//            "    INNER JOIN (SELECT \n" +
//            "        MAX(sr_result_month) AS sr_result_month,\n" +
//            "            b_id,\n" +
//            "            sr_id,\n" +
//            "            sr_frn_sm_id,\n" +
//            "            s_subject_code,\n" +
//            "            s_subject_name\n" +
//            "    FROM\n" +
//            "        student_result sr\n" +
//            "    INNER JOIN theory_practical tp ON sr.sr_id = tp.tp_frn_sr_id\n" +
//            "    INNER JOIN student AS st ON st.st_id = sr.sr_frn_st_id\n" +
//            "    INNER JOIN branch AS b ON b.b_id = tp.tp_frn_b_id\n" +
//            "    INNER JOIN subject AS s ON s.s_id = tp.tp_frn_s_id\n" +
//            "    GROUP BY s_subject_code,b_branch_abbreviation\n" +
//            "    ORDER BY b_id) AS res_months ON sr.sr_result_month = res_months.sr_result_month\n" +
//            "        AND sr.sr_frn_sm_id = res_months.sr_frn_sm_id\n" +
//            "        AND b.b_id = res_months.b_id\n" +
//            "    WHERE\n" +
//            "        s_is_regular_subject = 1\n" +
//            "            AND CASE\n" +
//            "            WHEN sm.sm_semester_value != 8 THEN st_is_active = 1\n" +
//            "            ELSE st_is_active = 0\n" +
//            "        END\n" +
//            "            AND sm.sm_semester_value = ?1\n" +
//            "    GROUP BY s_subject_code\n" +
//            "    ORDER BY sub_performance DESC) AS res,\n" +
//            "    (SELECT @s\\:=0) AS s\n", nativeQuery =
//            true)
    @Query(value="SELECT \n" +
            "    *, @s\\:=@s + 1 AS swre_rank\n" +
            "FROM\n" +
            "    (SELECT \n" +
            "        s_subject_code AS swre_subject_code,\n" +
            "            s_subject_name AS swre_subject_name,\n" +
            "            AVG(tp_total_score) AS swre_subject_performance,\n" +
            "            b_branch_abbreviation AS swre_branch,\n" +
            "            subjects.total_count AS swre_total_count\n" +
            "    FROM\n" +
            "        (SELECT \n" +
            "        COUNT(*) AS total_count\n" +
            "    FROM\n" +
            "        subject s\n" +
            "        INNER JOIN semester sm on sm.sm_id=s.s_frn_sm_id\n" +
            "    WHERE\n" +
            "        s_is_regular_subject = 1 and sm_semester_value=?1) AS subjects, theory_practical tp\n" +
            "    INNER JOIN student_result sr ON sr.sr_id = tp.tp_frn_sr_id\n" +
            "    INNER JOIN subject s ON s.s_id = tp.tp_frn_s_id\n" +
            "    INNER JOIN semester sm on sm.sm_id=s.s_frn_sm_id\n" +
            "    INNER JOIN branch b ON b.b_id = tp.tp_frn_b_id, (SELECT @s\\:=0) AS s\n" +
            "    WHERE\n" +
            "        s.s_is_regular_subject = 1 and sm_semester_value=?1\n" +
            "    GROUP BY s_id\n" +
            "    ORDER BY swre_subject_performance DESC) AS res",nativeQuery = true)
    @Transactional
    List<SubjectWiseRankingEntity> getSubjectRankingWrtSemester(String semesterValue);


//    @Query(value = "SELECT @s \\:= @s +1 AS swre_rank,\n" +
//            "              res.s_subject_code AS swre_subject_code,\n" +
//            "              res.s_subject_name AS swre_subject_name,\n" +
//            "              res.sub_performance AS swre_subject_performance,\n" +
//            "              subjects.total_count AS swre_total_count,\n" +
//            "              res.b_branch_abbreviation AS swre_branch\n" +
//            "FROM\n" +
//            "  (SELECT Count(DISTINCT s_subject_code) AS total_count\n" +
//            "   FROM subject s\n" +
//            "   INNER JOIN theory_practical tp ON tp.tp_frn_s_id = s.s_id\n" +
//            "   INNER JOIN student_result sr ON sr.sr_id = tp.tp_frn_sr_id\n" +
//            "   INNER JOIN student st ON st.st_id = sr.sr_frn_st_id\n" +
//            "   INNER JOIN branch b ON b.b_id=tp.tp_frn_b_id\n" +
//            "   WHERE s_is_regular_subject = 1\n" +
//            "     AND st_is_active = 1\n" +
//            "     AND b_branch_abbreviation=?1) AS subjects,\n" +
//            "\n" +
//            "  (SELECT s.s_subject_code,\n" +
//            "          s.s_subject_name,\n" +
//            "          Avg(tp.tp_external_score+tp.tp_internal_score) AS sub_performance,\n" +
//            "          b.b_branch_abbreviation\n" +
//            "   FROM subject s\n" +
//            "   INNER JOIN theory_practical tp ON tp.tp_frn_s_id = s.s_id\n" +
//            "   INNER JOIN student_result sr ON sr.sr_id = tp.tp_frn_sr_id\n" +
//            "   INNER JOIN student st ON st.st_id = sr.sr_frn_st_id\n" +
//            "   INNER JOIN branch b ON b.b_id=tp.tp_frn_b_id\n" +
//            "   INNER JOIN\n" +
//            "     (SELECT Max(sr_result_month) AS sr_result_month,\n" +
//            "             b_id,\n" +
//            "             sr_id,\n" +
//            "             sr_frn_sm_id,\n" +
//            "             s_subject_code,\n" +
//            "             s_subject_name,\n" +
//            "             b_branch_abbreviation\n" +
//            "      FROM student_result sr\n" +
//            "      INNER JOIN theory_practical tp ON sr.sr_id = tp.tp_frn_sr_id\n" +
//            "      INNER JOIN student AS st ON st.st_id = sr.sr_frn_st_id\n" +
//            "      INNER JOIN branch AS b ON b.b_id = tp.tp_frn_b_id\n" +
//            "      INNER JOIN subject AS s ON s.s_id = tp.tp_frn_s_id\n" +
//            "      GROUP BY s_subject_code\n" +
//            "      ORDER BY b_id) AS res_months ON sr.sr_result_month = res_months.sr_result_month\n" +
//            "   AND sr.sr_frn_sm_id = res_months.sr_frn_sm_id\n" +
//            "   AND b.b_id = res_months.b_id\n" +
//            "   WHERE s_is_regular_subject = 1\n" +
//            "     AND st_is_active = 1\n" +
//            "     AND res_months.b_branch_abbreviation=?1\n" +
//            "   GROUP BY s_subject_code\n" +
//            "   ORDER BY sub_performance DESC) AS res,\n" +
//            "\n" +
//            "  (SELECT @s \\:= 0) AS s", nativeQuery = true)

    @Query(value="SELECT \n" +
            "    *, @s\\:=@s + 1 AS swre_rank\n" +
            "FROM\n" +
            "    (SELECT \n" +
            "        s_subject_code AS swre_subject_code,\n" +
            "            s_subject_name AS swre_subject_name,\n" +
            "            AVG(tp_total_score) AS swre_subject_performance,\n" +
            "            b_branch_abbreviation AS swre_branch,\n" +
            "            subjects.total_count AS swre_total_count\n" +
            "    FROM\n" +
            "        (SELECT \n" +
            "        COUNT(*) AS total_count\n" +
            "    FROM\n" +
            "        subject s\n" +
            "        INNER JOIN branch b on b.b_id=s.s_frn_b_id\n" +
            "    WHERE\n" +
            "        s_is_regular_subject = 1  and b_branch_abbreviation=?1) AS subjects, theory_practical tp\n" +
            "    INNER JOIN student_result sr ON sr.sr_id = tp.tp_frn_sr_id\n" +
            "    INNER JOIN subject s ON s.s_id = tp.tp_frn_s_id\n" +
            "    INNER JOIN branch b ON b.b_id = tp.tp_frn_b_id, (SELECT @s\\:=0) AS s\n" +
            "    WHERE\n" +
            "        s.s_is_regular_subject = 1 and b_branch_abbreviation=?1\n" +
            "    GROUP BY s_id\n" +
            "    ORDER BY swre_subject_performance DESC) AS res\n",nativeQuery = true)
    @Transactional
    List<SubjectWiseRankingEntity> getSubjectRankingWrtBranch(String branchAbb);

}
