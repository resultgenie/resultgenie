package com.rg.domain.data.aggregateentities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Created by shreeshan on 25/05/17.
 * This is an aggregate entity - meaning it is formed by combining two or more tables.
 * So a seperate table is not found in the db
 */
@Entity
public class BranchwiseResult {

    @Id
    @Column(name = "bw_id")
    @JsonIgnore
    private Integer branchWiseId;

    @Column(name = "bw_sem_id")
    private String semId;

    @Column(name = "bw_branch_id")
    private Integer branchId;

    @Column(name = "bw_branch_abb")
    private String branchAbb;

    @Column(name = "bw_avg_result")
    private Double avgResult;

    @Column(name = "bw_result_month")
    private Long resultMonthTimestamp;

    public Integer getBranchWiseId() {
        return branchWiseId;
    }

    public void setBranchWiseId(Integer branchWiseId) {
        this.branchWiseId = branchWiseId;
    }

    public String getSemId() {
        return semId;
    }

    public void setSemId(String semId) {
        this.semId = semId;
    }

    public Integer getBranchId() {
        return branchId;
    }

    public void setBranchId(Integer branchId) {
        this.branchId = branchId;
    }

    public String getBranchAbb() {
        return branchAbb;
    }

    public void setBranchAbb(String branchAbb) {
        this.branchAbb = branchAbb;
    }

    public Double getAvgResult() {
        return avgResult;
    }

    public void setAvgResult(Double avgResult) {
        this.avgResult = avgResult;
    }

    public Long getResultMonthTimestamp() {
        return resultMonthTimestamp;
    }

    public void setResultMonthTimestamp(Long resultMonthTimestamp) {
        this.resultMonthTimestamp = resultMonthTimestamp;
    }
}
