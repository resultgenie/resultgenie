package com.rg.domain.data.aggregateentities;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by shreeshan on 04/07/17.
 */
public interface BranchWisePassPercentageRepository extends JpaRepository<BranchWisePassPercentage,Integer>{


    @Query(value="SELECT   @s \\:=@s+1 as bwpp_id,  sm.sm_semester_value as bwpp_sem_val, \n" +
            "           b_branch_abbreviation as bwpp_branch_abb, \n" +
            "           sum(sr_result_class!='FAIL')/count(*)*100 as bwpp_pass_percentage\n" +
            "FROM       student_result sr \n" +
            "INNER JOIN student st \n" +
            "ON         st.st_id=sr.sr_frn_st_id \n" +
            "INNER JOIN branch b \n" +
            "ON         st.st_frn_b_id=b.b_id \n" +
            "INNER JOIN semester sm on sm.sm_id=sr.sr_frn_sm_id \n"+
            "INNER JOIN \n" +
            "           ( \n" +
            "                  SELECT rank, \n" +
            "                         sr_frn_sm_id, \n" +
            "                         sr_result_month \n" +
            "                  FROM   ( \n" +
            "                                    SELECT     sr_frn_sm_id, \n" +
            "                                               sr_result_month, \n" +
            "                                               @rn \\:=IF(sr_frn_sm_id = @g, @rn + 1, 1) rank, \n" +
            "                                               @g \\:=                                    sr_frn_sm_id\n" +
            "                                    FROM       ( \n" +
            "                                                        SELECT   sr_frn_sm_id, \n" +
            "                                                                 sr_result_month \n" +
            "                                                        FROM     student_result \n" +
            "                                                        GROUP BY sr_frn_sm_id, \n" +
            "                                                                 sr_result_month DESC ) AS r \n" +
            "                                    CROSS JOIN \n" +
            "                                               ( \n" +
            "                                                      SELECT @rn \\:= 0, \n" +
            "                                                             @g \\:=  NULL ) AS i ) q \n" +
            "                  WHERE  rank = 1) AS ssss on ssss.sr_frn_sm_id=sr.sr_frn_sm_id and sr" +
            ".sr_result_month=ssss.sr_result_month,(SELECT @s\\:= 0) AS s group by sr.sr_frn_sm_id,b_branch_abbreviation\n",nativeQuery = true)
    @Transactional
    List<BranchWisePassPercentage> getBranchWisePassPercentages();

    @Query(value="SELECT \n" +
            "    @s\\:=@s + 1 AS bwpp_id,\n" +
            "    sm.sm_semester_value AS bwpp_sem_val,\n" +
            "    b_branch_abbreviation AS bwpp_branch_abb,\n" +
            "    SUM(sr_result_class != 'FAIL') / COUNT(*) * 100 AS bwpp_pass_percentage\n" +
            "FROM\n" +
            "    student_result sr\n" +
            "        INNER JOIN\n" +
            "    student st ON st.st_id = sr.sr_frn_st_id\n" +
            "        INNER JOIN\n" +
            "    branch b ON st.st_frn_b_id = b.b_id\n" +
            "        INNER JOIN\n" +
            "    semester sm ON sm.sm_id = sr.sr_frn_sm_id,\n" +
            "    (SELECT @s\\:=0) AS s\n" +
            "WHERE\n" +
            "    st_joining_year = ?2\n" +
            "        AND sm_semester_value = ?1\n" +
            "GROUP BY b_branch_abbreviation",nativeQuery = true)
    @Transactional
    List<BranchWisePassPercentage> getBranchWisePassPercentagesForSemAndBatch(Integer sem, Integer i);
}
