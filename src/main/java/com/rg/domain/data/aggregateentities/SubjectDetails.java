package com.rg.domain.data.aggregateentities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

/**
 * Created by shreeshan on 29/05/17.
 */
@Entity
public class SubjectDetails {

    @Id
    @Column(name = "sd_id")
    @JsonIgnore
    private Integer subjectDetailsId;

    @Column(name = "sd_subject_name")
    private String subjectName;

    @Column(name = "sd_subject_code")
    private String subjectCode;

    @Column(name = "sd_is_lab")
    private Integer isLab;

    @Column(name = "sd_branch")
    private String branch;

    @Column(name = "sd_semester")
    private String semester;

    @Column(name = "sd_avg_external_score")
    private Integer avgExternalScore;

    @Column(name = "sd_avg_internal_score")
    private Integer avgInternalScore;

    @OneToOne
    @Transient
    private StudentScores subjectTopper;

    @OneToOne
    @Transient
    private SubjectRanking subjectRanking;


    public String getSubjectCode() {
        return subjectCode;
    }

    public void setSubjectCode(String subjectCode) {
        this.subjectCode = subjectCode;
    }

    public Integer getSubjectDetailsId() {
        return subjectDetailsId;
    }

    public void setSubjectDetailsId(Integer subjectDetailsId) {
        this.subjectDetailsId = subjectDetailsId;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public Integer getIsLab() {
        return isLab;
    }

    public void setIsLab(Integer isLab) {
        this.isLab = isLab;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    public Integer getAvgExternalScore() {
        return avgExternalScore;
    }

    public void setAvgExternalScore(Integer avgExternalScore) {
        this.avgExternalScore = avgExternalScore;
    }

    public Integer getAvgInternalScore() {
        return avgInternalScore;
    }

    public void setAvgInternalScore(Integer avgInternalScore) {
        this.avgInternalScore = avgInternalScore;
    }

    public StudentScores getSubjectTopper() {
        return subjectTopper;
    }

    public void setSubjectTopper(StudentScores subjectTopper) {
        this.subjectTopper = subjectTopper;
    }

    public SubjectRanking getSubjectRanking() {
        return subjectRanking;
    }

    public void setSubjectRanking(SubjectRanking subjectRanking) {
        this.subjectRanking = subjectRanking;
    }
}
