package com.rg.domain.data.aggregateentities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Created by shreeshan on 09/06/17.
 */
@Entity
public class BranchWiseResultCategoryCount {

    @Id
    @Column(name = "bwrcc_id")
    @JsonIgnore
    private Integer branchWiseResultCategoryCountId;

    @Column(name = "bwrcc_branch_abb")
    private String branchAbbreviation;

    @Column(name = "bwrcc_branch_name")
    private String branchName;

    @Column(name = "bwrcc_sem_val")
    private String semesterValue;

    @Column(name = "bwrcc_fcd_count")
    private Integer firstClassDistinctionCount;

    @Column(name = "bwrcc_fc_count")
    private Integer firstClassCount;

    @Column(name = "bwrcc_sc_count")
    private Integer secondClassCount;

    @Column(name = "bwrcc_failures_count")
    private Integer failuresCount;

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getSemesterValue() {
        return semesterValue;
    }

    public void setSemesterValue(String semesterValue) {
        this.semesterValue = semesterValue;
    }

    public Integer getBranchWiseResultCategoryCountId() {
        return branchWiseResultCategoryCountId;
    }

    public void setBranchWiseResultCategoryCountId(Integer branchWiseResultCategoryCountId) {
        this.branchWiseResultCategoryCountId = branchWiseResultCategoryCountId;
    }

    public String getBranchAbbreviation() {
        return branchAbbreviation;
    }

    public void setBranchAbbreviation(String branchAbbreviation) {
        this.branchAbbreviation = branchAbbreviation;
    }

    public Integer getFirstClassDistinctionCount() {
        return firstClassDistinctionCount;
    }

    public void setFirstClassDistinctionCount(Integer firstClassDistinctionCount) {
        this.firstClassDistinctionCount = firstClassDistinctionCount;
    }

    public Integer getFirstClassCount() {
        return firstClassCount;
    }

    public void setFirstClassCount(Integer firstClassCount) {
        this.firstClassCount = firstClassCount;
    }

    public Integer getSecondClassCount() {
        return secondClassCount;
    }

    public void setSecondClassCount(Integer secondClassCount) {
        this.secondClassCount = secondClassCount;
    }

    public Integer getFailuresCount() {
        return failuresCount;
    }

    public void setFailuresCount(Integer failuresCount) {
        this.failuresCount = failuresCount;
    }
}
