package com.rg.domain.data.aggregateentities;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by shreeshan on 07/07/17.
 */
public interface YearSemesterBranchCombinationRepository extends JpaRepository<YearSemesterBranchCombination,Integer> {

    /**
     * Get available combination of years,semesters and branches
     * @return
     */
    @Query(value="SELECT @s\\:=@s+1 as ysb_id, st_joining_year as ysb_year, \n" +
            "       sm_semester_value as ysb_semester, \n" +
            "       b_branch_abbreviation as ysb_branch, \n" +
            "       b_branch_value as ysb_branch_fullform \n"+
            "FROM (select @s\\:=0) as x,  student st \n" +
            "       INNER JOIN branch b \n" +
            "               ON b.b_id = st.st_frn_b_id \n" +
            "       INNER JOIN student_result sr \n" +
            "               ON sr.sr_frn_st_id = st.st_id \n" +
            "       INNER JOIN semester sm \n" +
            "               ON sm.sm_id = sr.sr_frn_sm_id \n" +
            "GROUP  BY st_joining_year, \n" +
            "          sm_semester_value, \n" +
            "          b_branch_abbreviation ",nativeQuery = true)
    @Transactional
    List<YearSemesterBranchCombination> getCombinationOfAvailableData();
}
