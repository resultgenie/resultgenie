package com.rg.domain.data.aggregateentities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

/**
 * Created by shreeshan on 08/07/17.
 */
@Entity
public class StudentSemesterRank {

    @Id
    @Column(name = "ssr_id")
    @JsonIgnore
    private Integer ssrId;

    @Column(name = "ssr_rank")
    @Transient
    private Integer rank;

    @Column(name = "ssr_student_name")
    private String studentName;

    @Column(name = "ssr_student_usn")
    private String studentUsn;

    @Column(name = "ssr_total_marks")
    private Integer totalMarks;

    @Column(name = "ssr_aggregate")
    private Integer semAggregate;

    @Column(name = "ssr_result_class")
    private String resultClass;

    @Column(name = "ssr_branch")
    private String branch;

    @Column(name = "ssr_semester")
    @JsonIgnore
    private String semester;

    public Integer getRank() {
        return rank;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }

    public Integer getSemAggregate() {
        return semAggregate;
    }

    public void setSemAggregate(Integer semAggregate) {
        this.semAggregate = semAggregate;
    }

    public Integer getSsrId() {
        return ssrId;
    }

    public void setSsrId(Integer ssrId) {
        this.ssrId = ssrId;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getStudentUsn() {
        return studentUsn;
    }

    public void setStudentUsn(String studentUsn) {
        this.studentUsn = studentUsn;
    }

    public Integer getTotalMarks() {
        return totalMarks;
    }

    public void setTotalMarks(Integer totalMarks) {
        this.totalMarks = totalMarks;
    }

    public String getResultClass() {
        return resultClass;
    }

    public void setResultClass(String resultClass) {
        this.resultClass = resultClass;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }
}
