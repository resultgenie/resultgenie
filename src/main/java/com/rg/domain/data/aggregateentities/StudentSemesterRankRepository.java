package com.rg.domain.data.aggregateentities;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by shreeshan on 08/07/17.
 */
public interface StudentSemesterRankRepository extends JpaRepository<StudentSemesterRank, Integer> {

    @Query(value = "SELECT @s \\:= @s + 1      AS ssr_id, \n" +
            "       st_student_name   AS ssr_student_name, \n" +
            "       st_student_usn    AS ssr_student_usn, \n" +
            "       sm_semester_value AS ssr_semester, \n" +
            "       b_branch_value    AS ssr_branch, \n" +
            "       sr_sem_aggregate  AS ssr_aggregate, \n" +
            "       sr_total_score    AS ssr_total_marks, \n" +
            "       sr_result_class   AS ssr_result_class \n" +
            "FROM   (SELECT @s \\:= 0) AS var_s, \n" +
            "       student st \n" +
            "       INNER JOIN student_result sr \n" +
            "               ON st.st_id = sr.sr_frn_st_id \n" +
            "       INNER JOIN semester sm \n" +
            "               ON sm.sm_id = sr.sr_frn_sm_id \n" +
            "       INNER JOIN branch b \n" +
            "               ON b.b_id = st_frn_b_id \n" +
            "WHERE  sr_frn_sm_id = ?2 \n" +
            "       AND st_joining_year = ?1 \n" +
            "ORDER  BY sr_sem_aggregate DESC ", nativeQuery = true)
    @Transactional
    List<StudentSemesterRank> getStudentsWithRankAcrossSemesters(String year,String semester);
}


