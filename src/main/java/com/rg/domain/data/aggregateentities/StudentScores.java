package com.rg.domain.data.aggregateentities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by shreeshan on 29/05/17.
 */
@Entity
public class StudentScores implements Serializable {

    @Id
    @Column(name = "stsc_id")
    @JsonIgnore
    private Integer stscId;

    @Column(name = "stsc_student_name")
    private String studentName;

    @Column(name = "stsc_student_usn")
    private String studentUsn;

    @Column(name = "stsc_external_marks")
    private Integer externalMarks;

    @Column(name = "stsc_internal_marks")
    private Integer internalMarks;

    @Column(name = "stsc_total_marks")
    private Integer totalMarks;

    @Column(name = "stsc_result_class")
    private String resultClass;

    public String getResultClass() {
        return resultClass;
    }

    public void setResultClass(String resultClass) {
        this.resultClass = resultClass;
    }

    public Integer getStscId() {
        return stscId;
    }

    public void setStscId(Integer stscId) {
        this.stscId = stscId;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getStudentUsn() {
        return studentUsn;
    }

    public void setStudentUsn(String studentUsn) {
        this.studentUsn = studentUsn;
    }

    public Integer getExternalMarks() {
        return externalMarks;
    }

    public void setExternalMarks(Integer externalMarks) {
        this.externalMarks = externalMarks;
    }

    public Integer getInternalMarks() {
        return internalMarks;
    }

    public void setInternalMarks(Integer internalMarks) {
        this.internalMarks = internalMarks;
    }

    public Integer getTotalMarks() {
        return totalMarks;
    }

    public void setTotalMarks(Integer totalMarks) {
        this.totalMarks = totalMarks;
    }


}
