package com.rg.domain.data.aggregateentities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

/**
 * Created by shreeshan on 07/07/17.
 */
@Entity
public class YearSemesterBranchCombination {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ysb_id")
    @JsonIgnore
    private Integer yearSemesterBranchId;

    @Column(name = "ysb_year")
    private String year;

    @Column(name = "ysb_branch")
    private String branch;

    @Column(name = "ysb_branch_fullform")
    private String branchFullform;

    @Column(name = "ysb_semester")
    private String semester;

    public Integer getYearSemesterBranchId() {
        return yearSemesterBranchId;
    }

    public void setYearSemesterBranchId(Integer yearSemesterBranchId) {
        this.yearSemesterBranchId = yearSemesterBranchId;
    }

    public String getBranchFullform() {
        return branchFullform;
    }

    public void setBranchFullform(String branchFullform) {
        this.branchFullform = branchFullform;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }
}
