package com.rg.domain.data.aggregateentities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Created by shreeshan on 29/05/17.
 */
@Entity
public class SubjectRanking {

    @Id
    @Column(name = "sbr_id")
    @JsonIgnore
    private Integer subjectRankingId;

    @Column(name = "sbr_branch_rank")
    private Integer branchRank;

    @Column(name = "sbr_branch_max_subjects")
    private Integer branchMaxSubjects;

    @Column(name = "sbr_semester_rank")
    private Integer semesterRank;

    @Column(name = "sbr_semester_max_subjects")
    private Integer semesterMaxSubjects;

    @Column(name = "sbr_college_rank")
    private Integer collegeRank;

    @Column(name = "sbr_college_max_subjects")
    private Integer collegeMaxSubjects;


    public Integer getSubjectRankingId() {
        return subjectRankingId;
    }

    public void setSubjectRankingId(Integer subjectRankingId) {
        this.subjectRankingId = subjectRankingId;
    }

    public Integer getBranchRank() {
        return branchRank;
    }

    public void setBranchRank(Integer branchRank) {
        this.branchRank = branchRank;
    }

    public Integer getBranchMaxSubjects() {
        return branchMaxSubjects;
    }

    public void setBranchMaxSubjects(Integer branchMaxSubjects) {
        this.branchMaxSubjects = branchMaxSubjects;
    }

    public Integer getSemesterRank() {
        return semesterRank;
    }

    public void setSemesterRank(Integer semesterRank) {
        this.semesterRank = semesterRank;
    }

    public Integer getSemesterMaxSubjects() {
        return semesterMaxSubjects;
    }

    public void setSemesterMaxSubjects(Integer semesterMaxSubjects) {
        this.semesterMaxSubjects = semesterMaxSubjects;
    }

    public Integer getCollegeRank() {
        return collegeRank;
    }

    public void setCollegeRank(Integer collegeRank) {
        this.collegeRank = collegeRank;
    }

    public Integer getCollegeMaxSubjects() {
        return collegeMaxSubjects;
    }

    public void setCollegeMaxSubjects(Integer collegeMaxSubjects) {
        this.collegeMaxSubjects = collegeMaxSubjects;
    }
}
