package com.rg.domain.data.aggregateentities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

/**
 * Created by shreeshan on 18/07/17.
 */
@Entity
public class StudentPlacementBean {

    @Id
    @JsonIgnore
    @Column(name = "spb_id")
    private Integer spbId;

    @Column(name = "spb_rank")
    @Transient
    private Integer rank;

    @Column(name = "spb_student_name")
    private String studentName;

    @Column(name = "spb_student_usn")
    private String studentUsn;

    @Column(name = "spb_result_class")
    private String resultClass;

    @Column(name = "spb_semester")
    private String semester;

    @Column(name = "spb_branch")
    private String branch;

    @Column(name = "spb_aggregate")
    private Double aggregate;

    @Column(name = "spb_backlog")
    private Integer backlogs;

    public Integer getBacklogs() {
        return backlogs;
    }

    public void setBacklogs(Integer backlogs) {
        this.backlogs = backlogs;
    }

    public Integer getSpbId() {
        return spbId;
    }

    public void setSpbId(Integer spbId) {
        this.spbId = spbId;
    }

    public Integer getRank() {
        return rank;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getStudentUsn() {
        return studentUsn;
    }

    public void setStudentUsn(String studentUsn) {
        this.studentUsn = studentUsn;
    }

    public String getResultClass() {
        return resultClass;
    }

    public void setResultClass(String resultClass) {
        this.resultClass = resultClass;
    }

    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public Double getAggregate() {
        return aggregate;
    }

    public void setAggregate(Double aggregate) {
        this.aggregate = aggregate;
    }
}
