package com.rg.domain.data.aggregateentities;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by shreeshan on 09/06/17.
 */
public interface BranchWiseResultCategoryCountRepository extends JpaRepository<BranchWiseResultCategoryCount,Integer> {


    @Query(value="SELECT @ss \\:= @ss + 1          AS bwrcc_id, \n" +
            " \n" +
            "                   sm.sm_semester_value    AS bwrcc_sem_val,  \n" +
            "                   b.b_branch_abbreviation AS bwrcc_branch_abb,  \n" +
            "                   b.b_branch_value        AS bwrcc_branch_name,  \n" +
            "                   Sum(CASE  \n" +
            "                         WHEN sr.sr_result_class = 'FIRST CLASS WITH DISTINCTION'  \n" +
            "                               OR sr.sr_result_class = 'FCD' THEN 1  \n" +
            "                         ELSE 0  \n" +
            "                       end)                AS bwrcc_fcd_count,  \n" +
            "                   Sum(CASE  \n" +
            "                         WHEN sr.sr_result_class = 'FIRST CLASS' THEN 1  \n" +
            "                         ELSE 0  \n" +
            "                       end)                AS bwrcc_fc_count,  \n" +
            "                   Sum(CASE  \n" +
            "                         WHEN sr.sr_result_class = 'SECOND CLASS' THEN 1  \n" +
            "                         ELSE 0  \n" +
            "                       end)                AS bwrcc_sc_count,  \n" +
            "                   Sum(CASE  \n" +
            "                         WHEN sr.sr_result_class = 'FAIL' THEN 1  \n" +
            "                         ELSE 0  \n" +
            "                       end)                AS bwrcc_failures_count  \n" +
            "            FROM   student_result sr  \n" +
            "                   INNER JOIN student st  \n" +
            "                           ON st.st_id = sr.sr_frn_st_id  \n" +
            "                   INNER JOIN branch b  \n" +
            "                           ON b.b_id = st.st_frn_b_id  \n" +
            "                   INNER JOIN semester sm  \n" +
            "                           ON sm.sm_id = sr.sr_frn_sm_id  \n" +
            "                   INNER JOIN (SELECT Max(sr_result_month) AS sr_result_month1,  \n" +
            "                                      b_id,  \n" +
            "                                      sr_frn_sm_id,  \n" +
            "                                      sr_id  \n" +
            "                               FROM   student_result sr  \n" +
            "                                      INNER JOIN student st  \n" +
            "                                              ON st.st_id = sr.sr_frn_st_id  \n" +
            "                                      INNER JOIN branch b  \n" +
            "                                              ON b.b_id = st.st_frn_b_id  \n" +
            "                                      INNER JOIN semester sm  \n" +
            "                                              ON sm.sm_id = sr.sr_frn_sm_id,  \n" +
            "                                      (SELECT @ss \\:= 0) AS ss  \n" +
            "                               GROUP  BY b_id,  \n" +
            "                                         sr_frn_sm_id) AS date_months  \n" +
            "                           ON sr.sr_result_month = date_months.sr_result_month1  \n" +
            "                              AND sr.sr_frn_sm_id = date_months.sr_frn_sm_id  \n" +
            "                              AND b.b_id = date_months.b_id  \n" +
            "            GROUP  BY b.b_id,  \n" +
            "                      sr.sr_frn_sm_id\n",nativeQuery = true)
    List<BranchWiseResultCategoryCount> getBranchWiseResultCategoryCount();

    @Query(value="SELECT \n" +
            "    @ss\\:=@ss + 1 AS bwrcc_id,\n" +
            "    sm.sm_semester_value AS bwrcc_sem_val,\n" +
            "    b.b_branch_abbreviation AS bwrcc_branch_abb,\n" +
            "    b.b_branch_value AS bwrcc_branch_name,\n" +
            "    SUM(CASE\n" +
            "        WHEN\n" +
            "            sr.sr_result_class = 'FIRST CLASS WITH DISTINCTION'\n" +
            "                OR sr.sr_result_class = 'FCD'\n" +
            "        THEN\n" +
            "            1\n" +
            "        ELSE 0\n" +
            "    END) AS bwrcc_fcd_count,\n" +
            "    SUM(CASE\n" +
            "        WHEN sr.sr_result_class = 'FIRST CLASS' THEN 1\n" +
            "        ELSE 0\n" +
            "    END) AS bwrcc_fc_count,\n" +
            "    SUM(CASE\n" +
            "        WHEN sr.sr_result_class = 'SECOND CLASS' THEN 1\n" +
            "        ELSE 0\n" +
            "    END) AS bwrcc_sc_count,\n" +
            "    SUM(CASE\n" +
            "        WHEN sr.sr_result_class = 'FAIL' THEN 1\n" +
            "        ELSE 0\n" +
            "    END) AS bwrcc_failures_count\n" +
            "FROM\n" +
            "    student_result sr\n" +
            "        INNER JOIN\n" +
            "    student st ON st.st_id = sr.sr_frn_st_id\n" +
            "        INNER JOIN\n" +
            "    branch b ON b.b_id = st.st_frn_b_id\n" +
            "        INNER JOIN\n" +
            "    semester sm ON sm.sm_id = sr.sr_frn_sm_id,\n" +
            "    (SELECT @ss\\:=0) AS ss\n" +
            "WHERE\n" +
            "    st_joining_year = ?2\n" +
            "        AND sm_semester_value = ?1\n" +
            "GROUP BY b.b_id , sr.sr_frn_sm_id\n",nativeQuery = true)
    @Transactional
    List<BranchWiseResultCategoryCount> getBranchWiseResultCategoryCountForSemAndBatch(Integer sem, int i);
}
