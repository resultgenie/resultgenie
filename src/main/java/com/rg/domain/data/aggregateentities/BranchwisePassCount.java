package com.rg.domain.data.aggregateentities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Created by shreeshan on 28/05/17.
 * This is an aggregate entity - meaning it is formed by combining two or more tables.
 * So a separate table is not found in the db
 */
@Entity
public class BranchwisePassCount {

    @Id
    @Column(name = "bw_id")
    @JsonIgnore
    private Integer branchWiseId;

    @Column(name = "bw_sem_id")
    private Integer semId;

    @Column(name = "bw_branch_abb")
    private String branchAbb;

    @Column(name = "bw_pass_count")
    private Integer passCount;


    public Integer getBranchWiseId() {
        return branchWiseId;
    }

    public void setBranchWiseId(Integer branchWiseId) {
        this.branchWiseId = branchWiseId;
    }

    public Integer getSemId() {
        return semId;
    }

    public void setSemId(Integer semId) {
        this.semId = semId;
    }

    public String getBranchAbb() {
        return branchAbb;
    }

    public void setBranchAbb(String branchAbb) {
        this.branchAbb = branchAbb;
    }

    public Integer getPassCount() {
        return passCount;
    }

    public void setPassCount(Integer passCount) {
        this.passCount = passCount;
    }


}
