package com.rg.domain.data.aggregateentities;

/**
 * Created by shreeshan on 01/07/17.
 */

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Created by shreeshan on 28/05/17.
 * This is an aggregate entity - meaning it is formed by combining two or more tables.
 * So a separate table is not found in the db
 */
@Entity
public class ClassResult {


    @Id
    @Column(name = "cs_id")
    @JsonIgnore
    private Integer classResultId;


    @Column(name = "cs_student_name")
    private String studentName;

    @Column(name = "cs_student_usn")
    private String studentUsn;

    @Column(name = "cs_subject_name")
    private String subjectName;

    @Column(name = "cs_subject_code")
    private String subjectCode;

    @Column(name = "cs_external_score")
    private Integer externalMarks;

    @Column(name = "cs_internal_score")
    private Integer internalMarks;

    @Column(name = "cs_subject_result_class")
    private String subjectResultClass;

    @Column(name = "cs_total_score")
    private Integer totalMarks;

    @Column(name = "cs_is_practical")
    private Integer isPractical;

    @Column(name = "cs_sem_aggregate")
    private Double studentAggregate;

    @Column(name = "cs_student_result_class")
    private String studentResultClass;

    @Column(name = "cs_sem_total")
    private Integer studentSemTotal;

    public String getSubjectResultClass() {
        return subjectResultClass;
    }

    public void setSubjectResultClass(String subjectResultClass) {
        this.subjectResultClass = subjectResultClass;
    }

    public Double getStudentAggregate() {
        return studentAggregate;
    }

    public void setStudentAggregate(Double studentAggregate) {
        this.studentAggregate = studentAggregate;
    }

    public String getStudentResultClass() {
        return studentResultClass;
    }

    public void setStudentResultClass(String studentResultClass) {
        this.studentResultClass = studentResultClass;
    }

    public Integer getStudentSemTotal() {
        return studentSemTotal;
    }

    public void setStudentSemTotal(Integer studentSemTotal) {
        this.studentSemTotal = studentSemTotal;
    }

    public Integer getClassResultId() {
        return classResultId;
    }

    public void setClassResultId(Integer classResultId) {
        this.classResultId = classResultId;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getStudentUsn() {
        return studentUsn;
    }

    public void setStudentUsn(String studentUsn) {
        this.studentUsn = studentUsn;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getSubjectCode() {
        return subjectCode;
    }

    public void setSubjectCode(String subjectCode) {
        this.subjectCode = subjectCode;
    }

    public Integer getExternalMarks() {
        return externalMarks;
    }

    public void setExternalMarks(Integer externalMarks) {
        this.externalMarks = externalMarks;
    }

    public Integer getInternalMarks() {
        return internalMarks;
    }

    public void setInternalMarks(Integer internalMarks) {
        this.internalMarks = internalMarks;
    }

    public Integer getTotalMarks() {
        return totalMarks;
    }

    public void setTotalMarks(Integer totalMarks) {
        this.totalMarks = totalMarks;
    }

    public Integer getIsPractical() {
        return isPractical;
    }

    public void setIsPractical(Integer isPractical) {
        this.isPractical = isPractical;
    }
}
