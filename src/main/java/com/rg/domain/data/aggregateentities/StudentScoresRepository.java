package com.rg.domain.data.aggregateentities;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by shreeshan on 30/05/17.
 */
public interface StudentScoresRepository extends JpaRepository<StudentScores, Integer> {

    @Transactional
    @Query(value = "SELECT 1 AS stsc_id, \n" +
            " \n" +
            "                   st.st_student_name                    AS stsc_student_name,  \n" +
            "                   st.st_student_usn                     AS stsc_student_usn,  \n" +
            "                   tp.tp_external_score                  AS stsc_external_marks,  \n" +
            "                   tp.tp_internal_score                  AS stsc_internal_marks,  \n" +
            "                   tp_external_score + tp_internal_score AS stsc_total_marks,  \n" +
            "                   tp_result_class                       AS stsc_result_class \n" +
            "            FROM   theory_practical AS tp  \n" +
            "                   INNER JOIN subject AS s  \n" +
            "                           ON s.s_id = tp.tp_frn_s_id  \n" +
            "                   INNER JOIN student_result AS sr  \n" +
            "                           ON sr.sr_id = tp.tp_frn_sr_id  \n" +
            "                   INNER JOIN student AS st  \n" +
            "                           ON st.st_id = sr.sr_frn_st_id  \n" +
            "                   INNER JOIN branch AS b  \n" +
            "                           ON b.b_id = tp.tp_frn_b_id  \n" +
            "                                  INNER JOIN (SELECT Max(sr_result_month) AS sr_result_month,  \n" +
            "                                      b_id,  \n" +
            "                                      sr_id,  \n" +
            "                                      sr_frn_sm_id  \n" +
            "                               FROM   student_result sr  \n" +
            "                                      INNER JOIN theory_practical tp  \n" +
            "                                              ON sr.sr_id = tp.tp_frn_sr_id  \n" +
            "                                      INNER JOIN student AS st  \n" +
            "                                              ON st.st_id = sr.sr_frn_st_id  \n" +
            "                                      INNER JOIN branch AS b  \n" +
            "                                              ON b.b_id = tp.tp_frn_b_id  \n" +
            "                                      INNER JOIN subject AS s  \n" +
            "                                              ON s.s_id = tp.tp_frn_s_id  \n" +
            "                               WHERE  s.s_subject_code = ?1  \n" +
            "                                      AND b.b_branch_abbreviation = ?2) AS res  \n" +
            "                           ON sr.sr_result_month = res.sr_result_month  \n" +
            "                              AND sr.sr_frn_sm_id = res.sr_frn_sm_id  \n" +
            "                              AND b.b_id = res.b_id\n" +
            "            WHERE  s.s_subject_code = ?1 AND b_branch_abbreviation=?2 \n" +
            "            ORDER  BY ( tp.tp_external_score + tp.tp_internal_score ) DESC  \n" +
            "            LIMIT  1                                           \n" +
            "            ", nativeQuery = true)
    StudentScores getSubjectTopScorer(String subjectCode,String branchAbb);


    @Transactional
    @Query(value = "SELECT @ss \\:= @ss + 1       stsc_id, \n" +
            "       st.st_student_name   AS stsc_student_name, \n" +
            "       st.st_student_usn    AS stsc_student_usn, \n" +
            "       tp.tp_external_score AS stsc_external_marks, \n" +
            "       tp.tp_internal_score AS stsc_internal_marks, \n" +
            "       tp_total_score       AS stsc_total_marks, \n" +
            "       tp_result_class      AS stsc_result_class \n" +
            "FROM   theory_practical AS tp \n" +
            "       INNER JOIN subject AS s \n" +
            "               ON s.s_id = tp.tp_frn_s_id \n" +
            "       INNER JOIN student_result AS sr \n" +
            "               ON sr.sr_id = tp.tp_frn_sr_id \n" +
            "       INNER JOIN student AS st \n" +
            "               ON st.st_id = sr.sr_frn_st_id \n" +
            "       INNER JOIN branch AS b \n" +
            "               ON b.b_id = tp.tp_frn_b_id \n" +
            "       INNER JOIN (SELECT Max(sr_result_month) AS sr_result_month, \n" +
            "                          b_id, \n" +
            "                          sr_id, \n" +
            "                          sr_frn_sm_id \n" +
            "                   FROM   student_result sr \n" +
            "                          INNER JOIN theory_practical tp \n" +
            "                                  ON sr.sr_id = tp.tp_frn_sr_id \n" +
            "                          INNER JOIN student AS st \n" +
            "                                  ON st.st_id = sr.sr_frn_st_id \n" +
            "                          INNER JOIN branch AS b \n" +
            "                                  ON b.b_id = tp.tp_frn_b_id \n" +
            "                          INNER JOIN subject AS s \n" +
            "                                  ON s.s_id = tp.tp_frn_s_id \n" +
            "                   WHERE  s.s_subject_code = ?1 \n" +
            "                          AND b.b_branch_abbreviation = ?3) AS res \n" +
            "               ON sr.sr_result_month = res.sr_result_month \n" +
            "                  AND sr.sr_frn_sm_id = res.sr_frn_sm_id \n" +
            "                  AND b.b_id = res.b_id, \n" +
            "       (SELECT @s \\:= 0) AS s \n" +
            "WHERE  s.s_subject_code = ?1 \n" +
            "       AND b.b_branch_abbreviation = ?3 \n" +
            "ORDER  BY stsc_total_marks DESC \n" +
            "LIMIT  ?2", nativeQuery = true)
    List<Object[]> getSubjectTopNScorers(String subjectCode,int limitRange,String branch);

    @Transactional
    @Query(value = "SELECT @ss \\:= @ss + 1                        stsc_id, \n" +
            "       st.st_student_name                    AS stsc_student_name, \n" +
            "       st.st_student_usn                     AS stsc_student_usn, \n" +
            "       tp.tp_external_score                  AS stsc_external_marks, \n" +
            "       tp.tp_internal_score                  AS stsc_internal_marks, \n" +
            "       tp_total_score                        AS stsc_total_marks, \n" +
            "       sr_result_month                       AS stsc_result_month, \n" +
            "       tp_result_class                       AS stsc_result_class \n"+
            "FROM   theory_practical AS tp \n" +
            "       INNER JOIN subject AS s \n" +
            "               ON s.s_id = tp.tp_frn_s_id \n" +
            "       INNER JOIN student_result AS sr \n" +
            "               ON sr.sr_id = tp.tp_frn_sr_id \n" +
            "       INNER JOIN student AS st \n" +
            "               ON st.st_id = sr.sr_frn_st_id, \n" +
            "       (SELECT @ss \\:= 0) AS ss \n" +
            "WHERE  s.s_subject_code = ?1 \n" +
            "       AND sr_result_month = (SELECT DISTINCT sr_result_month \n" +
            "                              FROM   student_result sr \n" +
            "                                     INNER JOIN theory_practical tp \n" +
            "                                             ON tp.tp_frn_sr_id = sr.sr_id \n" +
            "                                     INNER JOIN subject s \n" +
            "                                             ON tp.tp_frn_s_id = s.s_id \n" +
            "                                     INNER JOIN branch b " +
            "                                             ON tp.tp_frn_b_id=b.b_id\n "+
            "                              WHERE  s_subject_code = ?1  and b_branch_abbreviation= ?2   \n" +
            "                              ORDER  BY sr_result_month DESC \n" +
            "                              LIMIT  1) ", nativeQuery = true)
    List<StudentScores> getCurrentSemStudentSubjectScores(String subjectCode,String branchAbb);




//    @Transactional
//    @Query(value = "SELECT @ss \\:= @ss+1                           stsc_id, \n" +
//            "       st.st_student_name                    AS stsc_student_name, \n" +
//            "       st.st_student_usn                     AS stsc_student_usn, \n" +
//            "       tp.tp_external_score                  AS stsc_external_marks, \n" +
//            "       tp.tp_internal_score                  AS stsc_internal_marks, \n" +
//            "       (tp_total_score)                      AS stsc_total_marks, \n" +
//            "       sr_result_month                       AS stsc_result_month, \n" +
//            "       tp_result_class                       AS stsc_result_class \n"+
//            "       sm_semester_value                       AS stsc_subject_sem_value \n"+
//            "       sr_student_sem                       AS stsc_result_class \n"+
//            "FROM   theory_practical AS tp \n" +
//            "       INNER JOIN subject AS s \n" +
//            "               ON s.s_id = tp.tp_frn_s_id \n" +
//            "       INNER JOIN student_result AS sr \n" +
//            "               ON sr.sr_id = tp.tp_frn_sr_id \n" +
//            "       INNER JOIN student AS st \n" +
//            "               ON st.st_id = sr.sr_frn_st_id \n" +
//            " inner join branch b on b.b_id=tp.tp_frn_b_id, \n"+
//            "       (SELECT @ss \\:= 0) AS ss \n" +
//            "WHERE  s.s_subject_code = ?1 and b_branch_abbreviation= ?2 "+
//            "       AND tp.tp_result_class = 'FAIL' \n" +
//            "       AND sr_result_month = (SELECT DISTINCT sr_result_month \n" +
//            "                              FROM   student_result sr \n" +
//            "                                     INNER JOIN theory_practical tp \n" +
//            "                                             ON tp.tp_frn_sr_id = sr.sr_id \n" +
//            "                                     INNER JOIN subject s \n" +
//            "                                             ON tp.tp_frn_s_id = s.s_id \n" +
//            "                                     INNER JOIN branch b \n"+
//            "                                             ON b.b_id=s.s_frn_b_id \n"+
//            "                              WHERE  s_subject_code = ?1 and b_branch_abbreviation= ?2 \n"+
//            "                              ORDER  BY sr_result_month DESC \n" +
//            "                              LIMIT  1) ", nativeQuery = true)
//    List<StudentScores> getCurrentSemStudentSubjectScoresWithBacklogs(String subjectCode,String branchAbb);



}
