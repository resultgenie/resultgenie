package com.rg.domain.data.aggregateentities;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * Created by shreeshan on 30/05/17.
 */
public interface SubjectDetailsRepository extends JpaRepository<SubjectDetails,Integer>{

    @Query(value="SELECT @ss\\:= @ss + 1            sd_id, \n" +
            "       s.s_subject_name          AS sd_subject_name, \n" +
            "       s.s_subject_code          AS sd_subject_code, \n" +
            "       s.s_is_lab                AS sd_is_lab, \n" +
            "       sm.sm_semester_value      AS sd_semester, \n" +
            "       b.b_branch_abbreviation   AS sd_branch, \n" +
            "       Avg(tp.tp_external_score) AS sd_avg_external_score, \n" +
            "       Avg(tp.tp_internal_score) AS sd_avg_internal_score \n" +
            "FROM   theory_practical AS tp \n" +
            "       INNER JOIN subject AS s \n" +
            "               ON s.s_id = tp.tp_frn_s_id \n" +
            "       INNER JOIN semester AS sm \n" +
            "               ON sm.sm_id = tp.tp_frn_sm_id \n" +
            "       INNER JOIN branch AS b \n" +
            "               ON b.b_id = tp.tp_frn_b_id, \n" +
            "       (SELECT @ss\\:= 0) AS ss \n" +
            "WHERE  s.s_subject_code =?1 and b_branch_abbreviation=?2 ",nativeQuery = true)
    SubjectDetails getSubjectDetails(String subjectCode,String branchAbb);

}
