package com.rg.domain.data.aggregateentities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Created by shreeshan on 04/07/17.
 * This is an aggregate entity - meaning it is formed by combining two or more tables.
 * So a separate table is not found in the db
 */
@Entity
public class BranchWisePassPercentage {
    @Id
    @Column(name = "bwpp_id")
    @JsonIgnore
    private Integer branchWiseId;

    @Column(name = "bwpp_sem_val")
    private Integer semValue;

    @Column(name = "bwpp_branch_abb")
    @JsonIgnore
    private String branchAbb;

    @Column(name = "bwpp_pass_percentage")
    private Double passPercentage;


    public Integer getBranchWiseId() {
        return branchWiseId;
    }

    public void setBranchWiseId(Integer branchWiseId) {
        this.branchWiseId = branchWiseId;
    }

    public Integer getSemValue() {
        return semValue;
    }

    public void setSemValue(Integer semValue) {
        this.semValue = semValue;
    }

    public String getBranchAbb() {
        return branchAbb;
    }

    public void setBranchAbb(String branchAbb) {
        this.branchAbb = branchAbb;
    }

    public Double getPassPercentage() {
        return passPercentage;
    }

    public void setPassPercentage(Double passPercentage) {
        this.passPercentage = passPercentage;
    }
}
