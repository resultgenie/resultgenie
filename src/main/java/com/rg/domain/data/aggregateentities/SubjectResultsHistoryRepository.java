package com.rg.domain.data.aggregateentities;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * Created by shreeshan on 31/05/17.
 */
public interface SubjectResultsHistoryRepository extends JpaRepository<SubjectResultsHistory,Integer> {

    @Query(value = "SELECT @s \\:= @s + 1           srh_id, \n" +
            "       sr_result_month        AS srh_result_month, \n" +
            "       Avg(tp_external_score) AS srh_external_marks, \n" +
            "       Avg(tp_internal_score) AS srh_internal_marks \n" +
            "FROM   theory_practical AS tp \n" +
            "       INNER JOIN student_result AS sr \n" +
            "               ON tp.tp_frn_sr_id = sr.sr_id \n" +
            "       INNER JOIN student AS st \n" +
            "               ON st.st_id = sr.sr_frn_st_id \n" +
            "       INNER JOIN subject AS s \n" +
            "               ON s.s_id = tp.tp_frn_s_id \n" +
            " inner join branch b on b.b_id=tp.tp_frn_b_id, \n"+
            "       (SELECT @s \\:= 0) AS s \n" +
            "WHERE  s.s_subject_code = ?1 and b_branch_abbreviation=?2\n" +
            "GROUP  BY sr_result_month \n" +
            "ORDER  BY sr_result_month ASC ",nativeQuery = true)
    List<SubjectResultsHistory>  subjectHistory(String subjectCode,String branchAbb);

}
