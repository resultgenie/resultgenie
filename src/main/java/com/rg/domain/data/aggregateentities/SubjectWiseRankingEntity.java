package com.rg.domain.data.aggregateentities;

import javax.persistence.*;

/**
 * Created by shreeshan on 01/09/17.
 */
@Entity
public class SubjectWiseRankingEntity {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "swre_rank")
    private Integer rank;

    @Column(name = "swre_subject_code")
    private String subjectCode;

    @Column(name = "swre_subject_name")
    private String subjectName;

    @Column(name = "swre_subject_performance")
    private float subjectPerformance;

    @Column(name = "swre_total_count")
    private Integer totalCount;

    @Column(name = "swre_branch")
    private String branch;

    public Integer getRank() {
        return rank;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }

    public String getSubjectCode() {
        return subjectCode;
    }

    public void setSubjectCode(String subjectCode) {
        this.subjectCode = subjectCode;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public float getSubjectPerformance() {
        return subjectPerformance;
    }

    public void setSubjectPerformance(float subjectPerformance) {
        this.subjectPerformance = subjectPerformance;
    }

    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }
}
