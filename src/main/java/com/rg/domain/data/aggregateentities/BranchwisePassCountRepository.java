package com.rg.domain.data.aggregateentities;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by shreeshan on 28/05/17.
 */
public interface BranchwisePassCountRepository extends JpaRepository<BranchwisePassCount, Integer> {

    @Query(value = "SELECT @s\\:= @s + 1          bw_id, \n" +
            "       Count(*)              AS bw_pass_count, \n" +
            "       sr.sr_frn_sm_id       AS bw_sem_id, \n" +
            "       b_branch_abbreviation AS bw_branch_abb \n" +
            "FROM   student_result sr \n" +
            "       INNER JOIN student st \n" +
            "               ON sr.sr_frn_st_id = st.st_id \n" +
            "       INNER JOIN branch b \n" +
            "               ON b.b_id = st.st_frn_b_id \n" +
            "       INNER JOIN (SELECT Max(sr_result_month) AS sr_result_month1, \n" +
            "                          b_id, \n" +
            "                          sr_frn_sm_id, \n" +
            "                          sr_id \n" +
            "                   FROM   student_result sr \n" +
            "                          INNER JOIN student st \n" +
            "                                  ON st.st_id = sr.sr_frn_st_id \n" +
            "                          INNER JOIN branch b \n" +
            "                                  ON b.b_id = st.st_frn_b_id \n" +
            "                          INNER JOIN semester sm \n" +
            "                                  ON sm.sm_id = sr.sr_frn_sm_id, \n" +
            "                          (SELECT @ss\\:= 0) AS ss \n" +
            "                   GROUP  BY b_id, \n" +
            "                             sr_frn_sm_id) AS date_months \n" +
            "               ON sr.sr_result_month = date_months.sr_result_month1 \n" +
            "                  AND sr.sr_frn_sm_id = date_months.sr_frn_sm_id \n" +
            "                  AND b.b_id = date_months.b_id, \n" +
            "       (SELECT @s\\:= 0) AS s \n" +
            "WHERE  sr.sr_result_class != 'FAIL' \n" +
            "       AND sr.sr_result_class != 'TO BE ANNOUNCED LATER' \n" +
            "       AND sr.sr_result_class != 'MAL' \n" +
            "GROUP  BY sr.sr_frn_sm_id, \n" +
            "          st.st_frn_b_id ", nativeQuery = true)
    List<BranchwisePassCount> getBranchWisePassCount();

    @Query(value = "SELECT \n" +
            "    @s\\:=@s + 1 bw_id,\n" +
            "    COUNT(*) AS bw_pass_count,\n" +
            "    sr.sr_frn_sm_id AS bw_sem_id,\n" +
            "    b_branch_abbreviation AS bw_branch_abb\n" +
            "FROM\n" +
            "    student_result sr\n" +
            "        INNER JOIN\n" +
            "    student st ON sr.sr_frn_st_id = st.st_id\n" +
            "        INNER JOIN\n" +
            "    branch b ON b.b_id = st.st_frn_b_id\n" +
            "        INNER JOIN\n" +
            "    semester sm ON sm.sm_id = sr.sr_frn_sm_id,\n" +
            "    (SELECT @s\\:=0) AS s\n" +
            "WHERE\n" +
            "    sr.sr_result_class != 'FAIL'\n" +
            "        AND sr.sr_result_class != 'TO BE ANNOUNCED LATER'\n" +
            "        AND sr.sr_result_class != 'MAL'\n" +
            "        AND sm.sm_semester_value = ?1\n" +
            "        AND st_joining_year = ?2\n" +
            "GROUP BY sr.sr_frn_sm_id , st.st_frn_b_id",nativeQuery = true)
    @Transactional
    List<BranchwisePassCount> getBranchWisePassCountForSemAndBatch(Integer currentSem, Integer i);
}
