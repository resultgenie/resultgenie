package com.rg.domain.data.aggregateentities;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by shreeshan on 31/08/17.
 */
public interface BacklogStudentScoresRepository extends JpaRepository<BacklogStudentScores,Integer> {




    @Transactional
    @Query(value = "SELECT @ss \\:= @ss+1                           stsc_id,\n" +
            "    st.st_student_name                    AS stsc_student_name,\n" +
            "    st.st_student_usn                     AS stsc_student_usn,\n" +
            "    tp.tp_external_score                  AS stsc_external_marks,\n" +
            "    tp.tp_internal_score                  AS stsc_internal_marks,\n" +
            "    tp_total_score                        AS stsc_total_marks,\n" +
            "    sr_result_month                       AS stsc_result_month,\n" +
            "    tp_result_class                       AS stsc_result_class ,\n" +
            "    sm_semester_value                     AS stsc_subject_sem_value,\n" +
            "    res.sm_val                            AS stsc_student_sem,\n" +
            "    s_subject_code                        AS stsc_subject_code,\n" +
            "    s_subject_name                        AS stsc_subject_name,"+
            "    (select 0)                            AS stsc_is_active_backlog\n" +
            "    FROM   theory_practical AS tp\n" +
            "    INNER JOIN subject AS s\n" +
            "    ON s.s_id = tp.tp_frn_s_id\n" +
            "    INNER JOIN student_result AS sr\n" +
            "    ON sr.sr_id = tp.tp_frn_sr_id\n" +
            "    INNER JOIN student AS st\n" +
            "    ON st.st_id = sr.sr_frn_st_id\n" +
            "    INNER JOIN semester as sm\n" +
            "    ON sm.sm_id=sr.sr_frn_sm_id\n" +
            "    inner join branch b on b.b_id=tp.tp_frn_b_id\n" +
            "    inner join (select max(sm_semester_value) as sm_val,st_student_usn,st_student_name from student_result " +
            "sr\n" +
            "    inner join student st on st.st_id=sr.sr_frn_st_id\n" +
            "    inner join semester sm on sm.sm_id=sr.sr_frn_sm_id\n" +
            "    inner join branch b on b.b_id=st.st_frn_b_id where\n" +
            "    b_branch_abbreviation= ?2\n" +
            "    group by st_id) as res\n" +
            "    on\n" +
            "    res.st_student_usn=st.st_student_usn,\n" +
            "            (SELECT @ss \\:= 0) AS ss\n" +
            "    WHERE  s.s_subject_code = ?1 and b_branch_abbreviation= ?2\n" +
            "    AND tp.tp_result_class = 'FAIL'", nativeQuery = true)
    List<BacklogStudentScores> getStudentSubjectScoresWithBacklogs(String subjectCode, String branchAbb);

    @Transactional
    @Query(value = "SELECT \n" +
            "    @ss\\:=@ss + 1 stsc_id,\n" +
            "    st.st_student_name AS stsc_student_name,\n" +
            "    st.st_student_usn AS stsc_student_usn,\n" +
            "    tp.tp_external_score AS stsc_external_marks,\n" +
            "    tp.tp_internal_score AS stsc_internal_marks,\n" +
            "    tp_total_score AS stsc_total_marks,\n" +
            "    sr_result_month AS stsc_result_month,\n" +
            "    tp_result_class AS stsc_result_class,\n" +
            "    sm_semester_value AS stsc_subject_sem_value,\n" +
            "    s_subject_code                        AS stsc_subject_code,\n" +
            "    s_subject_name                        AS stsc_subject_name,"+
            "    (SELECT \n" +
            "            MAX(sm_semester_value)\n" +
            "        FROM\n" +
            "            student_result sr\n" +
            "                INNER JOIN\n" +
            "            student st ON st.st_id = sr.sr_frn_st_id\n" +
            "                INNER JOIN\n" +
            "            semester sm ON sm.sm_id = sr.sr_frn_sm_id\n" +
            "        WHERE\n" +
            "            st_student_usn = ?2) AS stsc_student_sem,\n" +
            "    (SELECT 0) AS stsc_is_active_backlog\n" +
            "FROM\n" +
            "    theory_practical AS tp\n" +
            "        INNER JOIN\n" +
            "    subject AS s ON s.s_id = tp.tp_frn_s_id\n" +
            "        INNER JOIN\n" +
            "    student_result AS sr ON sr.sr_id = tp.tp_frn_sr_id\n" +
            "        INNER JOIN\n" +
            "    student AS st ON st.st_id = sr.sr_frn_st_id\n" +
            "        INNER JOIN\n" +
            "    semester AS sm ON sm.sm_id = sr.sr_frn_sm_id\n" +
            "        INNER JOIN\n" +
            "    branch b ON b.b_id = tp.tp_frn_b_id, (select @ss\\:=0) as ss\n" +
            "WHERE\n" +
            "    st_student_usn = ?2 AND tp_result_class != ?1\n",nativeQuery = true)
    List<BacklogStudentScores> getStudentBacklogs(String stringForPass, String usn);
}
