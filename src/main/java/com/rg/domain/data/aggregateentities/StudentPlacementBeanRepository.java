package com.rg.domain.data.aggregateentities;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by shreeshan on 18/07/17.
 */
public interface StudentPlacementBeanRepository extends JpaRepository<StudentPlacementBean, Integer> {

    @Query(value = "SELECT @s \\:= @s + 1 spb_id, \n" +
            "       spb_student_name, \n" +
            "       spb_student_usn, \n" +
            "       spb_aggregate, \n" +
            "       spb_backlog, \n" +
            "       spb_branch, \n" +
            "       spb_semester,\n" +
            "       (CASE  \n" +
            "\t\tWHEN spb_aggregate >=70 and spb_backlog = 0 THEN 'FIRST CLASS WITH DISTINCTION' \n" +
            "        WHEN spb_aggregate < 70 and spb_aggregate >=60 and spb_backlog = 0 then 'FIRST CLASS' \n" +
            "        WHEN spb_aggregate < 60  and spb_backlog = 0 then 'SECOND CLASS' \n" +
            "        WHEN spb_backlog >= 1 then 'FAIL' \t\t\n" +
            "                                   end)                AS spb_result_class\n" +
            "FROM   (SELECT st_student_name       AS spb_student_name, \n" +
            "               st_student_usn        AS spb_student_usn, \n" +
            "               Avg(sr_sem_aggregate) AS spb_aggregate, \n" +
            "               Sum(sr_backlog_count) AS spb_backlog, \n" +
            "               b_branch_abbreviation AS spb_branch, \n" +
            "               results.sm_val        AS spb_semester \n" +
            "        FROM   student st \n" +
            "               INNER JOIN student_result sr \n" +
            "                       ON sr.sr_frn_st_id = st.st_id \n" +
            "               INNER JOIN branch b \n" +
            "                       ON b.b_id = st.st_frn_b_id \n" +
            "               INNER JOIN semester sm \n" +
            "                       ON sm.sm_id = sr.sr_frn_sm_id \n" +
            "               INNER JOIN (SELECT st_student_name        AS student_name, \n" +
            "                                  st_student_usn         AS usn, \n" +
            "                                  Max(sm_semester_value) AS sm_val \n" +
            "                           FROM   student st \n" +
            "                                  INNER JOIN student_result sr \n" +
            "                                          ON st.st_id = sr.sr_frn_st_id \n" +
            "                                  INNER JOIN semester sm \n" +
            "                                          ON sm.sm_id = sr.sr_frn_sm_id \n" +
            "                                  INNER JOIN branch b \n" +
            "                                          ON b.b_id = st.st_frn_b_id \n" +
            "                                  INNER JOIN (SELECT Max(sr_result_month) AS \n" +
            "                                                     result_month, \n" +
            "                                                     sm_semester_value    AS \n" +
            "                                                     sm_val \n" +
            "                                              FROM   student_result sr \n" +
            "                                                     INNER JOIN semester sm \n" +
            "                                                             ON sm.sm_id = \n" +
            "                                                                sr.sr_frn_sm_id \n" +
            "                                              WHERE  sm_semester_value IN ( ?1 \n" +
            "                                                     ) \n" +
            "                                              GROUP  BY sm_semester_value) AS \n" +
            "                                             res \n" +
            "                                          ON res.result_month = \n" +
            "                                             sr.sr_result_month \n" +
            "                                             AND \n" +
            "                                  res.sm_val = sm.sm_semester_value \n" +
            "                           WHERE  sm_semester_value IN ( ?1 ) \n" +
            "                                  AND b_branch_abbreviation IN ( ?2 ) \n" +
            "                           GROUP  BY st_student_usn) AS results \n" +
            "                       ON results.usn = st.st_student_usn \n" +
            "        GROUP  BY st_student_usn) AS final_res, \n" +
            "       (SELECT @s \\:= 0) AS s \n" +
            "WHERE final_res.spb_aggregate >= ?3 and final_res.spb_backlog=?4 order by final_res.spb_aggregate desc", nativeQuery = true)
    @Transactional
    List<StudentPlacementBean> getStudentsForPlacementsNoForBacklog(List<String> semesters, List<String> branches,
                                                                    Double aggregate, Integer activeBacklog);

    @Query(value = "SELECT @s \\:= @s + 1 spb_id, \n" +
            "       spb_student_name, \n" +
            "       spb_student_usn, \n" +
            "       spb_aggregate, \n" +
            "       spb_backlog, \n" +
            "       spb_branch, \n" +
            "       spb_semester,\n" +
            "       (CASE  \n" +
            "\t\tWHEN spb_aggregate >=70 and spb_backlog = 0 THEN 'FIRST CLASS WITH DISTINCTION' \n" +
            "        WHEN spb_aggregate < 70 and spb_aggregate >=60 and spb_backlog = 0 then 'FIRST CLASS' \n" +
            "        WHEN spb_aggregate < 60  and spb_backlog = 0 then 'SECOND CLASS' \n" +
            "        WHEN spb_backlog >= 1 then 'FAIL' \t\t\n" +
            "                                   end)                AS spb_result_class\n" +
            "FROM   (SELECT st_student_name       AS spb_student_name, \n" +
            "               st_student_usn        AS spb_student_usn, \n" +
            "               Avg(sr_sem_aggregate) AS spb_aggregate, \n" +
            "               Sum(sr_backlog_count) AS spb_backlog, \n" +
            "               b_branch_abbreviation AS spb_branch, \n" +
            "               results.sm_val        AS spb_semester \n" +
            "        FROM   student st \n" +
            "               INNER JOIN student_result sr \n" +
            "                       ON sr.sr_frn_st_id = st.st_id \n" +
            "               INNER JOIN branch b \n" +
            "                       ON b.b_id = st.st_frn_b_id \n" +
            "               INNER JOIN semester sm \n" +
            "                       ON sm.sm_id = sr.sr_frn_sm_id \n" +
            "               INNER JOIN (SELECT st_student_name        AS student_name, \n" +
            "                                  st_student_usn         AS usn, \n" +
            "                                  Max(sm_semester_value) AS sm_val \n" +
            "                           FROM   student st \n" +
            "                                  INNER JOIN student_result sr \n" +
            "                                          ON st.st_id = sr.sr_frn_st_id \n" +
            "                                  INNER JOIN semester sm \n" +
            "                                          ON sm.sm_id = sr.sr_frn_sm_id \n" +
            "                                  INNER JOIN branch b \n" +
            "                                          ON b.b_id = st.st_frn_b_id \n" +
            "                                  INNER JOIN (SELECT Max(sr_result_month) AS \n" +
            "                                                     result_month, \n" +
            "                                                     sm_semester_value    AS \n" +
            "                                                     sm_val \n" +
            "                                              FROM   student_result sr \n" +
            "                                                     INNER JOIN semester sm \n" +
            "                                                             ON sm.sm_id = \n" +
            "                                                                sr.sr_frn_sm_id \n" +
            "                                              WHERE  sm_semester_value IN ( ?1 \n" +
            "                                                     ) \n" +
            "                                              GROUP  BY sm_semester_value) AS \n" +
            "                                             res \n" +
            "                                          ON res.result_month = \n" +
            "                                             sr.sr_result_month \n" +
            "                                             AND \n" +
            "                                  res.sm_val = sm.sm_semester_value \n" +
            "                           WHERE  sm_semester_value IN ( ?1 ) \n" +
            "                                  AND b_branch_abbreviation IN ( ?2 ) \n" +
            "                           GROUP  BY st_student_usn) AS results \n" +
            "                       ON results.usn = st.st_student_usn \n" +
            "        GROUP  BY st_student_usn) AS final_res, \n" +
            "       (SELECT @s \\:= 0) AS s \n" +
            "WHERE final_res.spb_aggregate >= ?3 order by final_res.spb_aggregate desc", nativeQuery = true)
    @Transactional
    List<StudentPlacementBean> getStudentsForPlacements(List<String> semesters, List<String> branches, Double
            aggregate);


    @Query(value = "SELECT \n" +
            "    COUNT(distinct st_student_usn)\n" +
            "FROM\n" +
            "    student st\n" +
            "        INNER JOIN\n" +
            "    student_result sr ON sr.sr_frn_st_id = st.st_id\n" +
            "        INNER JOIN\n" +
            "    branch b ON b.b_id = st.st_frn_b_id\n" +
            "        INNER JOIN\n" +
            "    semester sm ON sm.sm_id = sr.sr_frn_sm_id\n" +
            "        INNER JOIN\n" +
            "    (SELECT \n" +
            "        st_student_name AS student_name,\n" +
            "            st_student_usn AS usn,\n" +
            "            MAX(sm_semester_value) AS sm_val\n" +
            "    FROM\n" +
            "        student st\n" +
            "    INNER JOIN student_result sr ON st.st_id = sr.sr_frn_st_id\n" +
            "    INNER JOIN semester sm ON sm.sm_id = sr.sr_frn_sm_id\n" +
            "    INNER JOIN branch b ON b.b_id = st.st_frn_b_id\n" +
            "    INNER JOIN (SELECT \n" +
            "        MAX(sr_result_month) AS result_month,\n" +
            "            sm_semester_value AS sm_val\n" +
            "    FROM\n" +
            "        student_result sr\n" +
            "    INNER JOIN semester sm ON sm.sm_id = sr.sr_frn_sm_id\n" +
            "    WHERE\n" +
            "        sm_semester_value IN (?1)\n" +
            "    GROUP BY sm_semester_value) AS res ON res.result_month = sr.sr_result_month\n" +
            "        AND res.sm_val = sm.sm_semester_value\n" +
            "    WHERE\n" +
            "        sm_semester_value IN (?1)\n" +
            "            AND b_branch_abbreviation IN (?2)\n" +
            "    GROUP BY st_student_usn) AS results ON results.usn = st.st_student_usn \n", nativeQuery = true)
    @Transactional
    Integer getTotalStudentCount(List<String> semesters, List<String> branches);
}
