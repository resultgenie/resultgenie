package com.rg.domain.data.aggregateentities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Created by shreeshan on 29/05/17.
 */
@Entity
public class SubjectResultsHistory {

    @Id
    @Column(name = "srh_id")
    @JsonIgnore
    private Integer subjectResultsHistoryId;

    @Column(name = "srh_result_month")
    private Long resultMonth;

    @Column(name = "srh_internal_marks")
    private Integer internalMarks;

    @Column(name = "srh_external_marks")
    private Integer externalMarks;

    public Integer getSubjectResultsHistoryId() {

        return subjectResultsHistoryId;
    }

    public void setSubjectResultsHistoryId(Integer subjectResultsHistoryId) {
        this.subjectResultsHistoryId = subjectResultsHistoryId;
    }

    public Long getResultMonth() {
        return resultMonth;
    }

    public void setResultMonth(Long resultMonth) {
        this.resultMonth = resultMonth;
    }

    public Integer getInternalMarks() {
        return internalMarks;
    }

    public void setInternalMarks(Integer internalMarks) {
        this.internalMarks = internalMarks;
    }

    public Integer getExternalMarks() {
        return externalMarks;
    }

    public void setExternalMarks(Integer externalMarks) {
        this.externalMarks = externalMarks;
    }
}
