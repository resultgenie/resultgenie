package com.rg.domain.data;

import com.rg.domain.data.aggregateentities.YearSemesterBranchCombination;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;
import java.util.List;

public interface SemesterRepository extends JpaRepository<Semester,Integer>{

	Semester findBySemesterValue( String string );


}
