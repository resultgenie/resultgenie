package com.rg.domain.data;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "college")
public class College {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "c_id")
    private Integer collegeId;

    @Column(name = "c_college_code")
    private String collegeCode;

    @Column(name = "c_college_name")
    private String collegeName;

    @Column(name = "c_college_abbreviation")
    private String collegeAbbreviation;

    @Column(name = "c_college_region")
    private String collegeRegion;

    @Column(name = "c_college_description")
    private String collegeDescription;

    @Column(name = "c_college_email")
    private String collegeEmail;

    @Column(name = "c_registered_datetime")
    private Long registeredDateTime;

    @Column(name = "c_key")
    @JsonIgnore
    private String collegeKey;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "college_branch", joinColumns = @JoinColumn(name = "bc_frn_c_id", referencedColumnName =
            "c_id"), inverseJoinColumns = @JoinColumn(name = "bc_frn_b_id", referencedColumnName = "b_id"))
    private List<Branch> branch = new ArrayList<>();

    public String getCollegeAbbreviation() {
        return collegeAbbreviation;
    }

    public void setCollegeAbbreviation(String collegeAbbreviation) {
        this.collegeAbbreviation = collegeAbbreviation;
    }

    public String getCollegeKey() {
        return collegeKey;
    }

    public void setCollegeKey(String collegeKey) {
        this.collegeKey = collegeKey;
    }

    public Long getRegisteredDateTime() {
        return registeredDateTime;
    }

    public void setRegisteredDateTime(Long registeredDateTime) {
        this.registeredDateTime = registeredDateTime;
    }

    public Integer getCollegeId() {
        return collegeId;
    }

    public void setCollegeId(Integer collegeId) {
        this.collegeId = collegeId;
    }

    public String getCollegeCode() {
        return collegeCode;
    }

    public void setCollegeCode(String collegeCode) {
        this.collegeCode = collegeCode;
    }

    public String getCollegeName() {
        return collegeName;
    }

    public void setCollegeName(String collegeName) {
        this.collegeName = collegeName;
    }

    public String getCollegeRegion() {
        return collegeRegion;
    }

    public void setCollegeRegion(String collegeRegion) {
        this.collegeRegion = collegeRegion;
    }

    public String getCollegeDescription() {
        return collegeDescription;
    }

    public void setCollegeDescription(String collegeDescription) {
        this.collegeDescription = collegeDescription;
    }

    public String getCollegeEmail() {
        return collegeEmail;
    }

    public void setCollegeEmail(String collegeEmail) {
        this.collegeEmail = collegeEmail;
    }

    public List<Branch> getBranch() {
        return branch;
    }

    public void setBranch(List<Branch> branch) {
        this.branch = branch;
    }
}
