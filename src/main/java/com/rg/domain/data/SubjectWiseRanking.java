package com.rg.domain.data;


import javax.persistence.*;

/**
 * Created by shreeshan on 01/09/17.
 */
@Entity
@Table(name = "subject_wise_ranking")
public class SubjectWiseRanking {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "swr_id")
    private Integer swrId;

    @Column(name = "swr_subject_name")
    private String subjectName;

    @Column(name = "swr_branch")
    private String branch;

    @Column(name = "swr_subject_code")
    private String subjectCode;

    @Column(name = "swr_subject_performance")
    private float subjectPerformance;

    @Column(name = "swr_total_subjects_in_college")
    private Integer totalSubjectsInCollege;

    @Column(name = "swr_rank_in_college")
    private Integer rankInCollege;

    @Column(name = "swr_total_subjects_in_branch")
    private Integer totalSubjectsInBranch;

    @Column(name = "swr_rank_in_branch")
    private Integer rankInBranch;

    @Column(name = "swr_total_subjects_in_semester")
    private Integer totalSubjectsInSemester;

    @Column(name = "swr_rank_in_semester")
    private Integer rankInSemester;


    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getSubjectCode() {
        return subjectCode;
    }

    public void setSubjectCode(String subjectCode) {
        this.subjectCode = subjectCode;
    }

    public float getSubjectPerformance() {
        return subjectPerformance;
    }

    public void setSubjectPerformance(float subjectPerformance) {
        this.subjectPerformance = subjectPerformance;
    }

    public Integer getSwrId() {
        return swrId;
    }

    public void setSwrId(Integer swrId) {
        this.swrId = swrId;
    }

    public Integer getTotalSubjectsInCollege() {
        return totalSubjectsInCollege;
    }

    public void setTotalSubjectsInCollege(Integer totalSubjectsInCollege) {
        this.totalSubjectsInCollege = totalSubjectsInCollege;
    }

    public Integer getRankInCollege() {
        return rankInCollege;
    }

    public void setRankInCollege(Integer rankInCollege) {
        this.rankInCollege = rankInCollege;
    }

    public Integer getTotalSubjectsInBranch() {
        return totalSubjectsInBranch;
    }

    public void setTotalSubjectsInBranch(Integer totalSubjectsInBranch) {
        this.totalSubjectsInBranch = totalSubjectsInBranch;
    }

    public Integer getRankInBranch() {
        return rankInBranch;
    }

    public void setRankInBranch(Integer rankInBranch) {
        this.rankInBranch = rankInBranch;
    }

    public Integer getTotalSubjectsInSemester() {
        return totalSubjectsInSemester;
    }

    public void setTotalSubjectsInSemester(Integer totalSubjectsInSemester) {
        this.totalSubjectsInSemester = totalSubjectsInSemester;
    }

    public Integer getRankInSemester() {
        return rankInSemester;
    }

    public void setRankInSemester(Integer rankInSemester) {
        this.rankInSemester = rankInSemester;
    }
}
