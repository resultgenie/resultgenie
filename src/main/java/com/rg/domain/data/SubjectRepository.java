package com.rg.domain.data;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface SubjectRepository extends JpaRepository<Subject,Integer>{

	Subject findBySubjectName( String name );

	@Query(value="select s_subject_code,b_branch_abbreviation,s_subject_name from subject s inner join branch b on b.b_id=s.s_frn_b_id where s.s_subject_name like %?1% or s.s_subject_code like %?1%",nativeQuery = true)
	List<Object> getSubjectAndSubjectCodeForString(String query);

    Subject findBySubjectNameAndSubjectCode(String name, String subjectCode);

    @Query(value="select * from subject s inner join branch b on b.b_id=s.s_frn_b_id where s_subject_code=?1 and b_branch_abbreviation=?2",nativeQuery = true)
    Subject findBySubjectCodeAndBranchAbbreviation(String subjectCode,String branchAbb);

	Subject findBySubjectCodeAndBranch(String subjectCode, Branch branch);
}
