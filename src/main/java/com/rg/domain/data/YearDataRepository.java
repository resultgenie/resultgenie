package com.rg.domain.data;

import org.springframework.data.jpa.repository.JpaRepository;

public interface YearDataRepository extends JpaRepository<YearData,Integer>{

}
