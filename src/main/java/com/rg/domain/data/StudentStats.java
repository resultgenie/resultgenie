package com.rg.domain.data;

import javax.persistence.*;

/**
 * @author shreesha
 *         This class contains certain stats of a student and it needs
 *         to be updated for every 6months/when a semester result is rolled out
 */
@Entity
@Table(name = "student_stats")
public class StudentStats {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ss_id")
    private Integer studentStatsId;

    @OneToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "ss_frn_st_id")
    private Student student;

    @OneToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "ss_frn_sm_id")
    private Semester currentSemester;

    @Column(name = "ss_current_ranking")
    private Integer currentRanking;

    @OneToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "ss_frn_s_id_best_score_subject")
    private Subject bestScoreSubject;

    @Column(name = "ss_best_score")
    private Float bestScore;

    @OneToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "ss_frn_s_id_worst_score_subject")
    private Subject worstScoreSubject;

    @Column(name = "ss_worst_score")
    private Float worstScore;

    @Column(name = "ss_aggregate")
    private Float aggregate;

    @Column(name = "ss_student_zone")
    private String studentZone;

    @OneToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "ss_frn_b_id")
    private Branch branch;

    @Column(name = "ss_current_year")
    private Integer currentYear;

    @OneToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "ss_frn_sr_id_current")
    private StudentResult currentSemStudentResult;

    public Integer getStudentStatsId() {
        return studentStatsId;
    }

    public void setStudentStatsId(Integer studentStatsId) {
        this.studentStatsId = studentStatsId;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Semester getCurrentSemester() {
        return currentSemester;
    }

    public void setCurrentSemester(Semester currentSemester) {
        this.currentSemester = currentSemester;
    }

    public Integer getCurrentRanking() {
        return currentRanking;
    }

    public void setCurrentRanking(Integer currentRanking) {
        this.currentRanking = currentRanking;
    }

    public Subject getBestScoreSubject() {
        return bestScoreSubject;
    }

    public void setBestScoreSubject(Subject bestScoreSubject) {
        this.bestScoreSubject = bestScoreSubject;
    }

    public Float getBestScore() {
        return bestScore;
    }

    public void setBestScore(Float bestScore) {
        this.bestScore = bestScore;
    }

    public Subject getWorstScoreSubject() {
        return worstScoreSubject;
    }

    public void setWorstScoreSubject(Subject worstScoreSubject) {
        this.worstScoreSubject = worstScoreSubject;
    }

    public Float getWorstScore() {
        return worstScore;
    }

    public void setWorstScore(Float worstScore) {
        this.worstScore = worstScore;
    }

    public Float getAggregate() {
        return aggregate;
    }

    public void setAggregate(Float aggregate) {
        this.aggregate = aggregate;
    }

    public String getStudentZone() {
        return studentZone;
    }

    public void setStudentZone(String studentZone) {
        this.studentZone = studentZone;
    }

    public Branch getBranch() {
        return branch;
    }

    public void setBranch(Branch branch) {
        this.branch = branch;
    }

    public Integer getCurrentYear() {
        return currentYear;
    }

    public void setCurrentYear(Integer currentYear) {
        this.currentYear = currentYear;
    }

    public StudentResult getCurrentSemStudentResult() {
        return currentSemStudentResult;
    }

    public void setCurrentSemStudentResult(StudentResult currentSemStudentResult) {
        this.currentSemStudentResult = currentSemStudentResult;
    }

    @Override
    public String toString() {
        return "StudentStats{" +
                "studentStatsId=" + studentStatsId +
                ", student=" + student +
                ", currentSemester=" + currentSemester +
                ", currentRanking=" + currentRanking +
                ", bestScoreSubject=" + bestScoreSubject +
                ", bestScore=" + bestScore +
                ", worstScoreSubject=" + worstScoreSubject +
                ", worstScore=" + worstScore +
                ", aggregate='" + aggregate + '\'' +
                ", studentZone='" + studentZone + '\'' +
                ", branch=" + branch +
                ", currentYear=" + currentYear +
                ", currentSemStudentResult=" + currentSemStudentResult +
                '}';
    }
}
