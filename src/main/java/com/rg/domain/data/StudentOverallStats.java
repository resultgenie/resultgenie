package com.rg.domain.data;

import javax.persistence.*;

/**
 * Created by shreeshan on 04/06/17.
 */
@Entity
@Table(name = "student_overall_stats")
public class StudentOverallStats {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "sos_id")
    private Integer studentOverallStatsId;

    @OneToOne
    @JoinColumn(name = "sos_frn_st_id")
    private Student student;

    @OneToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "sos_frn_s_id_overall_best_score_subject")
    private Subject overallBestScoreSubject;

    @Column(name = "sos_overall_best_score")
    private Float overallBestScore;

    @Column(name = "sos_change_in_best_score")
    private Float changeInBestScore = 0f;

    @Column(name = "sos_change_in_worst_score")
    private Float changeInWorstScore = 0f;

    @OneToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "sos_frn_s_id_overall_worst_score_subject")
    private Subject overallWorstScoreSubject;

    @Column(name = "sos_overall_worst_score")
    private Float overallWorstScore;

    public Float getChangeInBestScore() {
        return changeInBestScore;
    }

    public void setChangeInBestScore(Float changeInBestScore) {
        this.changeInBestScore = changeInBestScore;
    }

    public Float getChangeInWorstScore() {
        return changeInWorstScore;
    }

    public void setChangeInWorstScore(Float changeInWorstScore) {
        this.changeInWorstScore = changeInWorstScore;
    }

    public Integer getStudentOverallStatsId() {
        return studentOverallStatsId;
    }

    public void setStudentOverallStatsId(Integer studentOverallStatsId) {
        this.studentOverallStatsId = studentOverallStatsId;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Subject getOverallBestScoreSubject() {
        return overallBestScoreSubject;
    }

    public void setOverallBestScoreSubject(Subject overallBestScoreSubject) {
        this.overallBestScoreSubject = overallBestScoreSubject;
    }

    public Float getOverallBestScore() {
        return overallBestScore;
    }

    public void setOverallBestScore(Float overallBestScore) {
        this.overallBestScore = overallBestScore;
    }

    public Subject getOverallWorstScoreSubject() {
        return overallWorstScoreSubject;
    }

    public void setOverallWorstScoreSubject(Subject overallWorstScoreSubject) {
        this.overallWorstScoreSubject = overallWorstScoreSubject;
    }

    public Float getOverallWorstScore() {
        return overallWorstScore;
    }

    public void setOverallWorstScore(Float overallWorstScore) {
        this.overallWorstScore = overallWorstScore;
    }
}
