package com.rg.domain.data;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by shreeshan on 04/06/17.
 */
public interface StudentOverallStatsRepository extends JpaRepository<StudentOverallStats,Integer> {
    StudentOverallStats findByStudent(Student st);
}
