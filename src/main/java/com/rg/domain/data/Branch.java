package com.rg.domain.data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


/**
 * This is a college specific 'BRANCH' domain class
 */
@Entity
@Table(name = "branch")
public class Branch {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "b_id")
    private Integer branchId;

    @Column(name = "b_branch_value")
    private String branchValue;

    @Column(name = "b_branch_intake")
    private Integer branchIntake;

    @Column(name = "b_branch_abbreviation")
    private String branchAbbreviation;

    @OneToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "branch_subject", joinColumns = @JoinColumn(name = "bs_frn_b_id", referencedColumnName = "b_id" ), inverseJoinColumns = @JoinColumn(name = "bs_frn_s_id", referencedColumnName = "s_id"))
    private List<Subject> subjects = new ArrayList<>();


    public String getBranchAbbreviation() {
        return branchAbbreviation;
    }

    public void setBranchAbbreviation(String branchAbbreviation) {
        this.branchAbbreviation = branchAbbreviation;
    }

    public Integer getBranchIntake() {
        return branchIntake;
    }

    public void setBranchIntake(Integer branchIntake) {
        this.branchIntake = branchIntake;
    }

    public List<Subject> getSubjects() {
        return subjects;
    }

    public void setSubjects(List<Subject> subjects) {
        this.subjects = subjects;
    }


    public Integer getBranchId() {
        return branchId;
    }

    public void setBranchId(Integer branchId) {
        this.branchId = branchId;
    }

    public String getBranchValue() {
        return branchValue;
    }

    public void setBranchValue(String branchValue) {
        this.branchValue = branchValue;
    }

}
