package com.rg.domain.data;

import javax.persistence.*;

@Entity
@Table(name = "subject")
public class Subject {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "s_id")
    private Integer subjectId;

    @Column(name = "s_subject_name")
    private String subjectName;

    @Column(name = "s_subject_code")
    private String subjectCode;

    @Column(name = "s_is_lab")
    private Integer isLab;

    @ManyToOne
    @JoinColumn(name = "s_frn_b_id")
    private Branch branch;

    @ManyToOne
    @JoinColumn(name = "s_frn_sm_id")
    private Semester semester;

    @Column(name = "s_external_max_marks")
    private Integer externalMaxMarks;

    @Column(name = "s_internal_max_marks")
    private Integer internalMaxMarks;

    @Column(name="s_is_required_for_aggregate_calc")
    private Integer isRequiredForAggregateCalc;

    /**
     * If this flag is set '0' then the subject is either seminar or project work.
     * If its set '1' then its a normal theory/lab subject
     */
    @Column(name = "s_is_regular_subject")
    private Integer isRegularSubject;

    public Integer getIsRequiredForAggregateCalc() {
        return isRequiredForAggregateCalc;
    }

    public void setIsRequiredForAggregateCalc(Integer isRequiredForAggregateCalc) {
        this.isRequiredForAggregateCalc = isRequiredForAggregateCalc;
    }

    public Integer getIsRegularSubject() {
        return isRegularSubject;
    }

    public void setIsRegularSubject(Integer isRegularSubject) {
        this.isRegularSubject = isRegularSubject;
    }

    public Integer getExternalMaxMarks() {
        return externalMaxMarks;
    }

    public void setExternalMaxMarks(Integer externalMaxMarks) {
        this.externalMaxMarks = externalMaxMarks;
    }

    public Integer getInternalMaxMarks() {
        return internalMaxMarks;
    }

    public void setInternalMaxMarks(Integer internalMaxMarks) {
        this.internalMaxMarks = internalMaxMarks;
    }

    public Integer getIsLab() {
        return isLab;
    }

    public void setIsLab(Integer isLab) {
        this.isLab = isLab;
    }

    public Semester getSemester() {
        return semester;
    }

    public void setSemester(Semester semester) {
        this.semester = semester;
    }

    public Branch getBranch() {
        return branch;
    }

    public void setBranch(Branch branch) {
        this.branch = branch;
    }

    public Integer getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(Integer subjectId) {
        this.subjectId = subjectId;
    }

    public String getSubjectName()

    {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getSubjectCode() {
        return subjectCode;
    }

    public void setSubjectCode(String subjectCode) {
        this.subjectCode = subjectCode;
    }

}
