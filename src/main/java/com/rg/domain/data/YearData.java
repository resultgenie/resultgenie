package com.rg.domain.data;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "year_data")
public class YearData {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "yd_id")
	private Integer yearDataId;

	@Column(name = "yd_year_value")
	private Integer yearValue;

	@ManyToMany
	@JoinTable(
			name="year_data_semester"
			, joinColumns={
				@JoinColumn(name="year_data_yd_id", nullable=false)
				}
			, inverseJoinColumns={
				@JoinColumn(name="semester_sm_id", nullable=false)
				}
			)
	private List<Semester> semester = new ArrayList<>();

	public Integer getYearDataId() {
		return yearDataId;
	}

	public void setYearDataId(Integer yearDataId) {
		this.yearDataId = yearDataId;
	}

	public Integer getYearValue() {
		return yearValue;
	}

	public void setYearValue(Integer yearValue) {
		this.yearValue = yearValue;
	}

	public List<Semester> getSemester() {
		return semester;
	}

	public void setSemester(List<Semester> semester) {
		this.semester = semester;
	}

}
