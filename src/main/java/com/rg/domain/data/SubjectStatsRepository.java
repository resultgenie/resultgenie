package com.rg.domain.data;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by shreeshan on 04/06/17.
 */
public interface SubjectStatsRepository extends JpaRepository<SubjectStats,Integer> {
    SubjectStats findBySubject(Subject sb);

    SubjectStats findBySubjectAndBranch(Subject subject, Branch branch);
}
