package com.rg.domain.data;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface StudentRepository extends JpaRepository<Student,Integer> {

	@Query(value="from Student where studentUSN=?1")
	Student findByStudentUSN( String usn );

	Student findByStudentName(String name);

	@Query(value="select st_student_name,st_student_usn from student where st_student_name like %?1% or st_student_usn like %?1%",nativeQuery=true)
	List<Object>[] getStudentNameAndUsnForString(String string);

	Student findByStudentNameAndStudentUSN(String name, String usn);

	@Query(value="SELECT \n" +
			"    COUNT(DISTINCT (st_student_usn))\n" +
			"FROM\n" +
			"    student st\n" +
			"        INNER JOIN\n" +
			"    student_result sr ON sr.sr_frn_st_id = st.st_id\n" +
			"        INNER JOIN\n" +
			"    (SELECT \n" +
			"        rank, sr_frn_sm_id, sr_result_month\n" +
			"    FROM\n" +
			"        (SELECT \n" +
			"        sr_frn_sm_id,\n" +
			"            sr_result_month,\n" +
			"            sm_semester_value,\n" +
			"            @rn\\:=IF(sr_frn_sm_id = @g, @rn + 1, 1) rank,\n" +
			"            @g\\:=sr_frn_sm_id\n" +
			"    FROM\n" +
			"        (SELECT \n" +
			"        sr_frn_sm_id, sr_result_month\n" +
			"    FROM\n" +
			"        student_result\n" +
			"    GROUP BY sr_frn_sm_id , sr_result_month DESC) AS r\n" +
			"    INNER JOIN semester sm ON sm.sm_id = r.sr_frn_sm_id\n" +
			"    CROSS JOIN (SELECT @rn\\:=0, @g\\:=NULL) AS i) q\n" +
			"    WHERE\n" +
			"        rank = 1 AND sm_semester_value = ?1) AS res ON res.sr_frn_sm_id = sr.sr_frn_sm_id\n" +
			"        AND res.sr_result_month = sr.sr_result_month\n"
			,nativeQuery = true)
	@Transactional
    Integer getLatestStudentsBySem(String semValue);

	@Transactional
	Long countByIsActive(int isActive);
}
