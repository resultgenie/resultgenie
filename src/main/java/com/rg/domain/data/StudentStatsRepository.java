package com.rg.domain.data;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface StudentStatsRepository extends JpaRepository<StudentStats,Integer>{

	List<StudentStats> findByStudent(Student st);

    StudentStats findByStudentAndCurrentSemester(Student st, Semester semester);
}
