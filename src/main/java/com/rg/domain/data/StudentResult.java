package com.rg.domain.data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "student_result")
public class StudentResult {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "sr_id")
    private Integer studentResultId;

    @Column(name = "sr_result_class")
    private String resultClass;

    @OneToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "sr_frn_sm_id")
    private Semester semester;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "studentResult")
    private List<TheoryPractical> theoryPractical = new ArrayList<>();

    @Column(name = "sr_backlog_count")
    private Integer backlogCount;

    @Column(name = "sr_total_score")
    private Integer totalScore;

    @Column(name = "sr_sem_aggregate")
    private Float semAggregate;

    @Column(name = "sr_result_month")
    private Long resultMonth;


    @Column(name = "sr_total_max_marks")
    private Integer maxTotalMarks;

    public Integer getMaxTotalMarks() {
        return maxTotalMarks;
    }

    public void setMaxTotalMarks(Integer maxTotalMarks) {
        this.maxTotalMarks = maxTotalMarks;
    }

    public Long getResultMonth() {
        return resultMonth;
    }

    public void setResultMonth(Long resultMonth) {
        this.resultMonth = resultMonth;
    }

    public Integer getTotalScore() {
        return totalScore;
    }

    public void setTotalScore(Integer totalScore) {
        this.totalScore = totalScore;
    }

    public Float getSemAggregate() {
        return semAggregate;
    }

    public void setSemAggregate(Float semAggregate) {
        this.semAggregate = semAggregate;
    }

    public Integer getBacklogCount() {
        return backlogCount;
    }

    public void setBacklogCount(Integer backlogCount) {
        this.backlogCount = backlogCount;
    }

    public List<TheoryPractical> getTheoryPractical() {
        return theoryPractical;
    }

    public void setTheoryPractical(List<TheoryPractical> theoryPractical) {
        this.theoryPractical = theoryPractical;
    }

    public Integer getStudentResultId() {
        return studentResultId;
    }

    public void setStudentResultId(Integer studentResultId) {
        this.studentResultId = studentResultId;
    }

    public String getResultClass() {
        return resultClass;
    }

    public void setResultClass(String resultClass) {
        this.resultClass = resultClass;
    }

    public Semester getSemester() {
        return semester;
    }

    public void setSemester(Semester semester) {
        this.semester = semester;
    }
}
