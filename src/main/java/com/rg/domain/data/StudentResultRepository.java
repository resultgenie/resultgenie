package com.rg.domain.data;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


public interface StudentResultRepository extends JpaRepository<StudentResult, Integer>{

    StudentResult findBySemester(Semester semester);

    @Query(value="select * from student_result where sr_frn_sm_id=?1 and sr_frn_st_id=?2",nativeQuery=true)
    @Transactional
    StudentResult getStudentResultBySemesterAndStudent(Integer semesterId, Integer studentId);

    @Query(value="select count(*) from \n" +
            "(select distinct(st_student_usn) from student st inner join student_result sr on st.st_id=sr" +
            ".sr_frn_st_id\n" +
            " \n" +
            "            inner join (SELECT rank, sr_frn_sm_id, sr_result_month\n" +
            "              FROM ( \n" +
            "              SELECT sr_frn_sm_id, sr_result_month, \n" +
            "                     @rn \\:= IF(sr_frn_sm_id = @g, @rn + 1, 1) rank, @g \\:= sr_frn_sm_id  \n" +
            "                FROM ( \n" +
            "                  SELECT sr_frn_sm_id, sr_result_month  \n" +
            "                    FROM student_result \n" +
            "                   GROUP BY sr_frn_sm_id, sr_result_month DESC \n" +
            "              ) AS r CROSS JOIN ( \n" +
            "                  SELECT @rn \\:= 0, @g \\:= NULL \n" +
            "              ) AS i \n" +
            "            ) q where rank = 1) as res on res.sr_frn_sm_id=sr.sr_frn_sm_id and res.sr_result_month=sr \n" +
            "            .sr_result_month where sr_sem_aggregate>=?1) as res\n"
            , nativeQuery=true)
    @Transactional
    Long getDistinctionStudents(float distinctionThreshold);

    @Query(value="select count(*) from \n" +
            "(select distinct(st_student_usn) from student st inner join student_result sr on st.st_id=sr" +
            ".sr_frn_st_id\n" +
            " \n" +
            "            inner join (SELECT rank, sr_frn_sm_id, sr_result_month\n" +
            "              FROM ( \n" +
            "              SELECT sr_frn_sm_id, sr_result_month, \n" +
            "                     @rn \\:= IF(sr_frn_sm_id = @g, @rn + 1, 1) rank, @g \\:= sr_frn_sm_id  \n" +
            "                FROM ( \n" +
            "                  SELECT sr_frn_sm_id, sr_result_month  \n" +
            "                    FROM student_result \n" +
            "                   GROUP BY sr_frn_sm_id, sr_result_month DESC \n" +
            "              ) AS r CROSS JOIN ( \n" +
            "                  SELECT @rn \\:= 0, @g \\:= NULL \n" +
            "              ) AS i \n" +
            "            ) q where rank = 2) as res on res.sr_frn_sm_id=sr.sr_frn_sm_id and res.sr_result_month=sr \n" +
            "            .sr_result_month where sr_sem_aggregate>=?1) as res\n", nativeQuery=true)
    @Transactional
    Long getPreviousSemesterDistinctionStudents(float distinctionThreshold);

    // TODO: here sr_result_month selects only latest result sem's date (for example : it selects only 4th sem result date but we also need 2nd 6th and 8th result dates)
    @Query(value="select sum(sr_result_class!='FAIL')/count(*)*100 from student_result sr inner join student st on st" +
            ".st_id=sr.sr_frn_st_id inner join (SELECT rank, \n" +
            "                          sr_frn_sm_id, \n" +
            "                          sr_result_month \n" +
            "                   FROM   (SELECT sr_frn_sm_id, \n" +
            "                                  sr_result_month, \n" +
            "                                  @rn \\:= IF(sr_frn_sm_id = @g, @rn + 1, 1) rank, \n" +
            "                                  @g \\:= sr_frn_sm_id \n" +
            "                           FROM   (SELECT sr_frn_sm_id, \n" +
            "                                          sr_result_month \n" +
            "                                   FROM   student_result \n" +
            "                                   GROUP  BY sr_frn_sm_id, \n" +
            "                                             sr_result_month DESC) AS r \n" +
            "                                  CROSS JOIN (SELECT @rn \\:= 0, \n" +
            "                                                     @g \\:= NULL) AS i) q \n" +
            "                   WHERE  rank = 1) as res on res.sr_frn_sm_id=sr.sr_frn_sm_id and res" +
            ".sr_result_month=sr.sr_result_month where st_is_active=?1\n"
            ,nativeQuery = true)
    @Transactional
    Double getCollegePerformanceOfLatestSemesters(int isActive);

    // TODO: here sr_result_month selects only latest but one result sem's date (for example : it selects only 4th sem result date but we also need 2nd 6th and 8th result dates)
    @Query(value="select sum(sr_result_class!='FAIL')/count(*)*100 from student_result sr inner join student st on st" +
            ".st_id=sr.sr_frn_st_id inner join (SELECT rank, \n" +
            "                          sr_frn_sm_id, \n" +
            "                          sr_result_month \n" +
            "                   FROM   (SELECT sr_frn_sm_id, \n" +
            "                                  sr_result_month, \n" +
            "                                  @rn \\:= IF(sr_frn_sm_id = @g, @rn + 1, 1) rank, \n" +
            "                                  @g \\:= sr_frn_sm_id \n" +
            "                           FROM   (SELECT sr_frn_sm_id, \n" +
            "                                          sr_result_month \n" +
            "                                   FROM   student_result \n" +
            "                                   GROUP  BY sr_frn_sm_id, \n" +
            "                                             sr_result_month DESC) AS r \n" +
            "                                  CROSS JOIN (SELECT @rn \\:= 0, \n" +
            "                                                     @g \\:= NULL) AS i) q \n" +
            "                   WHERE  rank = 2) as res on res.sr_frn_sm_id=sr.sr_frn_sm_id and res" +
            ".sr_result_month=sr.sr_result_month where st_is_active=?1\n",nativeQuery = true)
    @Transactional
    Double getCollegePerformanceOfPreviousSemester(int isActive);


    @Query(value="SELECT Avg(sr_sem_aggregate) \n" +
            "FROM   student_result sr \n" +
            " inner join student st on st.st_id=sr.sr_frn_st_id \n"+
            "       INNER JOIN (SELECT rank, \n" +
            "                          sr_frn_sm_id, \n" +
            "                          sr_result_month \n" +
            "                   FROM   (SELECT sr_frn_sm_id, \n" +
            "                                  sr_result_month, \n" +
            "                                  @rn \\:= IF(sr_frn_sm_id = @g, @rn + 1, 1) rank, \n" +
            "                                  @g \\:= sr_frn_sm_id \n" +
            "                           FROM   (SELECT sr_frn_sm_id, \n" +
            "                                          sr_result_month \n" +
            "                                   FROM   student_result \n" +
            "                                   GROUP  BY sr_frn_sm_id, \n" +
            "                                             sr_result_month DESC) AS r \n" +
            "                                  CROSS JOIN (SELECT @rn \\:= 0, \n" +
            "                                                     @g \\:= NULL) AS i) q \n" +
            "                   WHERE  rank = 1) AS res \n" +
            "               ON res.sr_frn_sm_id = sr.sr_frn_sm_id \n" +
            "                  AND res.sr_result_month = sr.sr_result_month and st_is_active=?1",nativeQuery = true)
    @Transactional
    Double getCurrentSemesterRgScore(int isActive);


    @Query(value = "SELECT Avg(sr_sem_aggregate) \n" +
            "FROM   student_result sr \n" +
            " inner join student st on st.st_id=sr.sr_frn_st_id \n"+
            "       INNER JOIN (SELECT rank, \n" +
            "                          sr_frn_sm_id, \n" +
            "                          sr_result_month \n" +
            "                   FROM   (SELECT sr_frn_sm_id, \n" +
            "                                  sr_result_month, \n" +
            "                                  @rn \\:= IF(sr_frn_sm_id = @g, @rn + 1, 1) rank, \n" +
            "                                  @g \\:= sr_frn_sm_id \n" +
            "                           FROM   (SELECT sr_frn_sm_id, \n" +
            "                                          sr_result_month \n" +
            "                                   FROM   student_result \n" +
            "                                   GROUP  BY sr_frn_sm_id, \n" +
            "                                             sr_result_month DESC) AS r \n" +
            "                                  CROSS JOIN (SELECT @rn \\:= 0, \n" +
            "                                                     @g \\:= NULL) AS i) q \n" +
            "                   WHERE  rank = 2) AS res \n" +
            "               ON res.sr_frn_sm_id = sr.sr_frn_sm_id \n" +
            "                  AND res.sr_result_month = sr.sr_result_month and st_is_active=?1",nativeQuery = true)
    @Transactional
    Double getPreviousSemesterRgScore(int isActive);

    @Query(value="select sum(tp.tp_external_score+tp.tp_internal_score) as total from student_result sr\n" +
            "inner join theory_practical tp \n" +
            "on\n" +
            "tp.tp_frn_sr_id=sr.sr_id\n" +
            "inner join student st \n" +
            "on\n" +
            "sr.sr_frn_st_id=st.st_id\n" +
            "inner join subject as sb \n" +
            "on\n" +
            "sb.s_id=tp.tp_frn_s_id\n" +
            "where\n" +
            "st.st_student_usn=?1\n" +
            "group by tp_id\n" +
            "order by total desc limit 1,1",nativeQuery = true)
    @Transactional
    Double getSecondBestScoreOfStudent(String usn);

    @Query(value="select sum(tp.tp_external_score+tp.tp_internal_score) as total from student_result sr\n" +
            "inner join theory_practical tp \n" +
            "on\n" +
            "tp.tp_frn_sr_id=sr.sr_id\n" +
            "inner join student st \n" +
            "on\n" +
            "sr.sr_frn_st_id=st.st_id\n" +
            "inner join subject as sb \n" +
            "on\n" +
            "sb.s_id=tp.tp_frn_s_id\n" +
            "where\n" +
            "st.st_student_usn=?1\n" +
            "group by tp_id\n" +
            "order by total asc limit 1,1",nativeQuery = true)
    @Transactional
    Double getSecondLeastScoreOfStudent(String usn);

    @Query(value="SELECT  \n" +
            "       avg( sr_sem_aggregate ) \n" +
            "FROM   student_result sr \n" +
            "       INNER JOIN student st \n" +
            "               ON st.st_id = sr.sr_frn_st_id \n" +
            "WHERE  st_student_usn = ?1 \n" +
            "       AND sr_frn_sm_id != (SELECT Max(sr_frn_sm_id) \n" +
            "                            FROM   student_result sr \n" +
            "                                   INNER JOIN student st \n" +
            "                                           ON st.st_id = sr.sr_frn_st_id \n" +
            "                            WHERE  st_student_usn = ?1) ",nativeQuery = true)
    @Transactional
    Double getPreviousSemesterAggregate(String usn);

    @Query(value="SELECT final_result.rank \n" +
            "FROM   (SELECT @ss \\:= @ss + 1 AS rank, \n" +
            "               result.sr_sem_aggregate, \n" +
            "               result.sr_frn_st_id, \n" +
            "               result.sr_frn_sm_id, \n" +
            "               result.st_student_usn \n" +
            "        FROM   (SELECT sr_sem_aggregate, \n" +
            "                       sr_frn_st_id, \n" +
            "                       sr_frn_sm_id, \n" +
            "                       st_student_usn \n" +
            "                FROM   student_result sr \n" +
            "                       INNER JOIN student st \n" +
            "                               ON st.st_id = sr.sr_frn_st_id \n" +
            "                WHERE  sr_frn_sm_id = (SELECT DISTINCT( sr_frn_sm_id ) \n" +
            "                                       FROM   student_result sr \n" +
            "                                              INNER JOIN student st \n" +
            "                                                      ON \n" +
            "                                              st.st_id = sr.sr_frn_st_id \n" +
            "                                       WHERE  st_student_usn = ?1 \n" +
            "                                       ORDER  BY sr_frn_sm_id DESC \n" +
            "                                       LIMIT  1) \n" +
            "                       AND st_frn_b_id = (SELECT st_frn_b_id \n" +
            "                                          FROM   student \n" +
            "                                          WHERE  st_student_usn = ?1) \n" +
            "                GROUP  BY st_student_usn \n" +
            "                ORDER  BY sr_sem_aggregate DESC) AS result, \n" +
            "               (SELECT @ss \\:= 0) AS ss) final_result \n" +
            "WHERE  final_result.st_student_usn = ?1",nativeQuery = true)
    @Transactional
    Integer getLatestBranchRanking(String usn);

    @Query(value="SELECT final_result.rank \n" +
            "FROM   (SELECT @ss \\:= @ss + 1 AS rank, \n" +
            "               result.sr_sem_aggregate, \n" +
            "               result.sr_frn_st_id, \n" +
            "               result.sr_frn_sm_id, \n" +
            "               result.st_student_usn \n" +
            "        FROM   (SELECT sr_sem_aggregate, \n" +
            "                       sr_frn_st_id, \n" +
            "                       sr_frn_sm_id, \n" +
            "                       st_student_usn \n" +
            "                FROM   student_result sr \n" +
            "                       INNER JOIN student st \n" +
            "                               ON st.st_id = sr.sr_frn_st_id \n" +
            "                WHERE  sr_frn_sm_id = (SELECT DISTINCT( sr_frn_sm_id ) \n" +
            "                                       FROM   student_result sr \n" +
            "                                              INNER JOIN student st \n" +
            "                                                      ON \n" +
            "                                              st.st_id = sr.sr_frn_st_id \n" +
            "                                       WHERE  st_student_usn = ?1 \n" +
            "                                       ORDER  BY sr_frn_sm_id DESC \n" +
            "                                       LIMIT  1,1) \n" +
            "                       AND st_frn_b_id = (SELECT st_frn_b_id \n" +
            "                                          FROM   student \n" +
            "                                          WHERE  st_student_usn = ?1) \n" +
            "                GROUP  BY st_student_usn \n" +
            "                ORDER  BY sr_sem_aggregate DESC) AS result, \n" +
            "               (SELECT @ss \\:= 0) AS ss) final_result \n" +
            "WHERE  final_result.st_student_usn = ?1",nativeQuery = true)
    @Transactional
    Object getPreviousBranchRanking(String usn);

    @Query(value="SELECT final_result.rank \n" +
            "FROM   (SELECT @ss \\:= @ss + 1 AS rank, \n" +
            "               result.sr_sem_aggregate, \n" +
            "               result.sr_frn_st_id, \n" +
            "               result.sr_frn_sm_id, \n" +
            "               result.st_student_usn \n" +
            "        FROM   (SELECT sr_sem_aggregate, \n" +
            "                       sr_frn_st_id, \n" +
            "                       sr_frn_sm_id, \n" +
            "                       st_student_usn \n" +
            "                FROM   student_result sr \n" +
            "                       INNER JOIN student st \n" +
            "                               ON st.st_id = sr.sr_frn_st_id \n" +
            "                WHERE  sr_frn_sm_id = (SELECT DISTINCT( sr_frn_sm_id ) \n" +
            "                                       FROM   student_result sr \n" +
            "                                              INNER JOIN student st \n" +
            "                                                      ON \n" +
            "                                              st.st_id = sr.sr_frn_st_id \n" +
            "                                       WHERE  st_student_usn = ?1 \n" +
            "                                       ORDER  BY sr_frn_sm_id DESC \n" +
            "                                       LIMIT  1) \n" +
            "                GROUP  BY st_student_usn \n" +
            "                ORDER  BY sr_sem_aggregate DESC) AS result, \n" +
            "               (SELECT @ss \\:= 0) AS ss) final_result \n" +
            "WHERE  final_result.st_student_usn = ?1 ",nativeQuery = true)
    @Transactional
    Integer getCollegeRank(String usn);

    @Query(value="SELECT final_result.rank \n" +
            "FROM   (SELECT @ss \\:= @ss + 1 AS rank, \n" +
            "               result.sr_sem_aggregate, \n" +
            "               result.sr_frn_st_id, \n" +
            "               result.sr_frn_sm_id, \n" +
            "               result.st_student_usn \n" +
            "        FROM   (SELECT sr_sem_aggregate, \n" +
            "                       sr_frn_st_id, \n" +
            "                       sr_frn_sm_id, \n" +
            "                       st_student_usn \n" +
            "                FROM   student_result sr \n" +
            "                       INNER JOIN student st \n" +
            "                               ON st.st_id = sr.sr_frn_st_id \n" +
            "                WHERE  sr_frn_sm_id = (SELECT DISTINCT( sr_frn_sm_id ) \n" +
            "                                       FROM   student_result sr \n" +
            "                                              INNER JOIN student st \n" +
            "                                                      ON \n" +
            "                                              st.st_id = sr.sr_frn_st_id \n" +
            "                                       WHERE  st_student_usn = ?1 \n" +
            "                                       ORDER  BY sr_frn_sm_id DESC \n" +
            "                                       LIMIT  1,1) \n" +
            "                GROUP  BY st_student_usn \n" +
            "                ORDER  BY sr_sem_aggregate DESC) AS result, \n" +
            "               (SELECT @ss \\:= 0) AS ss) final_result \n" +
            "WHERE  final_result.st_student_usn = ?1 ",nativeQuery = true)
    @Transactional
    Integer getPreviousSemCollegeRank(String usn);

    @Query(value="select sr_sem_aggregate,sr_backlog_count\n" +
            "from \n" +
            "student_result sr \n" +
            "inner join student st \n" +
            "on st.st_id=sr.sr_frn_st_id\n" +
            "where \n" +
            "sr_frn_sm_id = (select distinct(sr_frn_sm_id) from student_result order by sr_frn_sm_id desc limit 1,1) \n" +
            "and \n" +
            "st.st_student_usn=?1",nativeQuery = true)
    List<Object[]> getPreviousSemAggregateAndBacklogCount(String usn);

    @Query(value="select avg(sr_sem_aggregate) from student_result sr inner join student st on st.st_id=sr.sr_frn_st_id where st_student_usn=?1",nativeQuery = true)
    @Transactional
    Double getStudentAggregate(String usn);
}
