package com.rg.service;

import com.rg.domain.data.CollegeRepository;
import com.rg.service.intf.CollegeService;
import com.rg.utils.Response;
import com.rg.utils.StatusCodes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

/**
 * Created by shreeshan on 28/05/17.
 */
@Service
@Transactional
public class CollegeServiceImpl implements CollegeService {

    @Autowired
    private CollegeRepository collegeRepo;

    @Override
    public ResponseEntity<Response> getCollege() {
        Response response = new Response();
        try{
            response.setBody(collegeRepo.findAll().get(0));
            response.setCode(StatusCodes.SUCCESS);
        }
        catch(Exception e)
        {
            e.printStackTrace();
            throw e;
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
