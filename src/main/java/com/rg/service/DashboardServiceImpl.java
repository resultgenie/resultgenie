package com.rg.service;

import com.rg.domain.credentials.BranchesRepository;
import com.rg.domain.data.BranchRepository;
import com.rg.domain.data.StudentRepository;
import com.rg.domain.data.StudentResultRepository;
import com.rg.domain.data.TheoryPracticalRepository;
import com.rg.domain.data.aggregateentities.BranchWisePassPercentage;
import com.rg.domain.data.aggregateentities.BranchWisePassPercentageRepository;
import com.rg.domain.data.aggregateentities.BranchWiseResultCategoryCount;
import com.rg.domain.data.aggregateentities.BranchWiseResultCategoryCountRepository;
import com.rg.exception.CustomGenericException;
import com.rg.service.intf.DashboardService;
import com.rg.uibeans.dashboard.CurrentSemBranchResults;
import com.rg.uibeans.dashboard.DashboardHighlighterBean;
import com.rg.utils.GeneralUtils;
import com.rg.utils.Response;
import com.rg.utils.StatusCodes;
import com.rg.utils.StringConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;

/**
 * Created by shreeshan on 16/06/17.
 */
@Service
@Transactional
public class DashboardServiceImpl implements DashboardService {


    @Autowired
    BranchRepository branchRepo;

    @Autowired
    BranchesRepository branchesRepo;

    @Autowired
    StudentRepository studentRepo;

    @Autowired
    TheoryPracticalRepository theoryPracticalRepo;

    @Autowired
    StudentResultRepository studentResultRepo;

    @Autowired
    BranchWiseResultCategoryCountRepository branchWiseResultCategoryCountRepo;

    @Autowired
    BranchWisePassPercentageRepository branchWisePassPercentageRepo;



    @Autowired
    Environment env;




    @Override
    public ResponseEntity<Response> getBranchList() {
        Response response = new Response();
        try {
            response.setBody(branchesRepo.findAll());
            response.setCode(StatusCodes.SUCCESS);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Response> getBranchCount() {
        Response response = new Response();
        try {
            response.setBody(branchRepo.count());
            response.setCode(StatusCodes.SUCCESS);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Response> getDashboardHighlighterData() {
        Response response = new Response();
        DashboardHighlighterBean dashboardHighlighterBean = new DashboardHighlighterBean();
        try {
            // total branch set
            dashboardHighlighterBean.setTotalBranchesCount((int) branchRepo.count());

            // setting student count and its change
            // student count is a jugad -- needs rethinking of logic
            Long studentCount = studentRepo.countByIsActive(1);
            Integer newlyAddedStudents = studentRepo.getLatestStudentsBySem("1");
            if (newlyAddedStudents == null) {
                throw new CustomGenericException(StatusCodes.NOT_FOUND_IN_DB_OR_FAILED_TO_RETRIEVE, StringConstants
                        .NO_STUDENTS);
            }
            Integer eightSemStudents = studentRepo.getLatestStudentsBySem("8");
            if (eightSemStudents == null) {
                throw new CustomGenericException(StatusCodes.NOT_FOUND_IN_DB_OR_FAILED_TO_RETRIEVE, StringConstants
                        .NO_STUDENTS);
            }
            double percentageChangeStudentCount = GeneralUtils.percentageChangeCalculator(studentCount.doubleValue(),
                    (double) studentCount.intValue() - newlyAddedStudents + eightSemStudents,false);
            dashboardHighlighterBean.setTotalStudentsCount(studentCount.intValue());
            dashboardHighlighterBean.setChangeInTotalStudentsCount(percentageChangeStudentCount);

            // college performance and its change
            Double collegePerformance = studentResultRepo.getCollegePerformanceOfLatestSemesters(1);
            if (collegePerformance == null) {
                throw new CustomGenericException(StatusCodes.NOT_FOUND_IN_DB_OR_FAILED_TO_RETRIEVE, StringConstants
                        .FAILED_TO_CALCULATE_COLLEGE_PERFORMANCE_CURRENT_SEM);
            }

            Double collegePerformancePreviousSem = studentResultRepo.getCollegePerformanceOfPreviousSemester(1);
            if (collegePerformancePreviousSem == null) {
                throw new CustomGenericException(StatusCodes.NOT_FOUND_IN_DB_OR_FAILED_TO_RETRIEVE, StringConstants
                        .FAILED_TO_CALCULATE_COLLEGE_PERFORMANCE_PREV_SEM);
            }
            double changeInCollegePerformance = GeneralUtils.percentageChangeCalculator(collegePerformance,
                    collegePerformancePreviousSem,false);
            dashboardHighlighterBean.setCollegePerformance(collegePerformance);
            dashboardHighlighterBean.setChangeInCollegePerformance(changeInCollegePerformance);

            // rg score and its change
            Double currentRgScore = studentResultRepo.getCurrentSemesterRgScore(1);
            if (currentRgScore == null) {
                throw new CustomGenericException(StatusCodes.NOT_FOUND_IN_DB_OR_FAILED_TO_RETRIEVE, StringConstants
                        .FAILED_TO_CALC_RG_SCORE_CURRENT_SEM);
            }
            Double previousSemRgScore = studentResultRepo.getPreviousSemesterRgScore(1);
            if (previousSemRgScore == null) {
                throw new CustomGenericException(StatusCodes.NOT_FOUND_IN_DB_OR_FAILED_TO_RETRIEVE, StringConstants
                        .FAILED_TO_CALC_RG_SCORE_PREV_SEM);
            }
            double changeInRgScore = GeneralUtils.percentageChangeCalculator(currentRgScore, previousSemRgScore,false);
            dashboardHighlighterBean.setRgScore(currentRgScore / 10);
            dashboardHighlighterBean.setChangeInRgScore(changeInRgScore);

            //distinction students count and their change
            float distinctionThreshold = 70;
            Long distinctionStudentsCount = studentResultRepo.getDistinctionStudents(distinctionThreshold);
            if (distinctionStudentsCount == null) {
                throw new CustomGenericException(StatusCodes.NOT_FOUND_IN_DB_OR_FAILED_TO_RETRIEVE, StringConstants
                        .FAILED_TO_CALC_DISTINCTION_STUDENTS_COUNT);
            }
            Long previousSemesterDistinctionStudentsCount = studentResultRepo.getPreviousSemesterDistinctionStudents
                    (distinctionThreshold);
            if (previousSemesterDistinctionStudentsCount == null) {
                throw new CustomGenericException(StatusCodes.NOT_FOUND_IN_DB_OR_FAILED_TO_RETRIEVE, StringConstants
                        .FAILED_TO_CALC_DISTINCTION_STUDENTS_COUNT_PREV_SEM);
            }
            Double changeInDistinctionStudents = GeneralUtils.percentageChangeCalculator(previousSemesterDistinctionStudentsCount.doubleValue(),distinctionStudentsCount
                    .doubleValue(),false);
            dashboardHighlighterBean.setTotalDistinctionStudentCount(distinctionStudentsCount.intValue());
            dashboardHighlighterBean.setChangeInTotalDistinctionStudentCount(changeInDistinctionStudents);

            response.setBody(dashboardHighlighterBean);
            response.setCode(StatusCodes.SUCCESS);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }


    @Override
    // todo: infer keys for the map from data itself instead of hardcoding  keys in resultCount Map
    public ResponseEntity<Response> getBranchWiseStudentResultCountsAcrossCategories() throws CustomGenericException {
        String semType = env.getProperty("sem.type");
        Response response = new Response();
        List<BranchWiseResultCategoryCount> branchWiseResultCategoryCountList = new ArrayList<>();
        Map<String, CurrentSemBranchResults> currentSemBranchResultsMap = new LinkedHashMap<>();
        try {

//            List<BranchWiseResultCategoryCount> branchWiseResultCategoryCountList = branchWiseResultCategoryCountRepo
//                    .getBranchWiseResultCategoryCount();
            // jugad logic - change it once u have updated results (2017-18 results)
            Integer currentSem;
            if(semType.equals("even"))
                currentSem=2;
            else
                currentSem=1;
            for(int i=16;i>=13;i--)
            {
                List<BranchWiseResultCategoryCount> list = branchWiseResultCategoryCountRepo
                        .getBranchWiseResultCategoryCountForSemAndBatch(currentSem,i);
                branchWiseResultCategoryCountList.addAll(list);
                currentSem+=2;
            }


//            if (list == null || branchWiseResultCategoryCountList.isEmpty()) {
//                throw new CustomGenericException(StatusCodes.NOT_FOUND_IN_DB_OR_FAILED_TO_RETRIEVE, StringConstants
//                        .ERROR_IN_BRANCHWISE_RESULT_CALC);
//            }
            Map<String, List<BranchWiseResultCategoryCount>> mapGroupedByBranches = groupByBranches
                    (branchWiseResultCategoryCountList);
            for (String branchAbbreviation : mapGroupedByBranches.keySet()) {
                List<Integer> semesters = new ArrayList<>();
                LinkedHashMap<String, List<Integer>> resultCount = new LinkedHashMap<>();
                resultCount = initializeMapForResultCount(resultCount);
                CurrentSemBranchResults currentSemBranchResults = new CurrentSemBranchResults();
                for (BranchWiseResultCategoryCount bc : mapGroupedByBranches.get(branchAbbreviation)) {
                    int sem = Integer.parseInt(bc.getSemesterValue());
                    if(GeneralUtils.decideOddOrEven(sem,semType))
                    {
                        semesters.add(sem);
                        resultCount.get("FCD").add(bc.getFirstClassDistinctionCount());
                        resultCount.get("FC").add(bc.getFirstClassCount());
                        resultCount.get("SC").add(bc.getSecondClassCount());
                        resultCount.get("FAIL").add(bc.getFailuresCount());
                        currentSemBranchResults.setBranchName(bc.getBranchName());
                    }

                }
                currentSemBranchResults.setSemesters(semesters);
                currentSemBranchResults.setResultCount(resultCount);
                currentSemBranchResultsMap.put(branchAbbreviation, currentSemBranchResults);

            }
            response.setBody(currentSemBranchResultsMap);
            response.setCode(StatusCodes.SUCCESS);

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Response> getBranchWisePassPercentages() {
        Response response = new Response();
        try {

//            List<BranchWisePassPercentage> listOfResults = branchWisePassPercentageRepo.getBranchWisePassPercentages();
            // jugad logic - change it once u have updated results (2017-18 results)
            List<BranchWisePassPercentage> finalList = new ArrayList<>();
            String semType = env.getProperty("sem.type");
            Integer sem;
            if(semType.equals("even"))
                sem=2;
            else
                sem=1;
            for(int i=16;i>=13;i--)
            {
                List<BranchWisePassPercentage> list = branchWisePassPercentageRepo.getBranchWisePassPercentagesForSemAndBatch(sem,i);
                finalList.addAll(list);
                sem+=2;
            }
            response.setBody(groupByBranches(finalList));
            response.setCode(StatusCodes.SUCCESS);

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    /**
     * Initialises a map of String as key and List of Integers as value - This is to hold the count of FCD,SC,FC,FAIL
     * in all semesters
     *
     * @param resultCount
     * @return
     */
    public LinkedHashMap<String, List<Integer>> initializeMapForResultCount(LinkedHashMap<String, List<Integer>>
                                                                                    resultCount) {
        resultCount.put("FCD", new ArrayList<>());
        resultCount.put("FC", new ArrayList<>());
        resultCount.put("SC", new ArrayList<>());
        resultCount.put("FAIL", new ArrayList<>());
        return resultCount;
    }


    /**
     * Given a list of BranchWiseResultCategoryCount objects, return a map of grouped results -
     * branch names as key and respective objects as values in List
     *
     * @return
     */
    public Map groupByBranches(List branchWiseResultResults) {
        try {
            String semType = env.getProperty("sem.type");
            if (branchWiseResultResults.size() > 0) {
                if (branchWiseResultResults.get(0) instanceof BranchWiseResultCategoryCount) {
                    Map<String, List<BranchWiseResultCategoryCount>> map = new HashMap<>();
                    List<BranchWiseResultCategoryCount> branchWiseResultCategoryCountList = branchWiseResultResults;
                    branchWiseResultCategoryCountList.forEach(bc -> map.put(bc.getBranchAbbreviation(), new
                            ArrayList<>()));
                    branchWiseResultCategoryCountList.forEach(bc -> map.get(bc.getBranchAbbreviation()).add(bc));
                    return map;
                }
                if (branchWiseResultResults.get(0) instanceof BranchWisePassPercentage) {
                    Map<String, List<BranchWisePassPercentage>> map = new HashMap<>();
                    List<BranchWisePassPercentage> branchWisePassPercentageList = branchWiseResultResults;
                    branchWisePassPercentageList.forEach(bc -> map.put(bc.getBranchAbb(), new ArrayList<>()));
                    branchWisePassPercentageList.forEach(
                            bc -> {
                                if(GeneralUtils.decideOddOrEven(bc.getSemValue(),semType))
                                {
                                    map.get(bc.getBranchAbb()).add(bc);
                                }

                            });

                    return map;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return new HashMap();
    }


}
