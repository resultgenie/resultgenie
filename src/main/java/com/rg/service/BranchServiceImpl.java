package com.rg.service;

import com.rg.domain.credentials.BranchesRepository;
import com.rg.domain.data.BranchRepository;
import com.rg.domain.data.aggregateentities.*;
import com.rg.exception.CustomGenericException;
import com.rg.service.intf.BranchService;
import com.rg.utils.Response;
import com.rg.utils.StatusCodes;
import com.rg.utils.StringConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by shreeshan on 26/04/17.
 */
@Service
@Transactional
public class BranchServiceImpl implements BranchService {

    @Autowired
    BranchRepository branchRepo;

    @Autowired
    BranchesRepository branchesRepo;

    @Autowired
    BranchwiseResultRepository branchwiseResultRepo;

    @Autowired
    BranchwisePassCountRepository branchwisePassCountRepo;

    @Autowired
    Environment env;

    @SuppressWarnings("unchecked")
    @Override
    public ResponseEntity<Response> searchBranch(String query) throws CustomGenericException {
        Response response = new Response();
        try {
            if (query == null) {
                throw new CustomGenericException(StatusCodes.REQUIRED_DATA_NOT_FOUND, StringConstants
                        .REQUIRED_DATA_NOT_FOUND);
            }
            List<Object>[] branchList = branchRepo.getBranchesForString(query);
            response.setBody(branchList);
            response.setCode(StatusCodes.SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Response> getSemWiseResults() throws CustomGenericException {
        Response response = new Response();

        Map<String, Map<Long, SortedMap<String, Double>>> bwResultsMap = new HashMap<>();

        try {
            List<BranchwiseResult> listOfBranchwiseResults = branchwiseResultRepo.getBranchWiseResults();
            if (listOfBranchwiseResults == null) {
                throw new CustomGenericException(StatusCodes.NOT_FOUND_IN_DB_OR_FAILED_TO_RETRIEVE, StringConstants
                        .ERROR_IN_BRANCHWISE_RESULT_CALC);
            }

            // grouping results by sem
            Map<String, List<BranchwiseResult>> mapGroupedBySem = listOfBranchwiseResults.stream().collect
                    (Collectors.groupingBy(BranchwiseResult::getSemId));

            // preparing exoskeleton structure required by UI
            mapGroupedBySem.forEach((sem, branchWiseResult) -> bwResultsMap.put(sem, new HashMap<>()));

            // iterating over each sem and then grouping results by timestamp
            for (String sem : mapGroupedBySem.keySet()) {
                List<BranchwiseResult> listWithinSem = new ArrayList<>();
                SortedMap<Long,SortedMap<String,Double>> timestampMap = new TreeMap();
                mapGroupedBySem.get(sem).forEach(branchWiseResult -> listWithinSem.add(branchWiseResult));

                // grouping results by timestamp
                TreeMap<Long, List<BranchwiseResult>> mapGroupedByTimestamp = new TreeMap<>(listWithinSem.stream()
                        .collect
                        (Collectors.groupingBy(BranchwiseResult::getResultMonthTimestamp)));

                for (Long timestamp : mapGroupedByTimestamp.keySet()) {
                    SortedMap<String,Double> branchMap = new TreeMap();
                    mapGroupedByTimestamp.get(timestamp).forEach((k) -> branchMap.put(k.getBranchAbb(), k.getAvgResult()));
                    timestampMap.put(timestamp, branchMap);
                }
                bwResultsMap.put(sem, timestampMap);
            }
            response.setBody(bwResultsMap);
            response.setCode(StatusCodes.SUCCESS);

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Response> getSemWisePassCount() throws CustomGenericException {
        Response response = new Response();
        Map<Integer, Map<String, Double>> passCountMap = new HashMap<>();
        Set<String> branches = new HashSet<>();
        Set<Integer> semesters = new TreeSet<>();
        List<BranchwisePassCount> listOfBranchwisePassCounts = new ArrayList<>();
        String semType = env.getProperty("sem.type");
        try {
//            List<BranchwisePassCount> listOfBranchwisePassCounts = branchwisePassCountRepo.getBranchWisePassCount();
//            if (listOfBranchwisePassCounts == null) {
//                throw new CustomGenericException(StatusCodes.NOT_FOUND_IN_DB_OR_FAILED_TO_RETRIEVE, StringConstants
//                        .ERROR_IN_BRANCHWISE_RESULT_CALC);
//            }

            Integer currentSem;
            if(semType.equals("even"))
                currentSem=2;
            else
                currentSem=1;
            for(int i=16;i>=13;i--)
            {
                List<BranchwisePassCount> list1 = branchwisePassCountRepo.getBranchWisePassCountForSemAndBatch(currentSem,i);
                listOfBranchwisePassCounts.addAll(list1);
                currentSem+=2;
            }

            // branches
            listOfBranchwisePassCounts.forEach(result -> branches.add(result.getBranchAbb()));

            // semesters
            listOfBranchwisePassCounts.forEach(result -> semesters.add(result.getSemId()));

//             prepare skeletal data structure
            semesters.forEach(semester -> passCountMap.put(semester, getBranchesSkeleton(branches)));


            for (BranchwisePassCount bw : listOfBranchwisePassCounts) {

                passCountMap.get(bw.getSemId()).put(bw.getBranchAbb(), bw.getPassCount().doubleValue());
            }
            for (Map.Entry<Integer, Map<String, Double>> entry : passCountMap.entrySet()) {
                int semester = entry.getKey();
                Map<String, Double> branchPassCountMap = entry.getValue();
                int total = 0;
                List<String> branchList = new ArrayList<>(branchPassCountMap.keySet());
                for (String branch : branchList) {
                    total += branchPassCountMap.get(branch);
                }
                for (String branch : branchList) {
                    branchPassCountMap.put(branch, branchPassCountMap.get(branch) / total);
                }
                passCountMap.put(semester, branchPassCountMap);
            }
            response.setBody(passCountMap);
            response.setCode(StatusCodes.SUCCESS);

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }


    @Override
    public ResponseEntity<Response> getBranches() {
        Response r = new Response();
        r.setBody(branchRepo.findAll());
        return new ResponseEntity<>(r, HttpStatus.OK);
    }


    /**
     * Used as helper method in getTimestampSkeleton() method to create a skeleton data structure
     *
     * @param branches
     * @return
     */
    public Map<String, Double> getBranchesSkeleton(Set<String> branches) {
        Map<String, Double> branchesMap = new HashMap<>();
        branches.forEach(branch -> branchesMap.put(branch, 0d));
        return branchesMap;
    }


    /**
     * Used as a helper method in getBranchWiseResults() method to create a skeleton data structure
     *
     * @param resultsMonths
     * @param branches
     * @return
     */
    public Map<Long, Map<String, Double>> getTimestampSkeleton(Set<Long> resultsMonths, Set<String> branches) {
        Map<Long, Map<String, Double>> timestampMap = new HashMap<>();
        resultsMonths.forEach(timestamp -> timestampMap.put(timestamp, getBranchesSkeleton(branches)));
        Map sortedTimestampMap = timestampMap.entrySet().stream()
                .sorted(Map.Entry.comparingByKey())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                        (oldValue, newValue) -> oldValue, LinkedHashMap::new));
        return sortedTimestampMap;
    }


}
