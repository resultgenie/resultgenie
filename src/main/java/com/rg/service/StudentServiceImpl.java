package com.rg.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.rg.domain.data.*;
import com.rg.domain.data.aggregateentities.*;
import com.rg.exception.CustomGenericException;
import com.rg.service.intf.StudentService;
import com.rg.uibeans.student.*;
import com.rg.utils.*;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.transaction.Transactional;
import java.io.File;
import java.io.IOException;
import java.util.*;


@Service
@Transactional
public class StudentServiceImpl implements StudentService {

    private static final Logger LOG = LoggerFactory.getLogger(StudentServiceImpl.class);

    @Autowired
    private SemesterRepository semesterRepo;

    @Autowired
    private SubjectRepository subjectRepo;

    @Autowired
    private BranchRepository branchRepo;

    @Autowired
    private CollegeRepository collegeRepo;

    @Autowired
    private StudentRepository studentRepo;

    @Autowired
    private TheoryPracticalRepository theoryPracticalRepo;

    @Autowired
    private StudentResultRepository studentResultRepo;

    @Autowired
    private YearDataRepository yearDataRepo;

    @Autowired
    private StudentStatsRepository studentStatsRepo;

    @Autowired
    private StudentOverallStatsRepository studentOverallStatsRepo;

    @Autowired
    private Environment env;


    @Autowired
    private SubjectStatsRepository subjectStatsRepo;

    @Autowired
    private StudentSemesterRankRepository studentSemesterRankRepo;

    @Autowired
    private StudentPlacementBeanRepository studentPlacementBeanRepo;

    @Autowired
    private SubjectWiseRankingRepository subjectWiseRankingRepo;

    @Autowired
    private SubjectWiseRankingEntityRepository subjectWiseRankingEntityRepo;

    @Autowired
    BacklogStudentScoresRepository backlogStudentScoresRepo;

    RestTemplate restTemplate = new RestTemplate();

    @Override
    public ResponseEntity<Response> fetchResult(String collegeId) throws IOException, CustomGenericException {
        Response response = new Response();
        try {
            if (collegeId == null) {
                throw new CustomGenericException(StatusCodes.REQUIRED_DATA_NOT_FOUND, StringConstants
                        .REQUIRED_DATA_NOT_FOUND);
            }
            // fetch the string here first

//            ResponseEntity<String> result = restTemplate.getForEntity(
//                    "http://results.vtu.ac.in/vitavi.php?rid=1EW12CS085&submit=SUBMIT",
//                    String.class);
//            StudentBean sBean =
//                    ParserUtils.parseResult(result.toString());
            List<String> subjectsToIgnoreForCalc = new ArrayList<>(Arrays.asList(new String[]{"10CIP18", "10CIV18",
                    "10CIP28", "10CIV28", "14CIP18", "14CIV18", "14CIP28", "14CIV28"}));
            ObjectMapper mapper = new ObjectMapper();
            TypeFactory t = TypeFactory.defaultInstance();
            int backlogCount = 0;
            List<TheoryPractical> theoryPracticalList = null;
            File folder = new File(env.getProperty("json.data"));
            File[] files = folder.listFiles();
            DateTime date = new DateTime();
            Branch branch = null;
            int count = 0;
            StudentStats studentStat;
            SubjectStats subjectStats;
            StudentOverallStats studentOverallStat;
            Semester semester;
            Map<String, Float> usnMap = new LinkedHashMap<>();
            List<Student> studentList = new ArrayList<>();
            List<StudentStats> studentStatList = new ArrayList<>();
            List<StudentOverallStats> studentOverallStatList = new ArrayList<>();
            List<Long> timestampValues = new ArrayList<>(Arrays.asList(1298268925000l, 1329804925000l,
                    1361427325000l, 1392963325000l));
            for (File file : files) {
                LOG.info(count++ + "/" + files.length + "--" + file.getName());
                List<StudentBean> listOfStudents = mapper.readValue(file,
                        t.constructCollectionType(ArrayList.class, StudentBean.class));

                usnMap.clear();
                studentList.clear();
                studentStatList.clear();
                for (StudentBean sBean : listOfStudents) {
                    try {
                        branch = branchRepo.findByBranchAbbreviation(sBean.getUSN().substring(5, 7).trim()
                                .toLowerCase());
                        // if a new branch is added then we need to be informed abt that or
                        // else we will not be able to generate usn for that particular branch to fetch results
                    } catch (Exception e) {
                        LOG.info("Exception occurred while creating branch in 'try' , in 'catch' block now");
                        e.printStackTrace();
                    }

                    Student st = null;
                    try {

                        backlogCount = 0;
                        theoryPracticalList = new ArrayList<>();
                        st = studentRepo.findByStudentUSN(sBean.getUSN());
                        if (st == null) {
                            st = new Student();

                            // make sure colg is there in db , else create one
                            // here
                            College college = collegeRepo.findByCollegeCode(sBean.getUSN().substring(1, 3));
                            st.setCollege(college);
                            st.setStudentName(sBean.getName());
                            st.setStudentUSN(sBean.getUSN());
                            st.setIsActive(1);
                            // make sure branch is there in db , else create one
                            // here
                            st.setBranch(branch);
                            st.setJoiningYear(sBean.getUSN().substring(3, 5));
                            st = studentRepo.save(st);
                            LOG.info("No data found while fetching student , creating one now --Student "
                                    + st.getStudentName());
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        if (st == null)
                            st = new Student();
                        st.setCollege(collegeRepo.findByCollegeCode(sBean.getUSN().substring(1, 3)));
                        st.setStudentName(sBean.getName());
                        st.setStudentUSN(sBean.getUSN());
                        st.setBranch(branch);
                        st.setIsActive(1);
                        st = studentRepo.save(st);
                        LOG.info("Exception occurred while creating student in 'try' , in 'catch' block now");
                    }

                    // make sure semester is there in db , else create one here
                    semester = semesterRepo.findBySemesterValue(sBean.getSemester().toString());
                    if (semester == null) {
                        semester = new Semester();
                        semester.setSemesterValue(sBean.getSemester().toString());
                        semester = semesterRepo.save(semester);

                        YearData yearData = new YearData();
                        yearData.getSemester().add(semester);
                        yearData.setYearValue(date.getYear());
                        yearDataRepo.save(yearData);
                        LOG.info("No data found while fetching semester , creating one now == Semester "
                                + semester.getSemesterValue());
                    }

                    for (SubjectBean sb : sBean.getSubject()) {
                        TheoryPractical tp = new TheoryPractical();
                        tp.setExternalScore(sb.getExternalMarks());
                        tp.setInternalScore(sb.getInternalMarks());
                        tp.setTotalScore(sb.getExternalMarks() + sb.getInternalMarks());
                        //sb.getTheory().equals("true") ? true : false
                        tp.setIsPractical(sb.getIsLab() == 1 ? true : false);
                        tp.setResult(sb.getResult());
                        if (sb.getResult().equals("FAIL")) {
                            backlogCount++;
                        }
                        Subject subject = null;


                        try {
                            subject = subjectRepo.findBySubjectCodeAndBranch(sb.getSubjectCode(), branch);
                            if (subject == null) {
                                subject = new Subject();
                                subject.setSubjectName(sb.getName());
                                subject.setSubjectCode(sb.getSubjectCode());
                                subject.setBranch(branch);
                                subject.setSemester(semester);
                                subject.setIsLab(sb.getIsLab());
                                subject.setExternalMaxMarks(sb.getName().equals("Seminar") ? 0 : sb.getIsLab() == 1 ?
                                        50 : subjectsToIgnoreForCalc.contains(sb.getSubjectCode()) ? 50 : 100);
                                subject.setInternalMaxMarks(sb.getName().equals("Project Work") ? 100 : sb.getName()
                                        .equals("Seminar") ? 50 : 25);
                                subject.setIsRegularSubject(sb.getName().equals("Project Work") ? 0 : sb.getName()
                                        .equals("Seminar") ? 0 : subjectsToIgnoreForCalc.contains(sb.getSubjectCode()
                                ) ? 0 : 1);
                                subject.setIsRequiredForAggregateCalc(subjectsToIgnoreForCalc.contains(sb
                                        .getSubjectCode()) ? 0 : 1);
                                subject = subjectRepo.save(subject);
                                LOG.info("No data found while fetching subject , creating one now --Subject "
                                        + subject.getSubjectName());
                            }

                        } catch (Exception e) {
                            if (subject == null)
                                subject = new Subject();
                            subject.setSubjectName(sb.getName());
                            subject.setSubjectCode(sb.getSubjectCode());
                            subject = subjectRepo.save(subject);
                            LOG.info("Exception occurred while creating student in 'try' , in 'catch' block now");

                        }
                        tp.setSubject(subject);
                        tp.setSemester(semester);
                        tp.setBranch(branch);
                        theoryPracticalList.add(tp);
                        // add subject data to student result

                        // subject stats
                        subjectStats = subjectStatsRepo.findBySubjectAndBranch(subject, branch);
                        subjectStats = SubjectStatsUtils.updateSubjectBestAndLeastScore(subjectStats, tp, st);
                        subjectStatsRepo.save(subjectStats);


                    }
                    // doing this inorder to update the existing student result record, instead of adding new ones
                    StudentResult sr = studentResultRepo.getStudentResultBySemesterAndStudent(semester.getSemesterId
                            (), st.getStudentId());
                    if (sr == null)
                        sr = new StudentResult();
                    theoryPracticalList = mergeListsBasedOnSubjects(sr.getTheoryPractical(), theoryPracticalList);
                    for (TheoryPractical theoryPractical : theoryPracticalList) {
                        theoryPractical.setStudentResult(sr);
                    }
                    //here update result class based on reval results
                    // TODO: update result class, update backlog count
                    sr.setResultClass(sBean.getResultClass());
                    sr.setSemester(semester);
                    sr.setTheoryPractical(theoryPracticalList);
                    sr.setSemAggregate(StudentStatUtils.getStudentAggregate(theoryPracticalList));
                    sr.setTotalScore(StudentStatUtils.getTotalMarks());
                    sr.setMaxTotalMarks(StudentStatUtils.getTotalMaxMarks());
                    sr.setBacklogCount(backlogCount);
                    // jugad logic to set result month - instead of doing this initialise a timestamp in the beginning
                    // of this service and use that in all studentresults inorder to persist to db
                    if (st.getStudentUSN().substring(3, 5).equals("11"))
                        sr.setResultMonth(new DateTime(timestampValues.get(0)).plusMonths(Integer.parseInt(semester
                                .getSemesterValue()) * 6).getMillis());
                    else if (st.getStudentUSN().substring(3, 5).equals("12"))
                        sr.setResultMonth(new DateTime(timestampValues.get(1)).plusMonths(Integer.parseInt(semester
                                .getSemesterValue()) * 6).getMillis());
                    else if (st.getStudentUSN().substring(3, 5).equals("13"))
                        sr.setResultMonth(new DateTime(timestampValues.get(2)).plusMonths(Integer.parseInt(semester
                                .getSemesterValue()) * 6).getMillis());
                    else if (st.getStudentUSN().substring(3, 5).equals("14"))
                        sr.setResultMonth(new DateTime(timestampValues.get(3)).plusMonths(Integer.parseInt(semester
                                .getSemesterValue()) * 6).getMillis());
//                     below commented code is for re-valuation results - deal it separately
//                    if (st.getStudentResult().size() > 0) {
//                        List<StudentResult> studentResults = new ArrayList<>();
//                        for (StudentResult sResult : st.getStudentResult()) {
//                            if (sResult.getSemester().getSemesterValue().equals(sr.getSemester().getSemesterValue()
// )) {
//                                studentResults.add(sr);
//                            } else
//                                studentResults.add(sResult);
//                        }
//                        st.setStudentResult(studentResults);
//                    } else
                    sr.setTheoryPractical(theoryPracticalList);
                    st.getStudentResult().add(sr);
                    studentList.add(st);
                    sr = studentResultRepo.save(sr);
//                    st = studentRepo.save(st);

                    /**
                     * Add Aggregate with usn to the map 'usnMap'
                     */
                    usnMap.put(st.getStudentUSN(), sr.getSemAggregate());

                    /**
                     * Set student stats
                     */
                    studentStat = studentStatsRepo.findByStudentAndCurrentSemester(st, semester);
                    if (studentStat == null) {
                        studentStat = new StudentStats();
                    }
                    studentStat.setCurrentSemester(semester);
                    studentStat.setAggregate(sr.getSemAggregate());
                    studentStat.setBranch(branch);
                    StudentStatUtils.setBestSubject(studentStat);
                    StudentStatUtils.setWorstSubject(studentStat);
                    studentStat.setStudent(st);


                    /**
                     * This zone considers only backlogs of this semester(not
                     * the total backlog count of a student) and agg of this
                     * semester As a result , student zone will vary for each
                     * semester
                     */
                    studentStat.setStudentZone(
                            StudentStatUtils.getStudentZoneForSemesterAgg(studentStat.getAggregate(), backlogCount));
                    studentStat.setBranch(branch);
                    studentStat.setCurrentYear(StudentStatUtils.setCurrentYearForSemester(semester));
                    studentStat.setCurrentSemStudentResult(st.getStudentResult().get(st.getStudentResult().size() - 1));
                    studentStatList.add(studentStat);

                    /**
                     * Setting student overall best/least scores
                     */
                    studentOverallStat = studentOverallStatsRepo.findByStudent(st);
                    studentOverallStat = StudentStatUtils.setOverallBestSubject(studentStat, studentOverallStat);
                    studentOverallStat = StudentStatUtils.setOverallWorstSubject(studentStat, studentOverallStat);
                    studentOverallStat.setStudent(st);
//                    studentOverallStatList.add(studentOverallStat);
                    studentOverallStatsRepo.save(studentOverallStat);

                }

                /**
                 * Setting Student rank for the current semester
                 */
                usnMap = StudentStatUtils.sortByValue(usnMap);
                List<String> list = new ArrayList<>(usnMap.keySet());
                String usn;
                for (StudentStats s : studentStatList) {
                    usn = s.getStudent().getStudentUSN();
                    //since index starts from '0' adding '1' to it ( as we want our rank to start from 1)
                    s.setCurrentRanking(list.indexOf(usn) + 1);
                }
                try {
                    for (Student student : studentList) {
                        if (student.getStudentResult().get(student.getStudentResult().size() - 1).getSemester()
                                .getSemesterValue().equals("8"))
                            student.setIsActive(0);
                    }
                    studentList = studentRepo.save(studentList);
                    studentStatsRepo.save(studentStatList);
//                    studentOverallStatsRepo.save(studentOverallStatList);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                response.setBody(StringConstants.RESULTS_SAVED_IN_DB);
                response.setCode(StatusCodes.SUCCESS);
            }

            LOG.info("Done processing students. Now ranking subjects with respect to college,branch,semester");

            Map<String, SubjectWiseRanking> rankMap = new HashMap<>();

            // latest result subject ranking
            LOG.info("Fetching college rank");
            List<SubjectWiseRankingEntity> rankingWrtCollege = subjectWiseRankingEntityRepo
                    .getSubjectRankingWrtCollege();
            if ((rankingWrtCollege == null || rankingWrtCollege.size() == 0)) {
                throw new CustomGenericException(StatusCodes.NOT_FOUND_IN_DB_OR_FAILED_TO_RETRIEVE,
                        StringConstants.FAILED_TO_RETRIEVE_SUBJECT_RANK_WRT_COLLEGE);
            }
            rankingWrtCollege.forEach(obj -> {
                SubjectWiseRanking sr;
                String key = obj.getSubjectCode() + ";" + obj.getBranch();
                if (rankMap.get(key) == null) {
                    sr = new SubjectWiseRanking();
                } else {
                    sr = rankMap.get(key);
                }
                sr.setTotalSubjectsInCollege(obj.getTotalCount());
                sr.setRankInCollege(obj.getRank());
                sr.setBranch(obj.getBranch());
                sr.setSubjectCode(obj.getSubjectCode());
                sr.setSubjectName(obj.getSubjectName());
                sr.setSubjectPerformance(obj.getSubjectPerformance());
                rankMap.put(key, sr);
            });

            List<Semester> semesters = semesterRepo.findAll();
            for (Semester sem : semesters) {
                LOG.info("Fetching semester rank -- " + sem.getSemesterValue());
                List<SubjectWiseRankingEntity> rankingWrtSemester = subjectWiseRankingEntityRepo
                        .getSubjectRankingWrtSemester
                                (sem.getSemesterValue());
                if ((rankingWrtSemester == null || rankingWrtSemester.size() == 0)) {
                    throw new CustomGenericException(StatusCodes.NOT_FOUND_IN_DB_OR_FAILED_TO_RETRIEVE,
                            StringConstants.FAILED_TO_RETRIEVE_SUBJECT_RANK_WRT_SEMESTER + " -- " + sem
                                    .getSemesterValue());
                }
                rankingWrtSemester.forEach(obj -> {
                    SubjectWiseRanking sr;
                    String key = obj.getSubjectCode() + ";" + obj.getBranch();
                    if (rankMap.get(key) == null) {
                        sr = new SubjectWiseRanking();
                        sr.setBranch(obj.getBranch());
                        sr.setSubjectCode(obj.getSubjectCode());
                        sr.setSubjectName(obj.getSubjectName());
                        sr.setSubjectPerformance(obj.getSubjectPerformance());
                    } else {
                        sr = rankMap.get(key);
                    }
                    sr.setTotalSubjectsInSemester(obj.getTotalCount());
                    sr.setRankInSemester(obj.getRank());
                    rankMap.put(key, sr);
                });

            }

            List<Branch> branchesList = branchRepo.findAll();
            for (Branch b : branchesList) {
                LOG.info("Fetching branch rank -- " + b.getBranchAbbreviation());
                List<SubjectWiseRankingEntity> rankingWrtBranch = subjectWiseRankingEntityRepo
                        .getSubjectRankingWrtBranch(b
                                .getBranchAbbreviation());
                if ((rankingWrtBranch == null || rankingWrtBranch.size() == 0)) {
                    throw new CustomGenericException(StatusCodes.NOT_FOUND_IN_DB_OR_FAILED_TO_RETRIEVE,
                            StringConstants.FAILED_TO_RETRIEVE_SUBJECT_RANK_WRT_BRANCH + " -- " + b
                                    .getBranchAbbreviation());
                }
                rankingWrtBranch.forEach(obj -> {
                    SubjectWiseRanking sr;
                    String key = obj.getSubjectCode() + ";" + obj.getBranch();
                    if (rankMap.get(key) == null) {
                        sr = new SubjectWiseRanking();
                        sr.setBranch(obj.getBranch());
                        sr.setSubjectCode(obj.getSubjectCode());
                        sr.setSubjectName(obj.getSubjectName());
                        sr.setSubjectPerformance(obj.getSubjectPerformance());
                    } else {
                        sr = rankMap.get(key);
                    }
                    sr.setTotalSubjectsInBranch(obj.getTotalCount());
                    sr.setRankInBranch(obj.getRank());
                    rankMap.put(key, sr);
                });

                subjectWiseRankingRepo.save(rankMap.values());
            }

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
        LOG.info("-----------SUCCESS--------------");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }


    @SuppressWarnings("unchecked")
    @Override
    public ResponseEntity<Response> getStudentStat(String name) throws CustomGenericException {
        Response response = new Response();
        try {
            if (name == null) {
                throw new CustomGenericException(StatusCodes.REQUIRED_DATA_NOT_FOUND, StringConstants
                        .REQUIRED_DATA_NOT_FOUND);
            }
            Student st = studentRepo.findByStudentName(name);
            if (st == null) {
                throw new CustomGenericException(StatusCodes.NOT_FOUND_IN_DB_OR_FAILED_TO_RETRIEVE, StringConstants
                        .NO_STUDENT);
            }
            response.setBody(studentStatsRepo.findByStudent(st));
            response.setCode(StatusCodes.SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }


    @SuppressWarnings("unchecked")
    @Override
    public ResponseEntity<Response> searchStudent(String query) throws CustomGenericException {
        Response response = new Response();
        try {
            if (query == null) {
                throw new CustomGenericException(StatusCodes.REQUIRED_DATA_NOT_FOUND, StringConstants
                        .REQUIRED_DATA_NOT_FOUND);
            }
            List<Object>[] studentList = studentRepo.getStudentNameAndUsnForString(query);
            response.setBody(studentList);
            response.setCode(StatusCodes.SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Response> getStudent(String usn) throws CustomGenericException {
        Response response = new Response();
        try {
            Student st = studentRepo.findByStudentUSN(usn);
            if (st == null) {
                throw new CustomGenericException(StatusCodes.NOT_FOUND_IN_DB_OR_FAILED_TO_RETRIEVE, StringConstants
                        .NO_STUDENT);
            }
            StudentStats studentStats = studentStatsRepo.findByStudentAndCurrentSemester(st, st.getStudentResult()
                    .get(st.getStudentResult().size() - 1).getSemester());
            if (studentStats == null) {
                throw new CustomGenericException(StatusCodes.NOT_FOUND_IN_DB_OR_FAILED_TO_RETRIEVE, StringConstants
                        .NO_STUDENT_STATS);
            }
            StudentOverallStats studentOverallStats = studentOverallStatsRepo.findByStudent(st);
            if (studentOverallStats == null) {
                throw new CustomGenericException(StatusCodes.NOT_FOUND_IN_DB_OR_FAILED_TO_RETRIEVE, StringConstants
                        .NO_STUDENT_OVERALL_STATS);
            }

            //mapping
            StudentViewBean svBean = new StudentViewBean();
            StudentDetails sd = new StudentDetails();
            SubjectScore bestSubjectScore = new SubjectScore();
            SubjectScore worstSubjectScore = new SubjectScore();
            List<StudentResultUI> studentResultUIList = new ArrayList<>();

            // branch rank
            Integer latestSemBranchRanking = studentResultRepo.getLatestBranchRanking(usn);
            if (latestSemBranchRanking == null) {
                latestSemBranchRanking = 0;
//                throw new CustomGenericException(StatusCodes.NOT_FOUND_IN_DB_OR_FAILED_TO_RETRIEVE, StringConstants
// .FAILED_TO_RETRIEVE_BRANCH_RANK_LATEST_SEM);
            }
            Object previousSemBranchRanking = studentResultRepo.getPreviousBranchRanking(usn);
            if (previousSemBranchRanking == null) {
                previousSemBranchRanking = 0;
//                throw new CustomGenericException(StatusCodes.NOT_FOUND_IN_DB_OR_FAILED_TO_RETRIEVE, StringConstants
// .FAILED_TO_RETRIEVE_BRANCH_RANK_PREV_SEM);
            }
            Double changeInBranchRanking = GeneralUtils.percentageChangeCalculator(latestSemBranchRanking.doubleValue
                    (), new Double(previousSemBranchRanking.toString()), true);
            svBean.setBranchRank(latestSemBranchRanking);
            svBean.setChangeInBranchRank(changeInBranchRanking);

            // aggregate
            Double semAggregate = studentResultRepo.getStudentAggregate(usn);
            if (semAggregate == null) {
                semAggregate = 0d;
//                throw new CustomGenericException(StatusCodes.NOT_FOUND_IN_DB_OR_FAILED_TO_RETRIEVE, StringConstants
// .FAILED_TO_RETRIEVE_STUDENT_AGGREGATE);
            }
            svBean.setAggregate(semAggregate);
            Double previousSemAggregate = studentResultRepo.getPreviousSemesterAggregate(usn);
            if (previousSemAggregate == null) {
                previousSemAggregate = 0d;
//                throw new CustomGenericException(StatusCodes.NOT_FOUND_IN_DB_OR_FAILED_TO_RETRIEVE, StringConstants
// .FAILED_TO_RETRIEVE_SEM_AGGREGATE_PREV_SEM);
            }
            Double percentageChangeInAggregate = GeneralUtils.percentageChangeCalculator(semAggregate,
                    previousSemAggregate, false);
            svBean.setChangeInAggregate(percentageChangeInAggregate.floatValue());

            // college rank
            Integer rankingInCollegeAtPresent = studentResultRepo.getCollegeRank(usn);
            if (rankingInCollegeAtPresent == null) {
                rankingInCollegeAtPresent = 0;
//                throw new CustomGenericException(StatusCodes.NOT_FOUND_IN_DB_OR_FAILED_TO_RETRIEVE, StringConstants
// .FAILED_TO_CALC_COLLEGE_RANK_CURR_SEM);
            }
            Integer previousSemCollegeRank = studentResultRepo.getPreviousSemCollegeRank(usn);
            if (previousSemCollegeRank == null) {
                previousSemCollegeRank = 0;
//                throw new CustomGenericException(StatusCodes.NOT_FOUND_IN_DB_OR_FAILED_TO_RETRIEVE, StringConstants
// .FAILED_TO_CALC_COLLEGE_RANK_PREV_SEM);
            }
            Double changeInCollegeRank = GeneralUtils.percentageChangeCalculator(rankingInCollegeAtPresent
                    .doubleValue(), previousSemCollegeRank.doubleValue(), true);
            svBean.setCollegeRank(rankingInCollegeAtPresent);
            svBean.setChangeInCollegeRank(changeInCollegeRank);


            //backlog count
            Integer backlogCount = theoryPracticalRepo.getStudentActiveBacklogs(usn);

            // student details
            sd.setStudentName(st.getStudentName());
            sd.setStudentUsn(st.getStudentUSN());
            sd.setStudentBacklogs(backlogCount);
            sd.setStudentBranch(st.getBranch().getBranchValue());
            sd.setStudentCurrentSemester(studentStats.getCurrentSemester().getSemesterValue());
            sd.setStudentCurrentYear(studentStats.getCurrentYear());
            sd.setStudentStatus(st.getIsActive() == 1 ? "Active" : "Passed Out");

            // student type
            String subUsn = usn.substring(usn.length() - 3);
            sd.setStudentType((subUsn.startsWith("4")) ? "DIPLOMA" : "REGULAR");

            // student ratings
            sd.setStudentRatings(new Double(studentStats.getCurrentSemStudentResult().getSemAggregate()) / 20);

            svBean.setStudentDetails(sd);
            for (StudentResult studentResult : studentStats.getStudent().getStudentResult()) {
                int count = 1;
                StudentResultUI studentResultUI = new StudentResultUI();
                List<StudentSubjects> studentSubjectsList = new ArrayList<>();
                studentResultUI.setResultClass(studentResult.getResultClass());
                studentResultUI.setAggregate(studentResult.getSemAggregate());
                studentResultUI.setSemester(studentResult.getSemester().getSemesterValue());
                studentResultUI.setTotalMarks(studentResult.getTotalScore());
                studentResultUI.setMaxTotalMarks(studentResult.getMaxTotalMarks());
                for (TheoryPractical tp : studentResult.getTheoryPractical()) {
                    StudentSubjects studentSubject = new StudentSubjects();
                    studentSubject.setId(count++);
                    studentSubject.setTotalMarks(tp.getTotalScore());
                    studentSubject.setExternalMarks(tp.getExternalScore());
                    studentSubject.setInternalMarks(tp.getInternalScore());
                    studentSubject.setRequiredForRadar(tp.getSubject().getSubjectName().equals("Seminar") ? false :
                            true);
                    studentSubject.setSubjectCode(tp.getSubject().getSubjectCode());
                    studentSubject.setSubjectName(tp.getSubject().getSubjectName());
                    studentSubject.setSubjectResultClass(tp.getResult());
                    studentSubject.setExternalMaxMarks(tp.getSubject().getExternalMaxMarks());
                    studentSubject.setInternalMaxMarks(tp.getSubject().getInternalMaxMarks());
                    studentSubjectsList.add(studentSubject);
                }
                studentResultUI.setStudentSubjectsList(studentSubjectsList);
                studentResultUIList.add(studentResultUI);
            }
            svBean.setStudentResultUIList(studentResultUIList);
            // student zone
            svBean.setStudentZone(studentStats.getStudentZone());
            List<Object[]> previousSemAggregateAndBacklog = studentResultRepo.getPreviousSemAggregateAndBacklogCount
                    (usn);
            if (previousSemAggregateAndBacklog == null || previousSemAggregateAndBacklog.size() == 0) {
                Object[] arr = new Object[2];
                arr[0] = 0;
                arr[1] = 0;
                previousSemAggregateAndBacklog.add(arr);
//                throw new CustomGenericException(StatusCodes.NOT_FOUND_IN_DB_OR_FAILED_TO_RETRIEVE, StringConstants
// .FAILED_TO_CALC_BACKLOG_PREV_SEM);
            }
            String previousSemStudentZone = StudentStatUtils.getStudentZoneForSemesterAgg(new Float
                    (previousSemAggregateAndBacklog.get(0)[0].toString()), Integer.parseInt
                    (previousSemAggregateAndBacklog.get(0)[1].toString()));
            svBean.setChangeInStudentZone(previousSemStudentZone.equals(studentStats.getStudentZone()) ? "--" :
                    previousSemStudentZone);

            // best score
            bestSubjectScore.setScore(studentOverallStats.getOverallBestScore());
            bestSubjectScore.setSubjectName(studentOverallStats.getOverallBestScoreSubject().getSubjectName());
            bestSubjectScore.setChangeInScore(studentOverallStats.getChangeInBestScore());
            svBean.setBestScore(bestSubjectScore);

            // least score
            worstSubjectScore.setScore(studentOverallStats.getOverallWorstScore());
            worstSubjectScore.setSubjectName(studentOverallStats.getOverallWorstScoreSubject().getSubjectName());
            worstSubjectScore.setChangeInScore(studentOverallStats.getChangeInWorstScore());
            svBean.setLeastScore(worstSubjectScore);


            // student backlog subjects
            List<BacklogStudentScores> studentBacklogs = backlogStudentScoresRepo
                    .getStudentBacklogs("PASS", usn);

            studentBacklogs.forEach(obj -> {
                if (obj.getStudentSem().equals(obj.getSubjectSem()))
                    obj.setIsActiveBacklog(1);
                else
                    obj.setIsActiveBacklog(0);
            });
            svBean.setBacklogSubjects(studentBacklogs);

            response.setBody(svBean);
            response.setCode(StatusCodes.SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Response> getStudentsWithRankAcrossBranches(String year, String semester) throws
            CustomGenericException {
        Response response = new Response();
        try {
            if (year == null || semester == null) {
                throw new CustomGenericException(StatusCodes.REQUIRED_DATA_NOT_FOUND, StringConstants
                        .REQUIRED_DATA_NOT_FOUND);
            }
            List<StudentSemesterRank> results = studentSemesterRankRepo.getStudentsWithRankAcrossSemesters(year,
                    semester);
            if (results == null || results.size() == 0) {
                throw new CustomGenericException(StatusCodes.NOT_FOUND_IN_DB_OR_FAILED_TO_RETRIEVE, StringConstants
                        .NO_STUDENTS);
            }
            int rank = 1;
            for (StudentSemesterRank ssr : results) {
                ssr.setRank(rank++);
            }
            response.setBody(results);
            response.setCode(StatusCodes.SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Response> getStudentsForPlacements(PlacementCriteriaBean placementCriteriaBean) throws
            CustomGenericException {
        Response response = new Response();
        PlacementUIBean uiBean = new PlacementUIBean();
        PlacementHighlighterUI highlighterUI = new PlacementHighlighterUI();
        Map<String, Integer> branchDistributionMap = new HashMap<>();
        Map<String, Integer> classDistributionMap = new HashMap<>();
        try {
            if (placementCriteriaBean == null) {
                throw new CustomGenericException(StatusCodes.REQUIRED_DATA_NOT_FOUND, StringConstants
                        .REQUIRED_DATA_NOT_FOUND);
            }
            List<StudentPlacementBean> results = null;
            if (placementCriteriaBean.getNoActiveBacklog() != null && placementCriteriaBean.getNoActiveBacklog() == 1) {
                results = studentPlacementBeanRepo.getStudentsForPlacementsNoForBacklog
                        (placementCriteriaBean.getSemesters(), placementCriteriaBean.getBranches(),
                                placementCriteriaBean
                                        .getAggregate(), 0);
            } else {
                results = studentPlacementBeanRepo.getStudentsForPlacements
                        (placementCriteriaBean.getSemesters(), placementCriteriaBean.getBranches(),
                                placementCriteriaBean
                                        .getAggregate());
            }
            if (results == null || results.size() == 0) {
                throw new CustomGenericException(StatusCodes.NOT_FOUND_IN_DB_OR_FAILED_TO_RETRIEVE, StringConstants
                        .NO_STUDENTS);
            }
            // ranking results
            int rank = 1;
            for (StudentPlacementBean spb : results) {
                spb.setRank(rank++);
            }
            uiBean.setEligibleStudents(results);
            for (StudentPlacementBean bean : results) {
                branchDistributionMap.putIfAbsent(bean.getBranch(), 0);
                branchDistributionMap.put(bean.getBranch(), branchDistributionMap.get(bean.getBranch()) + 1);

                classDistributionMap.putIfAbsent(bean.getResultClass(), 0);
                classDistributionMap.put(bean.getResultClass(), classDistributionMap.get(bean.getResultClass()) + 1);
            }
            uiBean.setBranchDistribution(branchDistributionMap);
            uiBean.setClassDistribution(classDistributionMap);

            highlighterUI.setBestStudent(results.get(0));
            highlighterUI.setEligibleCandidates(results.size());
            highlighterUI.setTotalBranches((int) branchRepo.count());

            Double currentRgScore = studentResultRepo.getCurrentSemesterRgScore(1);
            Double previousSemRgScore = studentResultRepo.getPreviousSemesterRgScore(1);
            if (previousSemRgScore == null) {
                throw new CustomGenericException(StatusCodes.NOT_FOUND_IN_DB_OR_FAILED_TO_RETRIEVE, StringConstants
                        .FAILED_TO_CALC_RG_SCORE_PREV_SEM);
            }
            double changeInRgScore = GeneralUtils.percentageChangeCalculator(currentRgScore, previousSemRgScore, false);
            highlighterUI.setRgScore(currentRgScore / 10);
            highlighterUI.setChangeInRgScore(changeInRgScore);

            //total candidates
            Integer totalCandidates = studentPlacementBeanRepo.getTotalStudentCount(placementCriteriaBean
                    .getSemesters(), placementCriteriaBean.getBranches());
            highlighterUI.setTotalCandidates(totalCandidates);

            // college performance and its change
            Double collegePerformance = studentResultRepo.getCollegePerformanceOfLatestSemesters(1);
            Double collegePerformancePreviousSem = studentResultRepo.getCollegePerformanceOfPreviousSemester(1);
            if (collegePerformancePreviousSem == null) {
                throw new CustomGenericException(StatusCodes.NOT_FOUND_IN_DB_OR_FAILED_TO_RETRIEVE, StringConstants
                        .FAILED_TO_CALCULATE_COLLEGE_PERFORMANCE_PREV_SEM);
            }
            double changeInCollegePerformance = GeneralUtils.percentageChangeCalculator(collegePerformance,
                    collegePerformancePreviousSem, false);
            highlighterUI.setCollegePerformance(collegePerformance);
            highlighterUI.setChangeInCollegePerformance(changeInCollegePerformance);
            uiBean.setHighlighter(highlighterUI);
            response.setBody(uiBean);
            response.setCode(StatusCodes.SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }


    private List<TheoryPractical> mergeListsBasedOnSubjects(List<TheoryPractical> theoryPracticalExistingList,
                                                            List<TheoryPractical> theoryPracticalNewList) {
        Map<String, TheoryPractical> newMap = new HashMap<>();
        Map<String, TheoryPractical> existingMap = new HashMap<>();
        theoryPracticalNewList.forEach(k -> newMap.put(k.getSubject().getSubjectCode(), k));
        theoryPracticalExistingList.forEach(k -> existingMap.put(k.getSubject().getSubjectCode(), k));
        List<TheoryPractical> newList = new ArrayList<>();
        if (theoryPracticalExistingList.size() > 0) {
            for (TheoryPractical tp : theoryPracticalExistingList) {
                if (newMap.containsKey(tp.getSubject().getSubjectCode()))
                    newList.add(mergeSubjects(newMap.get(tp.getSubject().getSubjectCode()), existingMap.get(tp
                            .getSubject().getSubjectCode())));
                else
                    newList.add(existingMap.get(tp.getSubject().getSubjectCode()));

            }
        } else
            newList.addAll(theoryPracticalNewList);
        return newList;
    }

    private TheoryPractical mergeSubjects(TheoryPractical newTp, TheoryPractical existingTp) {
        newTp.setTheoryPracticalId(existingTp.getTheoryPracticalId());
        return newTp;
    }


//  @SuppressWarnings("unchecked")
//  public ResponseEntity<Response> search(String text) {
//      Response response = new Response();
//      try {
//      	if (text == null)
//      	{
//      		throw new CustomGenericException("444", "required parameters not found");
//      	}
//          if (text.matches("^[0-9]{1}[A-Za-z]{2}[0-9]{2}[A-z]+[0-9]{3}$")) {
//              Student st = studentRepo.findByStudentUSN(text);
//              jsonObject.put("data", st);
//              return new ResponseEntity<>(response, HttpStatus.OK);
//          }

//          FullTextEntityManager fullTextEntityManager = searchUtils.getFullTextEntityManager();
//          //fullTextEntityManager.createIndexer().startAndWait();
//
//          fullTextEntityManager = searchUtils.getFullTextEntityManager();
//          QueryBuilder q = fullTextEntityManager.getSearchFactory().buildQueryBuilder().forEntity(Student.class)
//                  .get();
//          org.apache.lucene.search.Query luceneQuery = q.keyword().onFields("studentName").matching(text)
//                  .createQuery();
//          javax.persistence.Query jpaQuery = fullTextEntityManager.createFullTextQuery(luceneQuery, Student.class);
//          List<Student> list = jpaQuery.getResultList();
//          jsonObject.put("data", list);
//
//      } catch (Exception e) {
//          e.printStackTrace();
//      }
//      return new ResponseEntity<>(response, HttpStatus.OK);
//  }

//  @SuppressWarnings("unchecked")
//  @Override
//  public ResponseEntity<Response> autoCompleteFeature() {
//      Response response = new Response();
//      Set<String> set = new HashSet<String>();
//      String columns = "st_student_name,s_subject_name";
//      String tables = "student,subject";
//      Connection connection = null;
//      try {
//          connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/resultgenie", "root", "root");
//          Class.forName("com.mysql.jdbc.Driver");
//          Statement stmt = connection.createStatement();
//          ResultSet resultSet = stmt.executeQuery("select " + columns + " from " + tables);
//
//          while (resultSet.next()) {
//              for (String s : columns.split(",")) {
//                  if (!(resultSet.getString(s) == null)) {
//                      set.add(resultSet.getString(s));
//                  }
//              }
//          }
//          jsonObject.put("set", set);
//      } catch (Exception e) {
//          e.printStackTrace();
//      } finally {
//          try {
//              connection.close();
//          } catch (SQLException e) {
//              e.printStackTrace();
//          }
//      }
//      return new ResponseEntity<>(response, HttpStatus.OK);
//  }


}
