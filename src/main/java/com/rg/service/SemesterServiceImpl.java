package com.rg.service;

import com.rg.domain.data.aggregateentities.ClassResult;
import com.rg.domain.data.aggregateentities.ClassResultRepository;
import com.rg.domain.data.aggregateentities.YearSemesterBranchCombination;
import com.rg.domain.data.aggregateentities.YearSemesterBranchCombinationRepository;
import com.rg.exception.CustomGenericException;
import com.rg.service.intf.SemesterService;
import com.rg.uibeans.semester.BranchSemestersUI;
import com.rg.uibeans.semester.ClassResultsUI;
import com.rg.uibeans.semester.SubjectUI;
import com.rg.utils.Response;
import com.rg.utils.StatusCodes;
import com.rg.utils.StringConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

/**
 * Created by shreeshan on 26/04/17.
 */
@Service
@Transactional
public class SemesterServiceImpl implements SemesterService {


    @Autowired
    private ClassResultRepository classResultRepo;

    @Autowired
    private YearSemesterBranchCombinationRepository yearSemesterBranchCombinationRepo;


    @Override
    public ResponseEntity<Response> getWholeClassResults(String year, String branchAbb, String semesterValue) throws
            CustomGenericException {
        Response response = new Response();
        try {
            if (year == null || branchAbb == null || semesterValue == null) {
                throw new CustomGenericException(StatusCodes.REQUIRED_DATA_NOT_FOUND, StringConstants
                        .REQUIRED_DATA_NOT_FOUND);
            }
            List<ClassResult> classResults = classResultRepo.getClassResults(year, branchAbb, semesterValue);
            if (classResults == null || classResults.size() == 0) {
                throw new CustomGenericException(StatusCodes.NOT_FOUND_IN_DB_OR_FAILED_TO_RETRIEVE, StringConstants
                        .NO_RESULTS_FOR_CLASS);
            }
            response.setBody(groupResultsByStudentAndGetResultCategoryStat(classResults));
            response.setCode(StatusCodes.SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Response> getCombinationOfSemestersYearsBranches(Boolean includeBranch) throws
            CustomGenericException {
        Response response = new Response();
        Map<String, Map<String, BranchSemestersUI>> combinationsForUI = new LinkedHashMap<>();
        Map<String,Set<String>> combinationsForUIWithoutBranch = new LinkedHashMap<>();

        // this map contains mapping between branch abbreviation as key and full form as value
        Map<String, String> branchMap = new HashMap<>();

        try {
            if(includeBranch == null)
            {
                throw new CustomGenericException(StatusCodes.REQUIRED_DATA_NOT_FOUND, StringConstants
                        .REQUIRED_DATA_NOT_FOUND);
            }
            List<YearSemesterBranchCombination> combinations = yearSemesterBranchCombinationRepo
                    .getCombinationOfAvailableData();

            //populate map
            combinations.forEach(k -> branchMap.put(k.getBranch(), k.getBranchFullform()));


            // grouping db results by year
            Map<String, List<YearSemesterBranchCombination>> mapGroupedByYear = combinations.stream().collect
                    (Collectors.groupingBy(YearSemesterBranchCombination::getYear));


            // populating combinations map with year as main key
            mapGroupedByYear.forEach((year, yearSemesterBranchCombination) -> combinationsForUI.put(year, new
                    LinkedHashMap<>()));

            for (String year : mapGroupedByYear.keySet()) {

                if(includeBranch){

                    //grouping db results by branch
                    Map<String, List<YearSemesterBranchCombination>> mapGroupedByBranch = mapGroupedByYear.get(year)
                            .stream().collect(Collectors.groupingBy(YearSemesterBranchCombination::getBranch));
                    for (String branch : mapGroupedByBranch.keySet()) {
                        List<String> semesters = new ArrayList<>();
                        BranchSemestersUI branchSemestersUI = new BranchSemestersUI();
                        // creating semesters array for each branch under each year
                        mapGroupedByBranch.get(branch).forEach((yearSemesterBranchCombination) -> semesters.add
                                (yearSemesterBranchCombination.getSemester()));
                        branchSemestersUI.setBranchName(branchMap.get(branch));
                        branchSemestersUI.setSemesters(semesters);
                        combinationsForUI.get(year).put(branch, branchSemestersUI);
                    }
                }
                else{
                    Set<String> semestersWithoutBranches = new HashSet<>();
                    mapGroupedByYear.get(year).forEach(k->semestersWithoutBranches.add(k.getSemester()));
                    combinationsForUIWithoutBranch.put(year,semestersWithoutBranches);
                }
            }

            response.setBody(includeBranch?combinationsForUI:combinationsForUIWithoutBranch);
            response.setCode(StatusCodes.SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
        return new ResponseEntity<>(response, HttpStatus.OK);

    }

    /**
     * Helper method for getWholeClassResults()
     *
     * @param classResults
     * @return
     */
    private Map<String, Object> groupResultsByStudentAndGetResultCategoryStat(List<ClassResult> classResults) {
        Map<String, ClassResultsUI> classResultsMap = new LinkedHashMap<>();
        Map<String, AtomicLong> categoryCount = new LinkedHashMap<>();
        Map<String, Integer> subjectPassPerformance = new LinkedHashMap<>();
        Map<String, Integer> subjectCount = new LinkedHashMap<>();
        Map<String, Object> finalMap = new LinkedHashMap<>();
        List<String> subjectsForGivenSemester = new ArrayList<>();
        Map<String, String> mapForSubjectNameAndSubjectCode = new HashMap<>();

        TreeMap<Integer, AtomicLong> mostRepeatedNumberOfSubjectsCount = new TreeMap<>(Collections.reverseOrder());
        int rank = 1;
        try {
            for (ClassResult cs : classResults) {
                SubjectUI subjectUI = new SubjectUI();
                if (classResultsMap.get(cs.getStudentName()) == null) {
                    ClassResultsUI classResultUi = new ClassResultsUI();
                    List<SubjectUI> subjList = new ArrayList<>();
                    classResultUi.setRank(rank++);
                    classResultUi.setStudentUsn(cs.getStudentUsn());
                    classResultUi.setResultClass(cs.getStudentResultClass());
                    classResultUi.setStudentAggregate(cs.getStudentAggregate());
                    classResultUi.setStudentName(cs.getStudentName());
                    classResultUi.setStudentTotal(cs.getStudentSemTotal());
                    classResultUi.setSubjects(subjList);
                    classResultsMap.put(cs.getStudentName(), classResultUi);
                    categoryCount.putIfAbsent(cs.getStudentResultClass(), new AtomicLong(0));
                    categoryCount.get(cs.getStudentResultClass()).incrementAndGet();
                }

                subjectUI.setSubjectName(cs.getSubjectName());
                subjectUI.setExternalMarks(cs.getExternalMarks());
                subjectUI.setInternalMarks(cs.getInternalMarks());
                subjectUI.setSubjectCode(cs.getSubjectCode());
                subjectUI.setTotalMarks(cs.getTotalMarks());
                classResultsMap.get(cs.getStudentName()).getSubjects().add(subjectUI);

                // count of each subject
                subjectCount.putIfAbsent(cs.getSubjectName(), 0);
                subjectCount.put(cs.getSubjectName(),subjectCount.get(cs.getSubjectName())+1);

                // subject name and subject code mapping
                mapForSubjectNameAndSubjectCode.put(cs.getSubjectName(), cs.getSubjectCode());


                // computing subject pass count
                subjectPassPerformance.putIfAbsent(cs.getSubjectName(), 0);
                if (cs.getSubjectResultClass().equals("PASS")) {
                    subjectPassPerformance.put(cs.getSubjectName(), subjectPassPerformance.get(cs.getSubjectName()) +
                            1);
                }
            }

            // sorting subject frequency based on values desc
            subjectCount.entrySet().stream()
                    .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                    .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                            (oldValue, newValue) -> oldValue, LinkedHashMap::new));
            for (String subjectName : subjectPassPerformance.keySet()) {
                double percentage = subjectPassPerformance.get(subjectName).doubleValue() / subjectCount.get
                        (subjectName).doubleValue() * 100;
                subjectPassPerformance.put(subjectName, (int) percentage);
            }

            // making sure all the students have equal number of subjects for the given branch and semester - jugad
            // logic
            for (String studentName : classResultsMap.keySet()) {
                mostRepeatedNumberOfSubjectsCount.putIfAbsent(classResultsMap.get(studentName).getSubjects().size(),
                        new AtomicLong(0));
                mostRepeatedNumberOfSubjectsCount.get(classResultsMap.get(studentName).getSubjects().size())
                        .incrementAndGet();
            }

            int mostRepeatedSubjectCountAcrossStudents = mostRepeatedNumberOfSubjectsCount.firstEntry().getKey();

            subjectsForGivenSemester.addAll(new ArrayList<>(subjectCount.keySet()).subList(0,
                    mostRepeatedSubjectCountAcrossStudents));
            List<String> studentsToRemove = new ArrayList<>();
            for (String studentName : classResultsMap.keySet()) {
                List<String> existingSubjects = new ArrayList<>();
                List<String> subjects = new ArrayList<>(subjectsForGivenSemester);
                classResultsMap.get(studentName).getSubjects().forEach(subject -> existingSubjects.add(subject
                        .getSubjectName()));
                subjects.removeAll(existingSubjects);
                for (String subjectName : subjects) {
                    SubjectUI newSubjectUi = new SubjectUI();
                    newSubjectUi.setSubjectName(subjectName);
                    newSubjectUi.setSubjectCode(mapForSubjectNameAndSubjectCode.get(subjectName));
                    newSubjectUi.setExternalMarks(0);
                    newSubjectUi.setInternalMarks(0);
                    newSubjectUi.setTotalMarks(0);
                    classResultsMap.get(studentName).getSubjects().add(newSubjectUi);
                }

                // adding students to be removed, if they have irregularity in subjects
                if(classResultsMap.get(studentName).getSubjects().size() > subjectsForGivenSemester.size())
                    studentsToRemove.add(studentName);


            }
            // removing student if he has irregular subjects
            studentsToRemove.forEach(student->classResultsMap.remove(student));

            // removing irregular subjects
            subjectPassPerformance.keySet().removeIf(subject->!subjectsForGivenSemester.contains(subject));

            finalMap.put("classResults", classResultsMap.values());
            finalMap.put("studentResultCategoriesCount", categoryCount);
            finalMap.put("subjectPassPerformance", subjectPassPerformance);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return finalMap;
    }
}
