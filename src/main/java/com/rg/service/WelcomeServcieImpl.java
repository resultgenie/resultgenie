package com.rg.service;

import com.rg.config.TenantContext;
import com.rg.domain.credentials.*;
import com.rg.domain.data.BranchRepository;
import com.rg.domain.data.College;
import com.rg.domain.data.CollegeRepository;
import com.rg.exception.CustomGenericException;
import com.rg.service.intf.WelcomeService;
import com.rg.utils.EmailSender;
import com.rg.utils.Response;
import com.rg.utils.StatusCodes;
import com.rg.utils.StringConstants;
import org.apache.ibatis.jdbc.ScriptRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Random;

/**
 * Created by shreeshan on 24/04/17.
 */
@Service
@Transactional
public class WelcomeServcieImpl implements WelcomeService {

    private static final Logger LOG = LoggerFactory.getLogger(WelcomeServcieImpl.class);

    @Autowired
    TenantRepository tenantRepo;

    @Autowired
    UserRepository userRepo;

    @Autowired
    EmailSender sender;

    @Autowired
    RoleRepository roleRepo;

    @Autowired
    BranchRepository branchRepo;

    @Autowired
    CollegeRepository collegeRepo;

    @Autowired
    Environment env;


    PasswordEncoder encoder = new BCryptPasswordEncoder();


    @Override
    public ResponseEntity<Response> userRegister(User user) throws Exception {

        Response response = new Response();
        try {
            if (user.getName() == null || user.getPassword() == null || user.getKey() == null || user.getEmailId() == null) {
                throw new CustomGenericException(StatusCodes.REQUIRED_DATA_NOT_FOUND, StringConstants
                        .REQUIRED_DATA_NOT_FOUND);
            }

            Tenant tenant = tenantRepo.findByKey(user.getKey());
            if (tenant == null) {
                throw new CustomGenericException(StatusCodes.NOT_FOUND_IN_DB_OR_FAILED_TO_RETRIEVE, StringConstants
                        .NO_COLLEGE_FOR_KEY);
            }
            User dbUser = userRepo.findByEmailId(user.getEmailId());
            if (dbUser != null) {
                throw new CustomGenericException(StatusCodes.DATA_ALREADY_EXISTS, StringConstants.EMAIL_ID_EXISTS);
            }
            //below line is jugad !! guess why ?;)
            user.setUsername(user.getEmailId());

            String hashedPassword = encoder.encode(user.getPassword());
            user.setPassword(hashedPassword);
            user.setTenant(tenant);
            user.getRoles().add(roleRepo.findByRole(StringConstants.USER_ROLE));
            user.setEnabled(1);
            userRepo.save(user);
            response.setCode(StatusCodes.SUCCESS);
            response.setBody(StringConstants.REGISTRATION_SUCCESSFUL);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }


    @Override
    @Transactional
    public ResponseEntity<Response> setupCollege(College college) throws CustomGenericException,
            ClassNotFoundException, SQLException, FileNotFoundException {

        Response response = new Response();
        try {
            if (college == null) {
                throw new CustomGenericException(StatusCodes.REQUIRED_DATA_NOT_FOUND, StringConstants
                        .REQUIRED_DATA_NOT_FOUND);
            }
            String dbName = "rg_" + college.getCollegeCode();
            Tenant t = tenantRepo.findOne(dbName);
            if (t == null) {
                String path = env.getProperty("rg_sql_path");
                LOG.info("path ----> " + path);
                Class.forName("com.mysql.jdbc.Driver");
                Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/?user=root&password=root");
                Statement s = conn.createStatement();
                s.executeUpdate("drop database if exists " + dbName);
                s.executeUpdate("CREATE DATABASE " + dbName);
                s.executeUpdate("use " + dbName);

                String tenantLoginName = college.getCollegeCode();
                String tenantLoginPassword = new BigInteger(130, new SecureRandom()).toString(32).substring(0, 10);

//              create hash of the password to save it in db
                String hashedPassword = encoder.encode(tenantLoginPassword);

                byte[] array = new byte[7];
                new Random().nextBytes(array);

                //DB IS CREATED , NOW SAVE THE TENANT
                Tenant tenant = tenantRepo.save(new Tenant(dbName, "jdbc:mysql://localhost:3306/" + dbName +
                        "?autoReconnect=true&useSSL=false", "root", "root", tenantLoginName, hashedPassword, new
                        BigInteger(130, new SecureRandom()).toString(32).substring(0, 8)));
                // TENANT IS SAVED , NOW CREATE A SCHEMA FOR HIM
                conn = DriverManager.getConnection("jdbc:mysql://localhost/" + dbName + "?user=" + tenant.getUsername
                        () + "&password=" + tenant.getPassword());
                ScriptRunner sRunner = new ScriptRunner(conn);
                Reader reader = new BufferedReader(
                        new FileReader(new File(path)));
                sRunner.runScript(reader);
                tenant = tenantRepo.findOne("rg_" + college.getCollegeCode());
                LOG.info("Tenant Found " + tenant.getId() + " " + tenant.getUrl());
                TenantContext.setCurrentTenant(tenant.getId());
                college.setRegisteredDateTime(System.currentTimeMillis());
                branchRepo.save(college.getBranch());
                collegeRepo.save(college);

//            mail loginname and password to the customer
//            sender.sendToTenantUponRegistration(college.getCollegeEmail(), tenantLoginName, tenantLoginPassword,
// tenant.getKey());

            } else {
                LOG.info("College already registered");
                throw new CustomGenericException(StatusCodes.DATA_ALREADY_EXISTS, StringConstants.COLLEGE_EXISTS);
            }
            response.setBody(StringConstants.COLLEGE_SETUP_SUCCESSFUL);
            response.setCode(StatusCodes.SUCCESS);

        } catch (Exception e) {
            e.printStackTrace();
//            tenantRepo.delete("rg_" + college.getCollegeCode());
            // delete database also
            throw e;

        }
        return new ResponseEntity<Response>(response, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Response> userLoginCheck(String username, HttpServletRequest httpRequest,
                                                   HttpServletResponse httpResponse) throws CustomGenericException,
            IOException {
        Response r = new Response();
        try {
            if (username == null) {
                throw new CustomGenericException(StatusCodes.REQUIRED_DATA_NOT_FOUND, StringConstants
                        .REQUIRED_DATA_NOT_FOUND);
            }
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            if (auth.getPrincipal() instanceof org.springframework.security.core.userdetails.User) {
                org.springframework.security.core.userdetails.User user = (org.springframework.security.core
                        .userdetails.User) auth.getPrincipal();
                if (!username.equals(user.getUsername())) {
                    httpResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                    ServletOutputStream writer = httpResponse.getOutputStream();
                    writer.flush();
                    return null;
                } else {
                    r.setBody(StringConstants.LOGGED_IN);
                    r.setCode(StatusCodes.SUCCESS);
                }
            } else {
                httpResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                ServletOutputStream writer = httpResponse.getOutputStream();
                writer.flush();
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
        return new ResponseEntity<Response>(r, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Response> usernameCheck(String username) throws CustomGenericException {
        Response response = new Response();
        try {
            if(username == null)
            {
                throw new CustomGenericException(StatusCodes.REQUIRED_DATA_NOT_FOUND, StringConstants
                        .REQUIRED_DATA_NOT_FOUND);
            }
            User dbUser = userRepo.findByUsername(username);
            if (dbUser != null) {
                throw new CustomGenericException(StatusCodes.DATA_ALREADY_EXISTS, StringConstants.USERNAME_EXISTS);
            }
            response.setBody(StringConstants.USERNAME_AVAILABLE);
            response.setCode(StatusCodes.SUCCESS);

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Response> emailIdCheck(String email) throws CustomGenericException {
        Response response = new Response();
        try {
            if(email == null)
            {
                throw new CustomGenericException(StatusCodes.REQUIRED_DATA_NOT_FOUND, StringConstants
                        .REQUIRED_DATA_NOT_FOUND);
            }
            User dbUser = userRepo.findByEmailId(email);
            if (dbUser != null) {
                throw new CustomGenericException(StatusCodes.DATA_ALREADY_EXISTS, StringConstants.EMAIL_ID_EXISTS);
            }
            response.setBody(StringConstants.EMAIL_ID_AVALAIBLE);
            response.setCode(StatusCodes.SUCCESS);

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    //    @SuppressWarnings("unchecked")
//    public ResponseEntity<Response> login(Tenant tenant) throws Exception {
//        Response response = new Response();
//        try {
//            if (tenant.getLoginUsername() == null || tenant.getLoginPassword() == null) {
//                throw new CustomGenericException("444", "required parameters not found");
//            }
//            Tenant tenantDb = tenantRepo.findByLoginUsername(tenant.getLoginUsername());
//            if (tenantDb == null) {
//                throw new CustomGenericException("404", "no tenant exists");
//            }
//            Boolean isSame = encoder.matches(tenant.getLoginPassword(), tenantDb.getLoginPassword());
////            Tenant tenantDb = tenantRepo.findByLoginUsernameAndLoginPassword(tenant.getLoginUsername(),
// hashedPassword);
//            if (!isSame) {
//                throw new CustomGenericException("404", "no tenant exists");
//            }
//            response.setCode(200);
//            response.setBody("login successfull");
//            return new ResponseEntity<Response>(response, HttpStatus.OK);
//        } catch (Exception e) {
//            e.printStackTrace();
//            throw e;
//        }
//
//    }
//
//    @Override
//    public ResponseEntity<Response> userLogin(User user) throws CustomGenericException {
//
//        Response response = new Response();
//        try {
//            if (user.getUsername() == null || user.getPassword() == null) {
//                throw new CustomGenericException("444", "required parameters not found");
//            }
//            User dbUser = userRepo.findByUsername(user.getUsername());
//            if (dbUser == null) {
//                throw new CustomGenericException("404", "User does not exists");
//            }
//            Boolean isSame = encoder.matches(user.getPassword(), dbUser.getPassword());
//            if (!isSame) {
//                throw new CustomGenericException("404", "User does not exists");
//            }
//            response.setCode(200);
//            response.setBody("login successful");
//            return new ResponseEntity<Response>(response, HttpStatus.OK);
//        } catch (Exception e) {
//            e.printStackTrace();
//            throw e;
//        }
//    }


}
