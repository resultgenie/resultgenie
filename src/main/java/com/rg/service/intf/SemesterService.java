package com.rg.service.intf;

import com.rg.exception.CustomGenericException;
import com.rg.utils.Response;
import org.springframework.http.ResponseEntity;

/**
 * Created by shreeshan on 26/04/17.
 */
public interface SemesterService {
    ResponseEntity<Response> getWholeClassResults(String year, String branchAbb, String semesterValue) throws CustomGenericException;


    ResponseEntity<Response> getCombinationOfSemestersYearsBranches(Boolean includeBranch) throws CustomGenericException;
}
