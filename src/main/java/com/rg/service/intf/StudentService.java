package com.rg.service.intf;

import com.rg.exception.CustomGenericException;
import com.rg.uibeans.student.PlacementCriteriaBean;
import com.rg.utils.Response;
import org.springframework.http.ResponseEntity;

import java.io.IOException;


public interface StudentService {



    ResponseEntity<Response> fetchResult(String collegeId) throws IOException, CustomGenericException;

    ResponseEntity<Response> getStudentStat(String usn) throws CustomGenericException;

    ResponseEntity<Response> searchStudent(String text) throws CustomGenericException;

    ResponseEntity<Response> getStudent(String usn) throws CustomGenericException;

    ResponseEntity<Response> getStudentsWithRankAcrossBranches(String year, String semester) throws CustomGenericException;


    ResponseEntity<Response> getStudentsForPlacements(PlacementCriteriaBean placementCriteriaBean) throws CustomGenericException;


    //public List getSuggestions(String text) throws InterruptedException;
    //public ResponseEntity<JSONObject> search(String text);
    //public ResponseEntity<JSONObject> autoCompleteFeature();


}
