package com.rg.service.intf;

import com.rg.utils.Response;
import org.springframework.http.ResponseEntity;

/**
 * Created by shreeshan on 28/05/17.
 */
public interface CollegeService {
    ResponseEntity<Response> getCollege();
}
