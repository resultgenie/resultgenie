package com.rg.service.intf;

import com.rg.domain.credentials.User;
import com.rg.domain.data.College;
import com.rg.exception.CustomGenericException;
import com.rg.utils.Response;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;

/**
 * Created by shreeshan on 24/04/17.
 */

public interface WelcomeService {

    ResponseEntity<Response> userRegister(User user) throws Exception;

    ResponseEntity<Response> setupCollege(College college) throws CustomGenericException, ClassNotFoundException, SQLException, FileNotFoundException;

    ResponseEntity<Response> userLoginCheck(String username, HttpServletRequest httpRequest, HttpServletResponse httpResponse) throws CustomGenericException, IOException;


    ResponseEntity<Response> usernameCheck(String username) throws CustomGenericException;

    ResponseEntity<Response> emailIdCheck(String email) throws CustomGenericException;

    //ResponseEntity<Response> login(Tenant tenant) throws Exception;
    //ResponseEntity<Response> userLogin(User user) throws CustomGenericException;

}
