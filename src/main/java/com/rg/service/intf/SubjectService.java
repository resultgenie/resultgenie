package com.rg.service.intf;

import com.rg.exception.CustomGenericException;
import com.rg.utils.Response;
import org.springframework.http.ResponseEntity;

/**
 * Created by shreeshan on 26/04/17.
 */
public interface SubjectService {
    ResponseEntity<Response> searchSubject(String query) throws CustomGenericException;

    ResponseEntity<Response> subjectStats(String subjectCode,String branchAbb) throws CustomGenericException;

    ResponseEntity<Response> subjectDetails(String subjectCode, String branchAbb) throws CustomGenericException;
}
