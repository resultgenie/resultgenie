package com.rg.service.intf;

import com.rg.exception.CustomGenericException;
import com.rg.utils.Response;
import org.springframework.http.ResponseEntity;

/**
 * Created by shreeshan on 16/06/17.
 */
public interface DashboardService {

    ResponseEntity<Response> getBranchList();

    ResponseEntity<Response> getBranchCount();

    ResponseEntity<Response> getDashboardHighlighterData();

    ResponseEntity<Response> getBranchWiseStudentResultCountsAcrossCategories() throws CustomGenericException;

    ResponseEntity<Response> getBranchWisePassPercentages();
}
