package com.rg.service.intf;

import com.rg.exception.CustomGenericException;
import com.rg.utils.Response;
import org.springframework.http.ResponseEntity;

/**
 * Created by shreeshan on 26/04/17.
 */
public interface BranchService {
    ResponseEntity<Response> searchBranch(String query) throws CustomGenericException;

    ResponseEntity<Response> getSemWiseResults() throws CustomGenericException;

    ResponseEntity<Response> getSemWisePassCount() throws CustomGenericException;

    ResponseEntity<Response> getBranches();
}
