package com.rg.service;

import com.rg.domain.data.*;
import com.rg.domain.data.aggregateentities.*;
import com.rg.exception.CustomGenericException;
import com.rg.service.intf.SubjectService;
import com.rg.uibeans.student.SubjectScore;
import com.rg.uibeans.subject.SubjectStatsBean;
import com.rg.utils.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by shreeshan on 26/04/17.
 */
@Service
@Transactional
public class SubjectServiceImpl implements SubjectService {

    @Autowired
    SubjectRepository subjectRepo;

    @Autowired
    TheoryPracticalRepository theoryPracticalRepo;

    @Autowired
    SubjectDetailsRepository subjectDetailsRepo;

    @Autowired
    StudentScoresRepository studentScoresRepo;

    @Autowired
    BacklogStudentScoresRepository backlogStudentScoresRepo;

    @Autowired
    SubjectResultsHistoryRepository subjectResultsHistoryRepo;

    @Autowired
    SubjectStatsRepository subjectStatsRepo;

    @Autowired
    BranchRepository branchRepo;

    @Autowired
    SubjectWiseRankingRepository subjectWiseRankingRepo;

    @SuppressWarnings("unchecked")
    @Override
    public ResponseEntity<Response> searchSubject(String query) throws CustomGenericException {
        Response response = new Response();
        try {
            if (query == null) {
                throw new CustomGenericException(StatusCodes.REQUIRED_DATA_NOT_FOUND, StringConstants
                        .REQUIRED_DATA_NOT_FOUND);
            }
            List<Object> subjectList = subjectRepo.getSubjectAndSubjectCodeForString(query);
            response.setBody(subjectList);
            response.setCode(StatusCodes.SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
 
    @Override
    public ResponseEntity<Response> subjectStats(String subjectCode, String branchAbb) throws CustomGenericException {

        Response response = new Response();
        SubjectScore bestSubjectScore = new SubjectScore();
        SubjectScore leastSubjectScore = new SubjectScore();
        SubjectScore subjectDifficultyBean = new SubjectScore();
        SubjectStatsBean subjectStatsBean = new SubjectStatsBean();

        NavigableMap<String, Integer> subjectResultRangesCountDistribution = new TreeMap<>();
        subjectResultRangesCountDistribution.put("91-100", 0);
        subjectResultRangesCountDistribution.put("71-90", 0);
        subjectResultRangesCountDistribution.put("51-70", 0);
        subjectResultRangesCountDistribution.put("35-50", 0);
        subjectResultRangesCountDistribution.put("0-34", 0);


        HashMap<String, AtomicInteger> subjectResultCategoryCountDistribution = new HashMap<>();

        try {
            if (subjectCode == null || branchAbb == null) {
                throw new CustomGenericException(StatusCodes.REQUIRED_DATA_NOT_FOUND, StringConstants
                        .REQUIRED_DATA_NOT_FOUND);
            }
            Long start = System.currentTimeMillis();
            Subject subject = subjectRepo.findBySubjectCodeAndBranchAbbreviation(subjectCode, branchAbb);
            System.out.println("subject  -- " + (System.currentTimeMillis() - start));
            if (subject == null) {
                throw new CustomGenericException(StatusCodes.NOT_FOUND_IN_DB_OR_FAILED_TO_RETRIEVE, StringConstants
                        .NO_SUBJECT);
            }


            start = System.currentTimeMillis();
            Branch branch = branchRepo.findByBranchAbbreviation(branchAbb);
            System.out.println("branch  -- " + (System.currentTimeMillis() - start));
            if (branch == null) {
                throw new CustomGenericException(StatusCodes.NOT_FOUND_IN_DB_OR_FAILED_TO_RETRIEVE, StringConstants
                        .NO_BRANCH);
            }


            start = System.currentTimeMillis();
            SubjectStats subjectStats = subjectStatsRepo.findBySubjectAndBranch(subject, branch);
            System.out.println("subjectStats  -- " + (System.currentTimeMillis() - start));
            if (subjectStats == null) {
                throw new CustomGenericException(StatusCodes.NOT_FOUND_IN_DB_OR_FAILED_TO_RETRIEVE, StringConstants
                        .NO_SUBJECT_STATS_FOR_CODE);
            }


            // subject max marks and max marks by
            bestSubjectScore.setScore(subjectStats.getHighestMarks());
            bestSubjectScore.setStudentName(subjectStats.getHighestScorer().getStudentName());
            bestSubjectScore.setChangeInScore(subjectStats.getChangeInHighestMarks());
            subjectStatsBean.setBestScore(bestSubjectScore);


            // subject min marks and min marks by
            leastSubjectScore.setScore(subjectStats.getLeastMarks());
            leastSubjectScore.setStudentName(subjectStats.getLeastScorer().getStudentName());
            leastSubjectScore.setChangeInScore(subjectStats.getChangeInLeastScore());
            subjectStatsBean.setLeastScore(leastSubjectScore);


            // overall subject ranking
            start = System.currentTimeMillis();
            List<Object[]> overallResultsSubjectRanking = theoryPracticalRepo.getOverallSubjectRanking(subjectCode,
                    branch.getBranchAbbreviation());
            System.out.println("overallResultsSubjectRanking  -- " + (System.currentTimeMillis() - start));
            if (overallResultsSubjectRanking == null || overallResultsSubjectRanking.size() == 0 ||
                    overallResultsSubjectRanking.get(0)[0] == null || overallResultsSubjectRanking.get(0)[1] == null
                    || overallResultsSubjectRanking.get(0)[2] == null) {
                overallResultsSubjectRanking = new ArrayList<>();
                overallResultsSubjectRanking.add(new Object[3]);
                overallResultsSubjectRanking = initialiseIfNull(overallResultsSubjectRanking, 0, 0, 0);
//                throw new CustomGenericException(StatusCodes.NOT_FOUND_IN_DB_OR_FAILED_TO_RETRIEVE, StringConstants
//                        .FAILED_TO_CALC_SUBJECT_RANK_CURR_SEM);
            }


            start = System.currentTimeMillis();
            List<Object[]> overallResultsSubjectRankingButPreviousResult = theoryPracticalRepo
                    .getOverallSubjectRankingButPreviousResult(subjectCode, branch.getBranchAbbreviation());
            System.out.println("overallResultsSubjectRankingButPreviousResult  -- " + (System.currentTimeMillis() -
                    start));
            if (overallResultsSubjectRankingButPreviousResult == null ||
                    overallResultsSubjectRankingButPreviousResult.size() == 0 ||
                    overallResultsSubjectRankingButPreviousResult.get(0)[0] == null ||
                    overallResultsSubjectRankingButPreviousResult.get(0)[1] == null ||
                    overallResultsSubjectRankingButPreviousResult.get(0)[2] == null) {
                overallResultsSubjectRankingButPreviousResult = new ArrayList<>();
                overallResultsSubjectRankingButPreviousResult.add(new Object[3]);
                overallResultsSubjectRankingButPreviousResult = initialiseIfNull
                        (overallResultsSubjectRankingButPreviousResult, 0, 0, 0);
//                throw new CustomGenericException(StatusCodes.NOT_FOUND_IN_DB_OR_FAILED_TO_RETRIEVE, StringConstants
//                        .FAILED_TO_CALC_SUBJECT_RANK_PREV_SEM);
            }

            Double changeInSubjectRanking = GeneralUtils.percentageChangeCalculator(toDouble
                            (overallResultsSubjectRanking.get(0)[0]),
                    toDouble(overallResultsSubjectRankingButPreviousResult.get(0)[0]), true);
            subjectStatsBean.setSubjectRank(toInteger(overallResultsSubjectRanking.get(0)[0]));
            subjectStatsBean.setSubjectRankChange(changeInSubjectRanking.intValue());


            // subject difficulty
            Double overallSubjectAvg = (toDouble(overallResultsSubjectRanking.get(0)
                    [1]) / (toDouble(overallResultsSubjectRanking.get(0)[2]) / 10));

            Double subjectDifficulty = (10 - overallSubjectAvg);
            subjectDifficultyBean.setScore(subjectDifficulty);
            subjectDifficultyBean.setSubjectName(subject.getSubjectName());

            Double overallButPreviousResultSubjectAverage = (toDouble(overallResultsSubjectRankingButPreviousResult
                    .get(0)[1])) /
                    (toDouble(overallResultsSubjectRankingButPreviousResult.get(0)[2])) / 10;
            Double previousSubjectDifficulty = (10 - overallButPreviousResultSubjectAverage);

            Double changeInSubjectDifficulty = GeneralUtils.percentageChangeCalculator(subjectDifficulty,
                    previousSubjectDifficulty, true);
            subjectDifficultyBean.setChangeInScore(changeInSubjectDifficulty);
            subjectStatsBean.setSubjectDifficulty(subjectDifficultyBean);


            // backlog object (along with change)
            start = System.currentTimeMillis();
            Integer subjectBacklogCount = theoryPracticalRepo.subjectBacklogCount(subjectCode, branch
                    .getBranchAbbreviation());
            System.out.println("subjectBacklogCount  -- " + (System.currentTimeMillis() - start));
            if (subjectBacklogCount == null) {
                throw new CustomGenericException(StatusCodes.NOT_FOUND_IN_DB_OR_FAILED_TO_RETRIEVE, StringConstants
                        .FAILED_TO_CALC_SUBJECT_BACKLOG_COUNT_CURR_SEM);
            }
            start = System.currentTimeMillis();
            Integer overallSubjectBacklogCountButPreviousResult = theoryPracticalRepo
                    .getPreviousSemSubjectBacklogCount(subjectCode, branch.getBranchAbbreviation());
            System.out.println("overallSubjectBacklogCountButPreviousResult  -- " + (System.currentTimeMillis() -
                    start));
            if (overallSubjectBacklogCountButPreviousResult == null) {
                throw new CustomGenericException(StatusCodes.NOT_FOUND_IN_DB_OR_FAILED_TO_RETRIEVE, StringConstants
                        .FAILED_TO_CALC_SUBJECT_BACKLOG_COUNT_PREV_SEM);
            }
            Double percentageChangeInBacklogCount = GeneralUtils.percentageChangeCalculator
                    (subjectBacklogCount.doubleValue(), overallSubjectBacklogCountButPreviousResult.doubleValue(),
                            true);
            subjectStatsBean.setSubjectBacklogs(subjectBacklogCount);
            subjectStatsBean.setSubjectBacklogsChange(percentageChangeInBacklogCount.intValue());


            // current sem student results
            start = System.currentTimeMillis();
            List<StudentScores> currentSemesterStudentSubjectScores = studentScoresRepo
                    .getCurrentSemStudentSubjectScores(subjectCode, branch.getBranchAbbreviation());
            System.out.println("currentSemesterStudentSubjectScores  -- " + (System.currentTimeMillis() - start));
            subjectStatsBean.setCurrentSemesterSubjectResult(currentSemesterStudentSubjectScores);

            // calculating subject result category count(pass,fail,absent etc)
            for (StudentScores ss : currentSemesterStudentSubjectScores) {
                subjectResultCategoryCountDistribution.putIfAbsent(ss.getResultClass(), new AtomicInteger(0));
                subjectResultCategoryCountDistribution.get(ss.getResultClass()).incrementAndGet();
                String key = null;
                Integer s = null;
                s = ss.getExternalMarks();
                if (s == 0)
                    s += 2;
                else if (s == 100)
                    s -= 2;
                key = subjectResultRangesCountDistribution.floorEntry(Integer.toString(s + 1)).getKey();
                subjectResultRangesCountDistribution.put(key, subjectResultRangesCountDistribution.get(key) + 1);


            }
            // category count
            subjectStatsBean.setSubjectResultPercentageDistribution(subjectResultCategoryCountDistribution);
            // marks range count
            subjectStatsBean.setSubjectResultRangesDistribution(subjectResultRangesCountDistribution);


            // top N students
            start = System.currentTimeMillis();
            List<Object[]> topNStudentSubjectScoresList = studentScoresRepo.getSubjectTopNScorers(subjectCode,
                    10, branch.getBranchAbbreviation());
            List<StudentScores> studentScores = new ArrayList<>();
            for(Object[] obj : topNStudentSubjectScoresList)
            {
                StudentScores scores = new StudentScores();
                scores.setStudentName((String) obj[1]);
                scores.setStudentUsn((String)obj[2]);
                scores.setExternalMarks(toInteger(obj[3]));
                scores.setInternalMarks(toInteger(obj[4]));
                scores.setTotalMarks(toInteger(obj[5]));
                scores.setResultClass((String)obj[6]);
                studentScores.add(scores);
            }
            System.out.println("topNStudentSubjectScoresList  -- " + (System.currentTimeMillis() - start));
            subjectStatsBean.setSubjectTopScores(studentScores);


            // current sem student results with backlogs
            start = System.currentTimeMillis();
            List<BacklogStudentScores> studentSubjectScoresWithBacklogs = backlogStudentScoresRepo
                    .getStudentSubjectScoresWithBacklogs(subjectCode,branch.getBranchAbbreviation());
            System.out.println("studentSubjectScoresWithBacklogs  -- "+(System.currentTimeMillis()-start));

            studentSubjectScoresWithBacklogs.forEach(obj->{
                if(obj.getStudentSem().equals(obj.getSubjectSem()))
                    obj.setIsActiveBacklog(1);
                else
                    obj.setIsActiveBacklog(0);
            });
            subjectStatsBean.setCurrentSemesterStudentWithBacklogs(studentSubjectScoresWithBacklogs);

            // subject results history
            start = System.currentTimeMillis();
            List<SubjectResultsHistory> subjectResultsHistoryList = subjectResultsHistoryRepo.subjectHistory
                    (subjectCode,branchAbb);
            System.out.println("subjectResultsHistoryList  -- " + (System.currentTimeMillis() - start));
            subjectStatsBean.setSubjectPerformancePlotData(subjectResultsHistoryList);


            // subject zone
            String currentZone = SubjectStatsUtils.getSubjectZoneForSubjectDifficulty(overallSubjectAvg);
            String previousZone = SubjectStatsUtils.getSubjectZoneForSubjectDifficulty
                    (overallButPreviousResultSubjectAverage);
            subjectStatsBean.setSubjectZone(currentZone);
            subjectStatsBean.setSubjectZoneChange(previousZone.equals(currentZone) ? "--" : previousZone);

            response.setBody(subjectStatsBean);
            response.setCode(StatusCodes.SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Response> subjectDetails(String subjectCode, String branchAbb) throws CustomGenericException {
        Response response = new Response();
        try {

            SubjectRanking subjectRanking = new SubjectRanking();
            if (subjectCode == null || branchAbb == null) {
                throw new CustomGenericException(StatusCodes.REQUIRED_DATA_NOT_FOUND, StringConstants
                        .REQUIRED_DATA_NOT_FOUND);
            }
            Long start = System.currentTimeMillis();
            Subject subject = subjectRepo.findBySubjectCodeAndBranchAbbreviation(subjectCode, branchAbb);
            System.out.println("subject  -- " + (System.currentTimeMillis() - start));
            if (subject == null) {
                throw new CustomGenericException(StatusCodes.NOT_FOUND_IN_DB_OR_FAILED_TO_RETRIEVE, StringConstants
                        .NO_SUBJECT);
            }


            start = System.currentTimeMillis();
            Branch branch = branchRepo.findByBranchAbbreviation(branchAbb);
            System.out.println("branch  -- " + (System.currentTimeMillis() - start));
            if (branch == null) {
                throw new CustomGenericException(StatusCodes.NOT_FOUND_IN_DB_OR_FAILED_TO_RETRIEVE, StringConstants
                        .NO_BRANCH);
            }


            // get subject details - static information (sort of demographics)
            start = System.currentTimeMillis();
            SubjectDetails subjectDetails = subjectDetailsRepo.getSubjectDetails(subjectCode, branch
                    .getBranchAbbreviation());
            System.out.println("subjectDetails  -- " + (System.currentTimeMillis() - start));
            if (subjectDetails != null) {
                // top student
                start = System.currentTimeMillis();
                StudentScores subjectTopper = studentScoresRepo.getSubjectTopScorer(subjectCode, branch
                        .getBranchAbbreviation());
                subjectDetails.setSubjectTopper(subjectTopper);
                System.out.println("subjectTopper  -- " + (System.currentTimeMillis() - start));

                SubjectWiseRanking sr = subjectWiseRankingRepo.findBySubjectCodeAndBranch(subjectCode,branch.getBranchAbbreviation());
                subjectRanking.setBranchRank(sr.getRankInBranch());
                subjectRanking.setBranchMaxSubjects(sr.getTotalSubjectsInBranch());
                subjectRanking.setSemesterRank(sr.getRankInSemester());
                subjectRanking.setSemesterMaxSubjects(sr.getTotalSubjectsInSemester());
                subjectRanking.setCollegeRank(sr.getRankInCollege());
                subjectRanking.setCollegeMaxSubjects(sr.getTotalSubjectsInCollege());
                subjectDetails.setSubjectRanking(subjectRanking);

                response.setBody(subjectDetails);
                response.setCode(StatusCodes.SUCCESS);
            }

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    private Double toDouble(Object o) {
        return new Double(o.toString());
    }

    private Integer toInteger(Object o) {
        return new Double(o.toString()).intValue();
    }

    private List<Object[]> initialiseIfNull(List<Object[]> list, Integer... args) {
        for (int i = 0; i <= args.length - 1; i++) {
            list.get(0)[i] = args[i];
        }
        return list;
    }


}
