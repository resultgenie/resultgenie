package com.rg.exception;

import com.rg.utils.EmailSender;
import com.rg.utils.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.mail.MessagingException;

@ControllerAdvice
public class ExceptionHandlerClass {

	@Autowired
	private EmailSender sender;

	@SuppressWarnings( "unchecked" )
	@ExceptionHandler( CustomGenericException.class )
	public ResponseEntity<Response> handleCustomException( CustomGenericException e ){
		Response response = new Response();
		response.setBody(e.getMessage());
		response.setCode(e.getCode());
		
		return new ResponseEntity<Response>(response, HttpStatus.INTERNAL_SERVER_ERROR);
	}


	@ExceptionHandler( Exception.class )
	public ResponseEntity<Exception> handleCustomException( Exception e ) throws MessagingException {
//		sender.sendToDevelopers(e);
		return new ResponseEntity<Exception>(new Exception(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
	}
}
