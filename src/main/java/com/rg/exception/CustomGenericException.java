package com.rg.exception;

public class CustomGenericException extends Exception {

	private static final long serialVersionUID = 1L;
	private String message;
	private Integer code;

	public CustomGenericException( Integer code, String msg )
	{
		this.code = code;
		message = msg;

	}

	public String getMessage()
	{
		return message;
	}

	public void setMessage( String message )
	{
		this.message = message;
	}

	public Integer getCode()
	{
		return code;
	}

	public void setCode( Integer code )
	{
		this.code = code;
	}

}
