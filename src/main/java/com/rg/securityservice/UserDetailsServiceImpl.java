//package com.rg.securityservice;
//
//import com.rg.domain.credentials.Role;
//import com.rg.domain.credentials.User;
//import com.rg.domain.credentials.UserRepository;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.core.GrantedAuthority;
//import org.springframework.security.core.authority.SimpleGrantedAuthority;
//import org.springframework.security.core.userdetails.UserDetails;
//import org.springframework.security.core.userdetails.UserDetailsService;
//import org.springframework.security.core.userdetails.UsernameNotFoundException;
//import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
//
//import java.util.HashSet;
//import java.util.Set;
//
///**
// * Created by shreeshan on 11/04/17.
// */
// @Service
//    public class UserDetailsServiceImpl implements UserDetailsService {
//        @Autowired
//        private UserRepository userRepository;
//
//        @Override
//        @Transactional(readOnly = true)
//        public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
//            User user = userRepository.findByUsername(username);
//            Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
//            for (Role role : user.getRoles()){
//                grantedAuthorities.add(new SimpleGrantedAuthority(role.getRole()));
//            }
//            grantedAuthorities.add(new SimpleGrantedAuthority(user.getTenant().getId()));
//            return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), grantedAuthorities);
//        }
//    }
