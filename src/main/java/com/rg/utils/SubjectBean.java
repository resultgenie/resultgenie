package com.rg.utils;

import java.io.Serializable;

/**
 * Created by prathyush on 5/11/16.
 */
public class SubjectBean implements Serializable {


    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String name;
    private String subjectCode;
    private int internalMarks;
    private int externalMarks;
    private int totalMarks;
    private String result;
    private String theory;
    private Integer isLab;
    private String attempt;

    public String getAttempt() {
        return attempt;
    }

    public void setAttempt(String attempt) {
        this.attempt = attempt;
    }


    public Integer getIsLab() {
        return isLab;
    }

    public void setIsLab(Integer isLab) {
        this.isLab = isLab;
    }

    public String getTheory() {
        return theory;
    }


    public void setTheory(String theory) {
        this.theory = theory;
    }


    public String getResult() {
        return result;
    }


    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }

    public int getInternalMarks() {
        return internalMarks;
    }

    public void setInternalMarks(int internalMarks) {
        this.internalMarks = internalMarks;
    }

    public int getExternalMarks() {
        return externalMarks;
    }

    public void setExternalMarks(int externalMarks) {
        this.externalMarks = externalMarks;
    }

    public int getTotalMarks() {
        return totalMarks;
    }

    public void setTotalMarks(int totalMarks) {
        this.totalMarks = totalMarks;
    }

    public String isResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getSubjectCode() {
        return subjectCode;
    }

    public void setSubjectCode(String subjectCode) {
        this.subjectCode = subjectCode;
    }
}
