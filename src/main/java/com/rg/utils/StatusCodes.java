package com.rg.utils;

/**
 * Created by shreeshan on 24/04/17.
 *
 * Below are the rules to create and assign status codes
 *
    1. 404 - required data not found in db
    2. 444 - required data not found in request body
    3. 409 - data already exists
    4. 500 - server error

 */
public class StatusCodes {
    public static final int REQUIRED_DATA_NOT_FOUND = 444;
    public static final int SUCCESS = 200;
    public static final int NOT_FOUND_IN_DB_OR_FAILED_TO_RETRIEVE = 404;
    public static final int DATA_ALREADY_EXISTS = 409;
    public static final int SERVER_ERROR = 500;

}
