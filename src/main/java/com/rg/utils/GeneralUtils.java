package com.rg.utils;

/**
 * All the utility functions that is required across the whole project will be listed down in this class
 * Created by shreeshan on 03/06/17.
 */
public class GeneralUtils {


    /**
     * Calculates the change of given two numbers in terms of percentage
     *
     * @param newNumber
     * @param originalNumber
     * @return
     */
    public static Double percentageChangeCalculator(Double newNumber, Double originalNumber, boolean invert) {
        Double percentageChange = null;
        try {
            Double change = newNumber - originalNumber;
            if (originalNumber == 0)
                return change;
            percentageChange = (change / originalNumber) * 100.0;
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (invert) {
            percentageChange *= -1;
            return percentageChange;
        } else
            return percentageChange;
    }

    public static boolean decideOddOrEven(Integer val, String semType) {
        if (semType.equals("even")) {
            if (val % 2 == 0)
                return true;
            else
                return false;
        } else {
            if (val % 2 != 0)
                return true;
            else
                return false;
        }

    }
}
