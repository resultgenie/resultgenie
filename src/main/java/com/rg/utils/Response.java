package com.rg.utils;


/**
 * Created by shreeshan on 21/03/17.
 */
public class Response {

    private Object body;
    private Integer code;

    public Object getBody() {
        return body;
    }

    public void setBody(Object body) {
        this.body = body;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }
}
