package com.rg.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Arrays;
import java.util.List;

/**
 * Created by shreeshan on 23/01/17.
 *
 */
@Component
@PropertySource(value = {"classpath:mail.properties"})
public class EmailSender {


    @Autowired
    @Qualifier("mailForUsers")
    JavaMailSender sender;

    @Autowired
    @Qualifier("mailForDevelopers")
    JavaMailSender devSender;

    @Autowired
    HttpServletRequest request;

    @Autowired
    Environment env;


    private static final Logger LOG = LoggerFactory.getLogger(EmailSender.class);

    public void sendToTenantUponRegistration(String tenantEmail,String tenantUsername, String tenantPassword,String key) throws MessagingException{
        try{
            MimeMessage mailMsg = sender.createMimeMessage();
            MimeMessageHelper mailMessage = new MimeMessageHelper(mailMsg);
            mailMessage.setTo(tenantEmail);
            mailMessage.setFrom(env.getProperty("mail.username"));
            mailMessage.setSubject("Your Credentials");
            mailMessage.setText("Hey !! Welcome Onboard. " +
                    "We hope you will have an enjoyable and insightful experience with ResultGenie. " +
                    "Please find your credentials for login\n\nUsername: "+tenantUsername+"\nPassword: "+tenantPassword+"\nUsers in this college can register using this key "+key);
            sender.send(mailMsg);

        }
        catch(Exception e){

        }

    }

    public void sendToDevelopers(Object obj) throws MessagingException {
        try {
            List<String> mailRecipients = Arrays.asList("nshreesha2011@gmail.com", "kingspp@gmail.com");
            InternetAddress[] addresses = new InternetAddress[mailRecipients.size()];
            for (int i = 0; i < mailRecipients.size(); i++) {
                addresses[i] = new InternetAddress(mailRecipients.get(i));
            }
            MimeMessage mailMsgDevelopers = devSender.createMimeMessage();
            MimeMessageHelper mailMessage = new MimeMessageHelper(mailMsgDevelopers);
           mailMessage.setFrom(env.getProperty("mail.developer.username"));
            mailMessage.setTo(addresses);
            if (obj instanceof Exception) {
                Exception e = (Exception) obj;
                mailMessage.setSubject("Unexpected Java Exception found");
                StringWriter stringWriter = new StringWriter();
                e.printStackTrace(new PrintWriter(stringWriter));
                mailMessage.setText("Server :: Exception encountered" + "\nJava Message ::\n " + e.getMessage() + "\n\n\n" + "Java Stack trace ::\n" + stringWriter.toString());
            }
            devSender.send(mailMsgDevelopers);
        } catch (Exception e1) {
            LOG.error(e1.getMessage(), e1);
        }

    }

}