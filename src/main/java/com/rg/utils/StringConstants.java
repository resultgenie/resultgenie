package com.rg.utils;

/**
 * Created by shreeshan on 24/04/17.
 */
public class StringConstants {

    /**
     * Success messages
     */

    public static final String REGISTRATION_SUCCESSFUL = "Registration successful";
    public static final String USER_ROLE = "USER";
    public static final String RESULTS_SAVED_IN_DB = "Results successfully saved in db";
    public static final String COLLEGE_SETUP_SUCCESSFUL = "College setup successful";
    public static final String BACKLOG_COUNT_NOT_PASSED = "No backlog count data was passed as arguement";
    public static final String SEMESTER_NOT_PASSED = "Semester data not passed as arguement";
    public static final String SERVER_ERROR = "Internal Error";
    public static final String STUDENT_STATS = "Student Stats object in JSON";
    public static final String STUDENTS_JSON = "Student Object in JSON";
    public static final String SUBJECTS_JSON = "Subject Object in JSON";
    public static final String BRANCHES_JSON = "Branch Object in JSON";
    public static final String SUCCESS_JSON = "Success message in JSON";
    public static final String STUDENTS_COUNT = "Total number of students in the college";
    public static final String DISTINCTION_STUDENTS_COUNT = "Total number of distinction students in the college";
    public static final String BRANCH_LIST = "List of branches in JSON";
    public static final String BRANCH_RESULTS = "List of semwise/branchwise results";
    public static final String COLLEGE_PERFORMANCE = "College performance in JSON";
    public static final String STUDENT_VIEW_DATA = "Data required for student view";
    public static final String NO_STUDENT_STATS = "Student Stats not found";
    public static final String SUBJECT_STATS = "Subject stats in JSON";
    public static final String NO_STUDENTS = "No students";
    public static final String NO_RG_SCORE = "Exception when calculating RG score";
    public static final String NO_STUDENT_OVERALL_STATS = "Overall Student Stats not found";
    public static final String NO_SUBJECT = "There is no subject for the subject code and branch";
    public static final String NO_SUBJECT_STATS_FOR_CODE = "No Subject stats are calculated for this subject";
    public static final String LOGGED_IN = "User is logged in";
    public static final String DASHBOARD_HIGHLIGHTER_DATA = "Dashboard highlighter data";
    public static final String CLASS_RESULTS = "Class results of the given year,branch and semester";
    public static final String USERNAME_AVAILABLE = "Username is available ";
    public static final String EMAIL_ID_AVALAIBLE = "Email ID is available";

    /**
     * Error messages
     */
    public static final String REQUIRED_DATA_NOT_FOUND = "Required parameters not found in request body";
    public static final String NO_COLLEGE = "College information not found";
    public static final String COLLEGE_EXISTS = "College already exists";
    public static final String NO_STUDENT = "Student doesn't exist";
    public static final String NO_COLLEGE_FOR_KEY = "No College found for the key";
    public static final String ERROR_IN_BRANCHWISE_RESULT_CALC = "Error in calculation of branchwise/semwise results OR There are no entries in the database";
    public static final String FAILED_TO_CALCULATE_COLLEGE_PERFORMANCE_CURRENT_SEM = "Failed to calculate college performance of the latest semesters";
    public static final String FAILED_TO_CALCULATE_COLLEGE_PERFORMANCE_PREV_SEM = "Failed to calculate college performance of the previous semesters";
    public static final String FAILED_TO_CALC_RG_SCORE_CURRENT_SEM = "Failed to calculate Rg Score on latest sem results";
    public static final String FAILED_TO_CALC_RG_SCORE_PREV_SEM = "Failed to calculate Rg Score on previous sem results";
    public static final String FAILED_TO_CALC_DISTINCTION_STUDENTS_COUNT = "Failed to calculate distinction students count";
    public static final String FAILED_TO_CALC_DISTINCTION_STUDENTS_COUNT_PREV_SEM = "Failed to calculate distinction students count in previous semesters ";
    public static final String FAILED_TO_RETRIEVE_BRANCH_RANK_LATEST_SEM = "Failed to calculate branch rank of latest semesters";
    public static final String FAILED_TO_RETRIEVE_BRANCH_RANK_PREV_SEM = "Failed to calculate branch rank of previous semesters";
    public static final String FAILED_TO_RETRIEVE_SEM_AGGREGATE_PREV_SEM = "Failed to calculate sem aggregate of previous semester";
    public static final String FAILED_TO_CALC_COLLEGE_RANK_CURR_SEM = "Failed to calculate current sem college rank";
    public static final String FAILED_TO_CALC_COLLEGE_RANK_PREV_SEM = "Failed to calculate previous sem college rank";
    public static final String FAILED_TO_CALC_BACKLOG_PREV_SEM = "Failed to calculate backlog count or aggregate of previous sem";
    public static final String FAILED_TO_RETRIEVE_SUBJECT_RANK_WRT_COLLEGE = "Failed to calculate subject rank with respect to college";
    public static final String FAILED_TO_RETRIEVE_SUBJECT_RANK_WRT_SEMESTER = "Failed to calculate subject rank with respect to semester";
    public static final String FAILED_TO_RETRIEVE_SUBJECT_RANK_WRT_BRANCH = "Failed to calculate subject rank with respect to branch";
    public static final String FAILED_TO_CALC_SUBJECT_AVG_CURR_SEM = "Failed to calculate average of scores that has been registered for the subject in current sem";
    public static final String FAILED_TO_CALC_SUBJECT_AVG_PREV_SEM = "Failed to calculate average of scores that has been registered for the subject in previous sem";
    public static final String FAILED_TO_CALC_SUBJECT_BACKLOG_COUNT_CURR_SEM = "Failed to calculate subject backlog count for the current sem";
    public static final String FAILED_TO_CALC_SUBJECT_BACKLOG_COUNT_PREV_SEM = "Failed to calculate subject backlog count for the previous sem";
    public static final String FAILED_TO_CALC_SUBJECT_RANK_CURR_SEM = "Failed to calculate subject ranking for the current sem";
    public static final String FAILED_TO_CALC_SUBJECT_RANK_PREV_SEM = "Failed to calculate subject ranking for the previous sem";
    public static final String USERNAME_EXISTS = "Username is taken, try a different one";
    public static final String EMAIL_ID_EXISTS = "This email ID is registered, please login";
    public static final String FAILED_TO_RETRIEVE_STUDENT_AGGREGATE = "Failed to calculate student's aggregate";
    public static final String AVAILABLE_SEMESTERS_YEARS_BRANCHES = "Available years/semesters/branches in the database";
    public static final String COLLEGE_JSON = "College JSON";
    public static final String NO_RESULTS_FOR_CLASS = "No results found for this class";
    public static final String NO_BRANCH = "There is no branch for the abbreviation being passed";

    /**
     * Log messages
     */

    /**
     * URL endpoints
     */
    public static final String BASE_API_URL = "/rest/v1/";

    /**
     * constant strings
     */
    public static final String FAIL = "FAIL";

}
