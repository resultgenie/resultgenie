package com.rg.utils;

import java.io.Serializable;
import java.util.List;

/**
 * Created by prathyush on 5/11/16.
 */
public class StudentBean implements Serializable {


    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String USN;
    private List<SubjectBean> subject;
    private Integer semester;
    private String name;
    private Integer totalMarks;
    private String resultClass;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUSN() {
        return USN;
    }

    public void setUSN(String uSN) {
        USN = uSN;
    }

    public Integer getSemester() {
        return semester;
    }

    public void setSemester(Integer semester) {
        this.semester = semester;
    }

    public String getResultClass() {
        return resultClass;
    }

    public void setResultClass(String resultClass) {
        this.resultClass = resultClass;
    }

    public Integer getTotalMarks() {
        return totalMarks;
    }

    public void setTotalMarks(Integer totalMarks) {
        this.totalMarks = totalMarks;
    }

    public List<SubjectBean> getSubject() {
        return subject;
    }

    public void setSubject(List<SubjectBean> subject) {
        this.subject = subject;
    }


}
