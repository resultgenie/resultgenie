package com.rg.utils;

import com.rg.domain.data.Student;
import com.rg.domain.data.SubjectStats;
import com.rg.domain.data.TheoryPractical;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by shreeshan on 04/06/17.
 */
public class SubjectStatsUtils {

    public static SubjectStats updateSubjectBestAndLeastScore(SubjectStats subjectStats, TheoryPractical tp, Student st) {
        try {
            if (subjectStats == null) {
                subjectStats = new SubjectStats();
                subjectStats.setSubject(tp.getSubject());
                subjectStats.setHighestMarks((double) (tp.getExternalScore() + tp.getInternalScore()));
                subjectStats.setLeastMarks((double) (tp.getExternalScore() + tp.getInternalScore()));
                subjectStats.setChangeInHighestMarks(0d);
                subjectStats.setChangeInLeastScore(0d);
                subjectStats.setLeastScorer(st);
                subjectStats.setHighestScorer(st);
                subjectStats.setBranch(tp.getBranch());
            }
            else{
                if(subjectStats.getHighestMarks() < (double)(tp.getExternalScore()+tp.getInternalScore()))
                {
                    Double newHighScore = (double)(tp.getExternalScore()+tp.getInternalScore());
                    Double changeInHighestMarks  = GeneralUtils.percentageChangeCalculator(newHighScore,subjectStats.getHighestMarks(),false);
                    subjectStats.setHighestMarks(newHighScore);
                    subjectStats.setChangeInHighestMarks(changeInHighestMarks);
                    subjectStats.setHighestScorer(st);
                }
                if(subjectStats.getLeastMarks() > (double)(tp.getExternalScore()+tp.getInternalScore()))
                {
                    Double newLeastScore = (double)(tp.getExternalScore()+tp.getInternalScore());
                    Double changeInLeastScore = GeneralUtils.percentageChangeCalculator(newLeastScore,subjectStats.getLeastMarks(),false);
                    subjectStats.setLeastMarks(newLeastScore);
                    subjectStats.setChangeInLeastScore(changeInLeastScore);
                    subjectStats.setLeastScorer(st);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return subjectStats;
    }


    public static String getSubjectZoneForSubjectDifficulty(Double subjectDifficultyScore)
    {
        Map<Double,String> subjectZones = new HashMap<>();
        subjectZones.put(9d,"Best");
        subjectZones.put(6d,"Good");
        subjectZones.put(5d,"Average");
        subjectZones.put(3.5,"Poor");
        subjectZones.put(3d,"Bad");
        String zone;
        if(subjectDifficultyScore>9)
            zone = subjectZones.get(9d);
        else if(subjectDifficultyScore>6)
            zone = subjectZones.get(6d);
        else if(subjectDifficultyScore>5)
            zone = subjectZones.get(5d);
        else if(subjectDifficultyScore>3.5)
            zone = subjectZones.get(3.5);
        else
            zone = subjectZones.get(3d);

        return zone;


    }
}
