package com.rg.utils;

import com.rg.domain.data.*;
import com.rg.exception.CustomGenericException;

import java.util.*;
import java.util.stream.Stream;

public class StudentStatUtils {

    static Integer totalMarksGlobal = 0;
    static Integer totalMaxMarks;

    static Subject bestScoredSubject = new Subject();
    static Subject worstScoredSubject = new Subject();

    static Float bestScore;
    static Float worstScore;


    /**
     * This map contains semester,year in key value pairs
     */
    static Map<Integer, Integer> yearMap = new HashMap<>();

    /**
     * This map contains studentzones for backlogs
     */
    static Map<String, String> studentZoneForBacklog = new HashMap<>();

    /**
     * This map contains studentzones for agg
     */
    static Map<String, String> studentZoneForAgg = new HashMap<>();

    public static String getStudentZoneForSemesterAgg(Float agg, Integer backlogCount) {
        studentZoneForBacklog.put("5", "Dead");
        studentZoneForBacklog.put("4", "BorderLine");
        studentZoneForBacklog.put("3", "BorderLine");
        studentZoneForBacklog.put("2", "On Verge");
        studentZoneForBacklog.put("1", "On Verge");
        studentZoneForBacklog.put("0", "All Clear ");

        studentZoneForAgg.put("55-60", "Average");
        studentZoneForAgg.put("60-90", "Good");
        studentZoneForAgg.put("90 and more", "Best");

        String studentZone = null;
        try {
            if (backlogCount == null) {
                throw new CustomGenericException(StatusCodes.REQUIRED_DATA_NOT_FOUND, StringConstants.BACKLOG_COUNT_NOT_PASSED);
            }
            if (backlogCount == 0) {
                if (agg < 60 && agg >= 55)
                    studentZone = studentZoneForAgg.get("55-60");
                else if (agg < 90 && agg >= 60)
                    studentZone = studentZoneForAgg.get("60-90");
                else if (agg >= 90)
                    studentZone = studentZoneForAgg.get("90 and more");
            }
            if (backlogCount > 5)
                studentZone = studentZoneForBacklog.get("5");
            else
                studentZone = studentZoneForBacklog.get(backlogCount.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return studentZone;
    }

    public static Integer setCurrentYearForSemester(Semester semester) {
        int year = 0;
        yearMap.put(1, 1);
        yearMap.put(2, 1);
        yearMap.put(3, 2);
        yearMap.put(4, 2);
        yearMap.put(5, 3);
        yearMap.put(6, 3);
        yearMap.put(7, 4);
        yearMap.put(8, 4);
        try {
            if (semester == null) {
                throw new CustomGenericException(StatusCodes.REQUIRED_DATA_NOT_FOUND, StringConstants.SEMESTER_NOT_PASSED);
            }
            year = yearMap.get(Integer.valueOf(semester.getSemesterValue()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return year;
    }

    public static Integer getTotalMarks() {
        return totalMarksGlobal;
    }

    public static Integer getTotalMaxMarks() {
        return totalMaxMarks;
    }

    public static Float getStudentAggregate(List<TheoryPractical> theoryPracticalList) {

        Float aggregate = null;
        Integer totalMarks = 0;
        Integer maxTotalMarks = 0;
        try {
            if (theoryPracticalList.isEmpty() || theoryPracticalList.size() <= 0) {
                return null;
            }
            for (TheoryPractical tp : theoryPracticalList) {
                if(tp.getSubject().getIsRequiredForAggregateCalc()==1) {
                    totalMarks += (tp.getExternalScore() + tp.getInternalScore());
                    maxTotalMarks += (tp.getSubject().getExternalMaxMarks() + tp.getSubject().getInternalMaxMarks());
                }
            }
            aggregate = (totalMarks.floatValue() / maxTotalMarks) * 100;
            Collections.sort(theoryPracticalList,
                    (t1, t2) -> (Integer.valueOf(t1.getExternalScore() + t1.getInternalScore()))
                            .compareTo(t2.getExternalScore() + t2.getInternalScore()));
            for(int i=theoryPracticalList.size()-1;i>=0;i--)
            {
                TheoryPractical tp = theoryPracticalList.get(i);
                if(tp.getSubject().getIsRegularSubject()==1 && tp.getSubject().getIsLab()==0) {
                    bestScoredSubject = tp.getSubject();
                    bestScore = (float)tp.getInternalScore() + tp.getExternalScore();
                    break;
                }
            }
            for(TheoryPractical tp: theoryPracticalList)
            {
                if(tp.getSubject().getIsRegularSubject()==1 && tp.getSubject().getIsLab()==0) {
                    worstScoredSubject = tp.getSubject();
                    worstScore = (float)tp.getInternalScore() + tp.getExternalScore();
                    break;
                }
            }
//            bestScoredSubject = theoryPracticalList.get(theoryPracticalList.size() - 1).getSubject();
//            bestScore = (float)theoryPracticalList.get(theoryPracticalList.size() - 1).getExternalScore() + theoryPracticalList.get(theoryPracticalList.size() - 1).getInternalScore();
//            worstScoredSubject = theoryPracticalList.get(0).getSubject();
//            worstScore = (float)theoryPracticalList.get(0).getExternalScore() + theoryPracticalList.get(0).getInternalScore();
            totalMarksGlobal = totalMarks;
            totalMaxMarks = maxTotalMarks;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return aggregate;
    }

    public static StudentStats setBestSubject(StudentStats studentStats) {
        try {
            studentStats.setBestScoreSubject(bestScoredSubject);
            studentStats.setBestScore(bestScore);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return studentStats;
    }

    public static StudentStats setWorstSubject(StudentStats studentStats) {
        try {
            studentStats.setWorstScoreSubject(worstScoredSubject);
            studentStats.setWorstScore(worstScore);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return studentStats;
    }

    public static <K, V extends Comparable<? super V>> Map<String, Float> sortByValue(Map<String, Float> map) {
        Map<String, Float> result = new LinkedHashMap<>();
        Stream<Map.Entry<String, Float>> st = map.entrySet().stream();

        st.sorted(Map.Entry.<String, Float>comparingByValue().reversed())
                .forEach(e -> result.put(e.getKey(), e.getValue()));

        return result;
    }

    public static StudentOverallStats setOverallBestSubject(StudentStats studentStat,StudentOverallStats studentOverallStats) {
        try{
            if (studentOverallStats == null) {
                studentOverallStats = new StudentOverallStats();
                studentOverallStats.setOverallBestScore(bestScore);
                studentOverallStats.setOverallBestScoreSubject(bestScoredSubject);
                studentOverallStats.setChangeInBestScore(bestScore);
            }
            else{
                if(studentOverallStats.getOverallBestScore() < studentStat.getBestScore())
                {
                    Double change = (GeneralUtils.percentageChangeCalculator(studentStat.getBestScore().doubleValue(),studentOverallStats.getOverallBestScore().doubleValue(),false));
                    studentOverallStats.setOverallBestScore(studentStat.getBestScore());
                    studentOverallStats.setOverallBestScoreSubject(studentStat.getBestScoreSubject());
                    studentOverallStats.setChangeInBestScore(change.floatValue());
                }
                else{
                    studentOverallStats.setChangeInBestScore(0f);
                }
            }

        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return studentOverallStats;


    }

    public static StudentOverallStats setOverallWorstSubject(StudentStats studentStat, StudentOverallStats studentOverallStats) {
        try{

            if (studentOverallStats == null) {
                studentOverallStats = new StudentOverallStats();
            }

            if(studentOverallStats.getOverallWorstScoreSubject() == null){
                studentOverallStats.setOverallWorstScore(worstScore);
                studentOverallStats.setOverallWorstScoreSubject(worstScoredSubject);
                studentOverallStats.setChangeInWorstScore(worstScore);
            }
            else{
                if(studentOverallStats.getOverallWorstScore() > studentStat.getWorstScore())
                {
                    float change = (GeneralUtils.percentageChangeCalculator(studentStat.getWorstScore().doubleValue(),studentOverallStats.getOverallWorstScore().doubleValue(),false)).floatValue();
                    studentOverallStats.setOverallWorstScore(studentStat.getWorstScore());
                    studentOverallStats.setOverallWorstScoreSubject(studentStat.getWorstScoreSubject());
                    studentOverallStats.setChangeInWorstScore(change);
                }
                else{
                    studentOverallStats.setChangeInWorstScore(0f);
                }
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return studentOverallStats;
    }
}

