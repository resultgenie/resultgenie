package com.rg;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RGApplication {

	public static void main( String[] args ) throws Exception
	{
		SpringApplication.run(RGApplication.class, args);
	}
}