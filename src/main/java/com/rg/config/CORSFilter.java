package com.rg.config;

import com.rg.domain.credentials.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

// TODO: Auto-generated Javadoc

/**
 * CORSFilter Class
 */
@Component
public class CORSFilter implements Filter {

    private static final Logger log = LoggerFactory.getLogger(CORSFilter.class);

    private UserRepository userRepo;

    public void destroy() {
        // TODO Auto-generated method stub

    }

    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
            throws IOException, ServletException {

        HttpServletResponse httpResponse = (HttpServletResponse) res;
        String url = ((HttpServletRequest) req).getServletPath();
        log.info("Request URL --> " + url);
        setUserContext();
        httpResponse.setHeader("Access-Control-Allow-Origin", "*");
        httpResponse.setHeader("Access-Control-Allow-Methods", "POST, GET, PUT, OPTIONS, DELETE");
        httpResponse.setHeader("Access-Control-Max-Age", "3600");
        httpResponse.setHeader("Access-Control-Allow-Headers", "X-HTTP-Method-Override, Content-Type, x-requested-with,userId,Authenticate");

        chain.doFilter(req, res);
    }

    public void init(FilterConfig config) throws ServletException {
        WebApplicationContext springContext =
                WebApplicationContextUtils.getWebApplicationContext(config.getServletContext());
        userRepo = springContext.getBean(UserRepository.class);
    }

    public void setUserContext() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth.getPrincipal() instanceof User) {
            User user = (User) auth.getPrincipal();
//            if(user==null)
//            {
//                httpResponse.sendError(, "Session expired");
//            }
            com.rg.domain.credentials.User dbUser = userRepo.findByUsername(user.getUsername());
            TenantContext.setCurrentTenant(dbUser.getTenant().getId());
            log.info("Tenant found --> " + dbUser.getTenant().getId());
            log.info("Resource Request by --> " + dbUser.getUsername());

        }
    }
}