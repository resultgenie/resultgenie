package com.rg.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rg.domain.credentials.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@Component
public class RESTAuthenticationSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {

    private RequestCache requestCache = new HttpSessionRequestCache();

    private final ObjectMapper mapper;

    private MappingJackson2HttpMessageConverter messageConverter = new MappingJackson2HttpMessageConverter();

    @Autowired
    UserRepository userRepo;

    public RESTAuthenticationSuccessHandler() {
        this.mapper = messageConverter.getObjectMapper();
    }

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
                                        Authentication authentication) throws IOException, ServletException {



//        SavedRequest savedRequest
//                = requestCache.getRequest(request, response);
//        if (savedRequest == null) {
//            clearAuthenticationAttributes(request);
//            return;
//        }

        String targetUrlParam = getTargetUrlParameter();
        if (isAlwaysUseDefaultTargetUrl()
                || (targetUrlParam != null
                && StringUtils.hasText(request.getParameter(targetUrlParam)))) {
            requestCache.removeRequest(request, response);
            clearAuthenticationAttributes(request);
            return;
        }

        User user = (User)authentication.getPrincipal();
        com.rg.domain.credentials.User dbUser = userRepo.findByEmailId(user.getUsername());
        TenantContext.setCurrentTenant(dbUser.getTenant().getId());

        response.setStatus(HttpServletResponse.SC_OK);
        PrintWriter writer = response.getWriter();
        mapper.writeValue(writer, dbUser.getUsername());
        writer.flush();

    }
}
