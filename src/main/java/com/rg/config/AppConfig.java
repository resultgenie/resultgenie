package com.rg.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;

@Configuration
@ComponentScan( basePackages = "com.rg" )
@PropertySource( value = { "classpath:application.properties" } )
public class AppConfig {

	private static final Logger log = LoggerFactory.getLogger( AppConfig.class );

	public void configureDefaultServletHandling( DefaultServletHandlerConfigurer configurer )
	{
		configurer.enable();
	}

	@Bean
	public FilterRegistrationBean CORSFilterBean()
	{
		FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean();
		filterRegistrationBean.setFilter( new CORSFilter() );
		filterRegistrationBean.setEnabled( true );
		filterRegistrationBean.setOrder( 1 );
		log.info( "Registered CORSFilterBean()" );
		return filterRegistrationBean;
	}

	@Bean
	public BCryptPasswordEncoder passwordEncoder() {
		BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
		return bCryptPasswordEncoder;
	}

//	@Autowired
//	private ElasticsearchTemplate template;
//
//	private static NodeClient getNodeClient() {
//		return (NodeClient) nodeBuilder().clusterName(UUID.randomUUID().toString()).local(true).node()
//				.client();
//	}
//
//
//	@Bean
//	public ElasticsearchTemplate elasticsearchTemplate() {
//		return new ElasticsearchTemplate(getNodeClient());
//	}

}
