package com.rg.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.google.common.base.Predicates;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.UiConfiguration;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

	@Bean
	public Docket documentation()
	{
		return new Docket( DocumentationType.SWAGGER_2 ).select().apis( RequestHandlerSelectors.any() )
				.paths( Predicates.not(PathSelectors.regex("/error")) ).build().pathMapping( "/" ).apiInfo( metadata() );
	}
	
	@Bean
	  UiConfiguration uiConfig() {
	    return new UiConfiguration(
	        "validatorUrl",// url
	        "none",       // docExpansion          => none | list
	        "alpha",      // apiSorter             => alpha
	        "schema",     // defaultModelRendering => schema
	        UiConfiguration.Constants.DEFAULT_SUBMIT_METHODS,
	        false,        // enableJsonEditor      => true | false
	        true,         // showRequestHeaders    => true | false
	        60000L);      // requestTimeout => in milliseconds, defaults to null (uses jquery xh timeout)
	  }

	private ApiInfo metadata()
	{
		return new ApiInfoBuilder().title( "ResultGenie API Spec" ).description( "This UI contains all the information required inorder to communicate with ResultGenie backend" ).version( "1.0" )
			.build();
	}
}