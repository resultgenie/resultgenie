package com.rg.uibeans.dashboard;

/**
 * Created by shreeshan on 17/06/17.
 */
public class DashboardHighlighterBean {

    private Integer totalStudentsCount;

    private Double changeInTotalStudentsCount;

    private Integer totalBranchesCount;

    private Double collegePerformance;

    private Double changeInCollegePerformance;

    private Integer totalDistinctionStudentCount;

    private Double changeInTotalDistinctionStudentCount;

    private Double rgScore;

    private Double changeInRgScore;

    public Integer getTotalStudentsCount() {
        return totalStudentsCount;
    }

    public void setTotalStudentsCount(Integer totalStudentsCount) {
        this.totalStudentsCount = totalStudentsCount;
    }

    public Double getChangeInTotalStudentsCount() {
        return changeInTotalStudentsCount;
    }

    public void setChangeInTotalStudentsCount(Double changeInTotalStudentsCount) {
        this.changeInTotalStudentsCount = changeInTotalStudentsCount;
    }

    public Integer getTotalBranchesCount() {
        return totalBranchesCount;
    }

    public void setTotalBranchesCount(Integer totalBranchesCount) {
        this.totalBranchesCount = totalBranchesCount;
    }

    public Double getCollegePerformance() {
        return collegePerformance;
    }

    public void setCollegePerformance(Double collegePerformance) {
        this.collegePerformance = collegePerformance;
    }


    public Integer getTotalDistinctionStudentCount() {
        return totalDistinctionStudentCount;
    }

    public void setTotalDistinctionStudentCount(Integer totalDistinctionStudentCount) {
        this.totalDistinctionStudentCount = totalDistinctionStudentCount;
    }

    public Double getChangeInCollegePerformance() {
        return changeInCollegePerformance;
    }

    public void setChangeInCollegePerformance(Double changeInCollegePerformance) {
        this.changeInCollegePerformance = changeInCollegePerformance;
    }

    public Double getChangeInTotalDistinctionStudentCount() {
        return changeInTotalDistinctionStudentCount;
    }

    public void setChangeInTotalDistinctionStudentCount(Double changeInTotalDistinctionStudentCount) {
        this.changeInTotalDistinctionStudentCount = changeInTotalDistinctionStudentCount;
    }

    public Double getRgScore() {
        return rgScore;
    }

    public void setRgScore(Double rgScore) {
        this.rgScore = rgScore;
    }

    public Double getChangeInRgScore() {
        return changeInRgScore;
    }

    public void setChangeInRgScore(Double changeInRgScore) {
        this.changeInRgScore = changeInRgScore;
    }
}
