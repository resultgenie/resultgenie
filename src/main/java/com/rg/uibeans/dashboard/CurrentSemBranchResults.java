package com.rg.uibeans.dashboard;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by shreeshan on 08/06/17.
 */
public class CurrentSemBranchResults {

    private String branchName;
    private List<Integer> semesters = new ArrayList<>();
    private Map<String, List<Integer>> resultCount = new HashMap<>();


    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public List<Integer> getSemesters() {
        return semesters;
    }

    public void setSemesters(List<Integer> semesters) {
        this.semesters = semesters;
    }

    public Map<String, List<Integer>> getResultCount() {
        return resultCount;
    }

    public void setResultCount(Map<String, List<Integer>> resultCount) {
        this.resultCount = resultCount;
    }

    public void clear()
    {
        branchName = "";
        semesters.clear();
        resultCount.clear();
    }
}
