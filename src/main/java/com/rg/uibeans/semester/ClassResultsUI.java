package com.rg.uibeans.semester;

import java.util.List;

/**
 * Created by shreeshan on 01/07/17.
 */
public class ClassResultsUI {

    private Integer rank;

    private String studentName;

    private String studentUsn;

    private List<SubjectUI> subjects;

    private Integer studentTotal;

    private String resultClass;

    private Double studentAggregate;

    public Integer getRank() {
        return rank;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getStudentUsn() {
        return studentUsn;
    }

    public void setStudentUsn(String studentUsn) {
        this.studentUsn = studentUsn;
    }

    public List<SubjectUI> getSubjects() {
        return subjects;
    }

    public void setSubjects(List<SubjectUI> subjects) {
        this.subjects = subjects;
    }

    public Integer getStudentTotal() {
        return studentTotal;
    }

    public void setStudentTotal(Integer studentTotal) {
        this.studentTotal = studentTotal;
    }

    public String getResultClass() {
        return resultClass;
    }

    public void setResultClass(String resultClass) {
        this.resultClass = resultClass;
    }

    public Double getStudentAggregate() {
        return studentAggregate;
    }

    public void setStudentAggregate(Double studentAggregate) {
        this.studentAggregate = studentAggregate;
    }
}
