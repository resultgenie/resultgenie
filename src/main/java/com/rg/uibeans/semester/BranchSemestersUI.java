package com.rg.uibeans.semester;

import java.util.List;

/**
 * Created by shreeshan on 21/07/17.
 */
public class BranchSemestersUI {

    private String branchName;

    private List<String> semesters;

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public List<String> getSemesters() {
        return semesters;
    }

    public void setSemesters(List<String> semesters) {
        this.semesters = semesters;
    }
}
