package com.rg.uibeans.student;

import java.util.List;

/**
 * Created by shreeshan on 11/09/17.
 */
public class StudentResultUI {


    private List<StudentSubjects> studentSubjectsList;

    private String semester;

    private Integer totalMarks;

    private String resultClass;

    private float aggregate;

    private Integer maxTotalMarks;

    public Integer getMaxTotalMarks() {
        return maxTotalMarks;
    }

    public void setMaxTotalMarks(Integer maxTotalMarks) {
        this.maxTotalMarks = maxTotalMarks;
    }

    public List<StudentSubjects> getStudentSubjectsList() {
        return studentSubjectsList;
    }

    public void setStudentSubjectsList(List<StudentSubjects> studentSubjectsList) {
        this.studentSubjectsList = studentSubjectsList;
    }

    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    public Integer getTotalMarks() {
        return totalMarks;
    }

    public void setTotalMarks(Integer totalMarks) {
        this.totalMarks = totalMarks;
    }

    public String getResultClass() {
        return resultClass;
    }

    public void setResultClass(String resultClass) {
        this.resultClass = resultClass;
    }

    public float getAggregate() {
        return aggregate;
    }

    public void setAggregate(float aggregate) {
        this.aggregate = aggregate;
    }
}
