package com.rg.uibeans.student;

import com.rg.domain.data.aggregateentities.StudentPlacementBean;

/**
 * Created by shreeshan on 22/07/17.
 */
public class PlacementHighlighterUI {

    private Integer eligibleCandidates;
    private Integer totalCandidates;
    private StudentPlacementBean bestStudent;
    private Integer totalBranches;
    private Double collegePerformance;
    private Double changeInCollegePerformance;
    private Double rgScore;
    private Double changeInRgScore;

    public Double getChangeInCollegePerformance() {
        return changeInCollegePerformance;
    }

    public void setChangeInCollegePerformance(Double changeInCollegePerformance) {
        this.changeInCollegePerformance = changeInCollegePerformance;
    }

    public Double getChangeInRgScore() {
        return changeInRgScore;
    }

    public void setChangeInRgScore(Double changeInRgScore) {
        this.changeInRgScore = changeInRgScore;
    }

    public Integer getEligibleCandidates() {
        return eligibleCandidates;
    }

    public void setEligibleCandidates(Integer eligibleCandidates) {
        this.eligibleCandidates = eligibleCandidates;
    }

    public Integer getTotalCandidates() {
        return totalCandidates;
    }

    public void setTotalCandidates(Integer totalCandidates) {
        this.totalCandidates = totalCandidates;
    }

    public StudentPlacementBean getBestStudent() {
        return bestStudent;
    }

    public void setBestStudent(StudentPlacementBean bestStudent) {
        this.bestStudent = bestStudent;
    }

    public Integer getTotalBranches() {
        return totalBranches;
    }

    public void setTotalBranches(Integer totalBranches) {
        this.totalBranches = totalBranches;
    }

    public Double getCollegePerformance() {
        return collegePerformance;
    }

    public void setCollegePerformance(Double collegePerformance) {
        this.collegePerformance = collegePerformance;
    }

    public Double getRgScore() {
        return rgScore;
    }

    public void setRgScore(Double rgScore) {
        this.rgScore = rgScore;
    }
}
