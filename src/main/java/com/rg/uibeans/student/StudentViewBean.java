package com.rg.uibeans.student;

import com.rg.domain.data.aggregateentities.BacklogStudentScores;

import java.util.List;

/**
 * Created by shreeshan on 28/05/17.
 */
public class StudentViewBean {

    private SubjectScore bestScore;

    private SubjectScore leastScore;

    private Double aggregate;

    private float changeInAggregate;

    private Integer branchRank;

    private Double changeInBranchRank;

    private Integer collegeRank;

    private Double changeInCollegeRank;

    private String studentZone;

    private String changeInStudentZone;

    private StudentDetails studentDetails;

    private List<StudentResultUI> studentResultUIList;

    private List<BacklogStudentScores> backlogSubjects;

    public List<BacklogStudentScores> getBacklogSubjects() {
        return backlogSubjects;
    }

    public void setBacklogSubjects(List<BacklogStudentScores> backlogSubjects) {
        this.backlogSubjects = backlogSubjects;
    }

    public String getChangeInStudentZone() {
        return changeInStudentZone;
    }

    public void setChangeInStudentZone(String changeInStudentZone) {
        this.changeInStudentZone = changeInStudentZone;
    }

    public Double getChangeInBranchRank() {
        return changeInBranchRank;
    }

    public void setChangeInBranchRank(Double changeInBranchRank) {
        this.changeInBranchRank = changeInBranchRank;
    }

    public Double getChangeInCollegeRank() {
        return changeInCollegeRank;
    }

    public void setChangeInCollegeRank(Double changeInCollegeRank) {
        this.changeInCollegeRank = changeInCollegeRank;
    }

    public float getChangeInAggregate() {
        return changeInAggregate;
    }

    public void setChangeInAggregate(float changeInAggregate) {
        this.changeInAggregate = changeInAggregate;
    }

    public SubjectScore getBestScore() {
        return bestScore;
    }

    public void setBestScore(SubjectScore bestScore) {
        this.bestScore = bestScore;
    }

    public SubjectScore getLeastScore() {
        return leastScore;
    }

    public void setLeastScore(SubjectScore leastScore) {
        this.leastScore = leastScore;
    }

    public Double getAggregate() {
        return aggregate;
    }

    public void setAggregate(Double aggregate) {
        this.aggregate = aggregate;
    }

    public Integer getBranchRank() {
        return branchRank;
    }

    public void setBranchRank(Integer branchRank) {
        this.branchRank = branchRank;
    }

    public Integer getCollegeRank() {
        return collegeRank;
    }

    public void setCollegeRank(Integer collegeRank) {
        this.collegeRank = collegeRank;
    }

    public String getStudentZone() {
        return studentZone;
    }

    public void setStudentZone(String studentZone) {
        this.studentZone = studentZone;
    }

    public StudentDetails getStudentDetails() {
        return studentDetails;
    }

    public void setStudentDetails(StudentDetails studentDetails) {
        this.studentDetails = studentDetails;
    }

    public List<StudentResultUI> getStudentResultUIList() {
        return studentResultUIList;
    }

    public void setStudentResultUIList(List<StudentResultUI> studentResultUIList) {
        this.studentResultUIList = studentResultUIList;
    }
}
