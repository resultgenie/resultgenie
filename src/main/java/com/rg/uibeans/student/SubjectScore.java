package com.rg.uibeans.student;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Created by shreeshan on 28/05/17.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SubjectScore {

    private Object score;

    private Object subjectName;

    private Object studentName;

    private Object changeInScore;

    private Object maxMarks;

    public Object getMaxMarks() {
        return maxMarks;
    }

    public void setMaxMarks(Object maxMarks) {
        this.maxMarks = maxMarks;
    }

    public Object getChangeInScore() {
        return changeInScore;
    }

    public void setChangeInScore(Object changeInScore) {
        this.changeInScore = changeInScore;
    }

    public Object getScore() {

        return score;
    }

    public void setScore(Object score) {
        this.score = score;
    }

    public Object getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(Object subjectName) {
        this.subjectName = subjectName;
    }

    public Object getStudentName() {
        return studentName;
    }

    public void setStudentName(Object studentName) {
        this.studentName = studentName;
    }


    public void clear() {
        this.score = 0;
        this.subjectName = null;
        this.studentName = null;

    }
}
