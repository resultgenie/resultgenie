package com.rg.uibeans.student;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by shreeshan on 18/07/17.
 */
public class PlacementCriteriaBean {

    private List<String> branches = new ArrayList<>();

    private List<String> semesters = new ArrayList<>();

    private Double aggregate;

    private Integer noActiveBacklog;

    public Integer getNoActiveBacklog() {
        return noActiveBacklog;
    }

    public void setNoActiveBacklog(Integer noActiveBacklog) {
        this.noActiveBacklog = noActiveBacklog;
    }

    public List<String> getBranches() {
        return branches;
    }

    public void setBranches(List<String> branches) {
        this.branches = branches;
    }

    public List<String> getSemesters() {
        return semesters;
    }

    public void setSemesters(List<String> semesters) {
        this.semesters = semesters;
    }

    public Double getAggregate() {
        return aggregate;
    }

    public void setAggregate(Double aggregate) {
        this.aggregate = aggregate;
    }
}
