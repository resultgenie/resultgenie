package com.rg.uibeans.student;

/**
 * Created by shreeshan on 11/09/17.
 */
public class StudentSubjects {

    private Integer id;

    private String subjectName;

    private String subjectCode;

    private Integer externalMarks;

    private Integer internalMarks;

    private Integer totalMarks;

    private String subjectResultClass;

    private Boolean isRequiredForRadar;

    private Integer internalMaxMarks;

    private Integer externalMaxMarks;

    public Integer getInternalMaxMarks() {
        return internalMaxMarks;
    }

    public void setInternalMaxMarks(Integer internalMaxMarks) {
        this.internalMaxMarks = internalMaxMarks;
    }

    public Integer getExternalMaxMarks() {
        return externalMaxMarks;
    }

    public void setExternalMaxMarks(Integer externalMaxMarks) {
        this.externalMaxMarks = externalMaxMarks;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getSubjectCode() {
        return subjectCode;
    }

    public void setSubjectCode(String subjectCode) {
        this.subjectCode = subjectCode;
    }

    public Integer getExternalMarks() {
        return externalMarks;
    }

    public void setExternalMarks(Integer externalMarks) {
        this.externalMarks = externalMarks;
    }

    public Integer getInternalMarks() {
        return internalMarks;
    }

    public void setInternalMarks(Integer internalMarks) {
        this.internalMarks = internalMarks;
    }

    public Integer getTotalMarks() {
        return totalMarks;
    }

    public void setTotalMarks(Integer totalMarks) {
        this.totalMarks = totalMarks;
    }

    public String getSubjectResultClass() {
        return subjectResultClass;
    }

    public void setSubjectResultClass(String subjectResultClass) {
        this.subjectResultClass = subjectResultClass;
    }

    public Boolean getRequiredForRadar() {
        return isRequiredForRadar;
    }

    public void setRequiredForRadar(Boolean requiredForRadar) {
        isRequiredForRadar = requiredForRadar;
    }
}
