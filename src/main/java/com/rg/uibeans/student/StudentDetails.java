package com.rg.uibeans.student;

/**
 * Created by shreeshan on 28/05/17.
 */
public class StudentDetails {

    private String studentName;

    private String studentType;

    private String studentUsn;

    private String studentCurrentSemester;

    private String studentBranch;

    private Integer studentCurrentYear;

    private Integer studentBacklogs;

    private Double studentRatings;

    private String studentStatus;

    public String getStudentStatus() {
        return studentStatus;
    }

    public void setStudentStatus(String studentStatus) {
        this.studentStatus = studentStatus;
    }

    public String getStudentUsn() {
        return studentUsn;
    }

    public void setStudentUsn(String studentUsn) {
        this.studentUsn = studentUsn;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getStudentType() {
        return studentType;
    }

    public void setStudentType(String studentType) {
        this.studentType = studentType;
    }

    public String getStudentCurrentSemester() {
        return studentCurrentSemester;
    }

    public void setStudentCurrentSemester(String studentCurrentSemester) {
        this.studentCurrentSemester = studentCurrentSemester;
    }

    public String getStudentBranch() {
        return studentBranch;
    }

    public void setStudentBranch(String studentBranch) {
        this.studentBranch = studentBranch;
    }

    public Integer getStudentCurrentYear() {
        return studentCurrentYear;
    }

    public void setStudentCurrentYear(Integer studentCurrentYear) {
        this.studentCurrentYear = studentCurrentYear;
    }

    public Integer getStudentBacklogs() {
        return studentBacklogs;
    }

    public void setStudentBacklogs(Integer studentBacklogs) {
        this.studentBacklogs = studentBacklogs;
    }

    public Double getStudentRatings() {
        return studentRatings;
    }

    public void setStudentRatings(Double studentRatings) {
        this.studentRatings = studentRatings;
    }
}
