package com.rg.uibeans.student;

import org.springframework.stereotype.Component;

/**
 * Created by shreeshan on 03/06/17.
 */
@Component
public class PercentageChangeBean {

    private Object currentState;

    private Object changeInState;

    public Object getCurrentState() {
        return currentState;
    }

    public void setCurrentState(Object currentState) {
        this.currentState = currentState;
    }

    public Object getChangeInState() {
        return changeInState;
    }

    public void setChangeInState(Object changeInState) {
        this.changeInState = changeInState;
    }
}
