package com.rg.uibeans.student;

import com.rg.domain.data.aggregateentities.StudentPlacementBean;

import java.util.List;
import java.util.Map;

/**
 * Created by shreeshan on 22/07/17.
 */
public class PlacementUIBean {

    private PlacementHighlighterUI highlighter;

    private List<StudentPlacementBean> eligibleStudents;

    private Map<String, Integer> classDistribution;

    private Map<String, Integer> branchDistribution;

    public PlacementHighlighterUI getHighlighter() {
        return highlighter;
    }

    public void setHighlighter(PlacementHighlighterUI highlighter) {
        this.highlighter = highlighter;
    }

    public List<StudentPlacementBean> getEligibleStudents() {
        return eligibleStudents;
    }

    public void setEligibleStudents(List<StudentPlacementBean> eligibleStudents) {
        this.eligibleStudents = eligibleStudents;
    }

    public Map<String, Integer> getClassDistribution() {
        return classDistribution;
    }

    public void setClassDistribution(Map<String, Integer> classDistribution) {
        this.classDistribution = classDistribution;
    }

    public Map<String, Integer> getBranchDistribution() {
        return branchDistribution;
    }

    public void setBranchDistribution(Map<String, Integer> branchDistribution) {
        this.branchDistribution = branchDistribution;
    }
}
