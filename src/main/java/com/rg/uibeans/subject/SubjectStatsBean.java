package com.rg.uibeans.subject;

import com.rg.domain.data.aggregateentities.BacklogStudentScores;
import com.rg.domain.data.aggregateentities.StudentScores;
import com.rg.domain.data.aggregateentities.SubjectResultsHistory;
import com.rg.uibeans.student.SubjectScore;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by shreeshan on 29/05/17.
 */
public class SubjectStatsBean {

    private SubjectScore bestScore;

    private SubjectScore leastScore;

    private SubjectScore subjectDifficulty;

    private Integer subjectRank;

    private Integer subjectRankChange;

    private Integer subjectBacklogs;

    private Integer subjectBacklogsChange;

    private String subjectZone;

    private String subjectZoneChange;

    private NavigableMap<String, Integer> subjectResultRangesDistribution = new TreeMap<>();

    private HashMap<String, AtomicInteger> subjectResultPercentageDistribution = new HashMap<>();

    private List<StudentScores> subjectTopScores = new ArrayList<>();

    private List<SubjectResultsHistory> subjectPerformancePlotData = new ArrayList<>();

    private List<StudentScores> currentSemesterSubjectResult = new ArrayList<>();

    private List<BacklogStudentScores> currentSemesterStudentWithBacklogs = new ArrayList<>();

    public SubjectScore getBestScore() {
        return bestScore;
    }

    public void setBestScore(SubjectScore bestScore) {
        this.bestScore = bestScore;
    }

    public SubjectScore getLeastScore() {
        return leastScore;
    }

    public void setLeastScore(SubjectScore leastScore) {
        this.leastScore = leastScore;
    }

    public SubjectScore getSubjectDifficulty() {
        return subjectDifficulty;
    }

    public void setSubjectDifficulty(SubjectScore subjectDifficulty) {
        this.subjectDifficulty = subjectDifficulty;
    }

    public Integer getSubjectRank() {
        return subjectRank;
    }

    public void setSubjectRank(Integer subjectRank) {
        this.subjectRank = subjectRank;
    }

    public Integer getSubjectRankChange() {
        return subjectRankChange;
    }

    public void setSubjectRankChange(Integer subjectRankChange) {
        this.subjectRankChange = subjectRankChange;
    }

    public Integer getSubjectBacklogs() {
        return subjectBacklogs;
    }

    public void setSubjectBacklogs(Integer subjectBacklogs) {
        this.subjectBacklogs = subjectBacklogs;
    }

    public Integer getSubjectBacklogsChange() {
        return subjectBacklogsChange;
    }

    public void setSubjectBacklogsChange(Integer subjectBacklogsChange) {
        this.subjectBacklogsChange = subjectBacklogsChange;
    }

    public String getSubjectZone() {
        return subjectZone;
    }

    public void setSubjectZone(String subjectZone) {
        this.subjectZone = subjectZone;
    }

    public String getSubjectZoneChange() {
        return subjectZoneChange;
    }

    public void setSubjectZoneChange(String subjectZoneChange) {
        this.subjectZoneChange = subjectZoneChange;
    }

    public List<StudentScores> getSubjectTopScores() {
        return subjectTopScores;
    }

    public void setSubjectTopScores(List<StudentScores> subjectTopScores) {
        this.subjectTopScores = subjectTopScores;
    }

    public List<SubjectResultsHistory> getSubjectPerformancePlotData() {
        return subjectPerformancePlotData;
    }

    public void setSubjectPerformancePlotData(List<SubjectResultsHistory> subjectPerformancePlotData) {
        this.subjectPerformancePlotData = subjectPerformancePlotData;
    }

    public List<StudentScores> getCurrentSemesterSubjectResult() {
        return currentSemesterSubjectResult;
    }

    public void setCurrentSemesterSubjectResult(List<StudentScores> currentSemesterSubjectResult) {
        this.currentSemesterSubjectResult = currentSemesterSubjectResult;
    }

    public List<BacklogStudentScores> getCurrentSemesterStudentWithBacklogs() {
        return currentSemesterStudentWithBacklogs;
    }

    public void setCurrentSemesterStudentWithBacklogs(List<BacklogStudentScores> currentSemesterStudentWithBacklogs) {
        this.currentSemesterStudentWithBacklogs = currentSemesterStudentWithBacklogs;
    }

    public HashMap<String, AtomicInteger> getSubjectResultPercentageDistribution() {
        return subjectResultPercentageDistribution;
    }

    public void setSubjectResultPercentageDistribution(HashMap<String, AtomicInteger>
                                                               subjectResultPercentageDistribution) {
        this.subjectResultPercentageDistribution = subjectResultPercentageDistribution;
    }

    public NavigableMap<String, Integer> getSubjectResultRangesDistribution() {
        return subjectResultRangesDistribution;
    }

    public void setSubjectResultRangesDistribution(NavigableMap<String, Integer> subjectResultRangesDistribution) {
        this.subjectResultRangesDistribution = subjectResultRangesDistribution;
    }
}
