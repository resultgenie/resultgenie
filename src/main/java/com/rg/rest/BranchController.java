package com.rg.rest;

import com.rg.service.intf.BranchService;
import com.rg.utils.Response;
import com.rg.utils.StatusCodes;
import com.rg.utils.StringConstants;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Branch related API's are listed below
 * Created by shreeshan on 26/04/17.
 */
@Api(value = StringConstants.BASE_API_URL + "branch/", description = "Branch related API's are listed below")
@RestController
@RequestMapping(StringConstants.BASE_API_URL + "branch/")
public class BranchController {

    @Autowired
    BranchService bService;


    /**
     * Search branch based on branch name
     *
     * @param branchName
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "Search branch based on branch name", nickname = "branch search")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "branchName", value = "Branch you want to search", required = true, dataType =
                    "string", paramType = "pathvariable")
    })
    @ApiResponses(value = {@ApiResponse(code = StatusCodes.SUCCESS, message = StringConstants.BRANCHES_JSON),
            @ApiResponse(code = StatusCodes.REQUIRED_DATA_NOT_FOUND, message = StringConstants.REQUIRED_DATA_NOT_FOUND),
            @ApiResponse(code = StatusCodes.SERVER_ERROR, message = StringConstants.SERVER_ERROR)})
    @RequestMapping(value = "/search/{branchName}", method = RequestMethod.GET)
    public ResponseEntity<Response> searchBranch(@PathVariable String branchName) throws Exception {
        try {
            return bService.searchBranch(branchName);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }


    @RequestMapping(value = "/get", method = RequestMethod.GET)
    public ResponseEntity<Response> getBranches() throws Exception {
        try {
            return bService.getBranches();
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }


    /**
     * Returns branch wise results for even(2,4,6,8)/odd(1,3,5,7) semesters
     *
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "Returns sem wise results for even(2,4,6,8)/odd(1,3,5,7) semesters")
    @ApiResponses(value = {@ApiResponse(code = StatusCodes.SUCCESS, message = StringConstants.BRANCH_RESULTS),
            @ApiResponse(code = StatusCodes.NOT_FOUND_IN_DB_OR_FAILED_TO_RETRIEVE, message = StringConstants
                    .ERROR_IN_BRANCHWISE_RESULT_CALC),
            @ApiResponse(code = StatusCodes.SERVER_ERROR, message = StringConstants.SERVER_ERROR)})
    @RequestMapping(value = "/semwise/results", method = RequestMethod.GET)
    public ResponseEntity<Response> getSemWiseResults() throws Exception {
        try {
            return bService.getSemWiseResults();
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }


    /**
     * Number of students who have passed in the latest announced results
     * Returns branch wise results for even(2,4,6,8)/odd(1,3,5,7) semesters
     *
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "Returns sem wise results for even(2,4,6,8)/odd(1,3,5,7) semesters")
    @ApiResponses(value = {@ApiResponse(code = StatusCodes.SUCCESS, message = StringConstants.BRANCH_RESULTS),
            @ApiResponse(code = StatusCodes.NOT_FOUND_IN_DB_OR_FAILED_TO_RETRIEVE, message = StringConstants
                    .ERROR_IN_BRANCHWISE_RESULT_CALC),
            @ApiResponse(code = StatusCodes.SERVER_ERROR, message = StringConstants.SERVER_ERROR)})
    @RequestMapping(value = "/semwise/pass", method = RequestMethod.GET)
    public ResponseEntity<Response> getSemWisePassCount() throws Exception {
        try {
            return bService.getSemWisePassCount();
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }


}
