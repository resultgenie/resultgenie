package com.rg.rest;

import com.rg.domain.credentials.User;
import com.rg.domain.data.College;
import com.rg.service.intf.WelcomeService;
import com.rg.utils.Response;
import com.rg.utils.StatusCodes;
import com.rg.utils.StringConstants;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by shreeshan on 09/04/17.
 */
@Api(value = StringConstants.BASE_API_URL+"noauth/", description = "Contains API endpoints which do not require Authentication")
@RestController
@RequestMapping(StringConstants.BASE_API_URL+"noauth/")
public class WelcomeController {

    @Autowired
    private WelcomeService wService;


    @ApiOperation(value = "User Register API", nickname = "user register")
    @RequestMapping(value = "/userRegister", method = RequestMethod.POST)
    @ApiResponses(value = {@ApiResponse(code = StatusCodes.SUCCESS, message = StringConstants.REGISTRATION_SUCCESSFUL),
            @ApiResponse(code =  StatusCodes.NOT_FOUND_IN_DB_OR_FAILED_TO_RETRIEVE, message = StringConstants.NO_COLLEGE_FOR_KEY),
            @ApiResponse(code =  StatusCodes.REQUIRED_DATA_NOT_FOUND, message = StringConstants.REQUIRED_DATA_NOT_FOUND),
            @ApiResponse(code = StatusCodes.SERVER_ERROR, message = StringConstants.SERVER_ERROR)})
    public ResponseEntity<Response> userRegister(@RequestBody User user) throws Exception {
        try {
            return wService.userRegister(user);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }


    @ApiOperation(value = "API to setup new college", nickname = "college setup")
    @ApiResponses(value = {@ApiResponse(code = StatusCodes.SUCCESS, message = StringConstants.COLLEGE_SETUP_SUCCESSFUL),
            @ApiResponse(code =  StatusCodes.REQUIRED_DATA_NOT_FOUND, message = StringConstants.REQUIRED_DATA_NOT_FOUND),
            @ApiResponse(code =  StatusCodes.DATA_ALREADY_EXISTS, message = StringConstants.COLLEGE_EXISTS),
            @ApiResponse(code = StatusCodes.SERVER_ERROR, message = StringConstants.SERVER_ERROR)})
    @RequestMapping(value = "/collegesetup", method = RequestMethod.POST)
    public ResponseEntity<Response> setupCollege(@RequestBody College college) throws Exception {
        try {
            return wService.setupCollege(college);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }


    @ApiOperation(value = "API to check if user is logged in - based on username")
    @ApiResponses(value = {@ApiResponse(code = StatusCodes.SUCCESS, message = StringConstants.LOGGED_IN),
            @ApiResponse(code = StatusCodes.SERVER_ERROR, message = StringConstants.SERVER_ERROR)})
    @RequestMapping(value = "/check", method = RequestMethod.GET)
    public ResponseEntity<Response> userLoginCheck(@RequestParam("eid") String username,HttpServletRequest httpRequest,
                                                   HttpServletResponse httpResponse) throws Exception {
        try {
            return wService.userLoginCheck(username,httpRequest,httpResponse);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    @ApiOperation(value = "API to check if username is taken")
    @ApiResponses(value = {@ApiResponse(code = StatusCodes.SUCCESS, message = StringConstants.USERNAME_AVAILABLE),
            @ApiResponse(code = StatusCodes.SERVER_ERROR, message = StringConstants.SERVER_ERROR)})
    @RequestMapping(value = "/name/{username}", method = RequestMethod.GET)
    public ResponseEntity<Response> usernameCheck(@PathVariable String username) throws Exception {
        try {
            return wService.usernameCheck(username);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    @ApiOperation(value = "API to check if email ID is taken")
    @ApiResponses(value = {@ApiResponse(code = StatusCodes.SUCCESS, message = StringConstants.EMAIL_ID_AVALAIBLE),
            @ApiResponse(code = StatusCodes.SERVER_ERROR, message = StringConstants.SERVER_ERROR)})
    @RequestMapping(value = "/email", method = RequestMethod.GET)
    public ResponseEntity<Response> emailCheck(@RequestParam("id") String email) throws Exception {
        try {
            return wService.emailIdCheck(email);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }


//    @ApiOperation(value = "Login API", nickname = "tenant login")
//    @RequestMapping(value = "/login", method = RequestMethod.POST)
//    @ApiResponses(value = {@ApiResponse(code = 200, message = "login successfull"),
//            @ApiResponse(code = 444, message = "Required parameters not found"),
//            @ApiResponse(code = 404, message = "User doesn't exist"),
//            @ApiResponse(code = 500, message = "Internal server error")})
//    public ResponseEntity<Response> login(@RequestBody Tenant tenant) throws Exception {
//        try {
//            return wService.login(tenant);
//        } catch (Exception e) {
//            e.printStackTrace();
//            throw e;
//        }
//    }
//
//
//    @ApiOperation(value = "User Login API", nickname = "user login")
//    @RequestMapping(value = "/userlogin", method = RequestMethod.POST)
//    @ApiResponses(value = {@ApiResponse(code = 200, message = "login successfull"),
//            @ApiResponse(code = 444, message = "Required parameters not found"),
//            @ApiResponse(code = 404, message = "User doesn't exist"),
//            @ApiResponse(code = 500, message = "Internal server error")})
//    public ResponseEntity<Response> userLogin(@RequestBody User user) throws Exception {
//        try {
//            return wService.userLogin(user);
//        } catch (Exception e) {
//            e.printStackTrace();
//            throw e;
//        }
//    }


}
