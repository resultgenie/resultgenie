package com.rg.rest;

import com.rg.service.intf.SubjectService;
import com.rg.utils.Response;
import com.rg.utils.StatusCodes;
import com.rg.utils.StringConstants;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Created by shreeshan on 26/04/17.
 */
@Api(value = StringConstants.BASE_API_URL + "subject/", description = "Subject related API's are listed below")
@RestController
@RequestMapping(StringConstants.BASE_API_URL + "subject/")
public class SubjectController {

    @Autowired
    SubjectService subjectService;

    @ApiOperation(value = "Search subject based on name/subjectcode", nickname = "subject search")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "queryString", value = "Name or Code of the subject you want to search", required = true, dataType = "string", paramType = "pathvariable")
    })
    @ApiResponses(value = {@ApiResponse(code = StatusCodes.SUCCESS, message = StringConstants.SUBJECTS_JSON),
            @ApiResponse(code = StatusCodes.REQUIRED_DATA_NOT_FOUND, message = StringConstants.REQUIRED_DATA_NOT_FOUND),
            @ApiResponse(code = StatusCodes.SERVER_ERROR, message = StringConstants.SERVER_ERROR)})
    @RequestMapping(value = "/search/{queryString}", method = RequestMethod.GET)
    public ResponseEntity<Response> searchSubject(@PathVariable String queryString) throws Exception {
        try {
            return subjectService.searchSubject(queryString);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    @ApiOperation(value = "Get Subject Stats required for subject view")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Branch Abbreviation", value = "Add branch abbreviation to get information of a particular subject", required = true, dataType = "string", paramType = "pathparam"),
            @ApiImplicitParam(name = "SubjectCode", value = "Add subject code to get information of a particular subject", required = true, dataType = "string", paramType = "pathparam")
    })
    @ApiResponses(value = {@ApiResponse(code = StatusCodes.SUCCESS, message = StringConstants.SUBJECT_STATS),
            @ApiResponse(code = StatusCodes.REQUIRED_DATA_NOT_FOUND, message = StringConstants.REQUIRED_DATA_NOT_FOUND),
            @ApiResponse(code = StatusCodes.NOT_FOUND_IN_DB_OR_FAILED_TO_RETRIEVE, message = StringConstants.NO_SUBJECT),
            @ApiResponse(code = StatusCodes.NOT_FOUND_IN_DB_OR_FAILED_TO_RETRIEVE, message = StringConstants.NO_SUBJECT_STATS_FOR_CODE),
            @ApiResponse(code = StatusCodes.NOT_FOUND_IN_DB_OR_FAILED_TO_RETRIEVE, message = StringConstants.FAILED_TO_RETRIEVE_SUBJECT_RANK_WRT_COLLEGE),
            @ApiResponse(code = StatusCodes.NOT_FOUND_IN_DB_OR_FAILED_TO_RETRIEVE, message = StringConstants.FAILED_TO_RETRIEVE_SUBJECT_RANK_WRT_SEMESTER),
            @ApiResponse(code = StatusCodes.NOT_FOUND_IN_DB_OR_FAILED_TO_RETRIEVE, message = StringConstants.FAILED_TO_RETRIEVE_SUBJECT_RANK_WRT_BRANCH),
            @ApiResponse(code = StatusCodes.NOT_FOUND_IN_DB_OR_FAILED_TO_RETRIEVE, message = StringConstants.FAILED_TO_CALC_SUBJECT_AVG_CURR_SEM),
            @ApiResponse(code = StatusCodes.NOT_FOUND_IN_DB_OR_FAILED_TO_RETRIEVE, message = StringConstants.FAILED_TO_CALC_SUBJECT_AVG_PREV_SEM),
            @ApiResponse(code = StatusCodes.NOT_FOUND_IN_DB_OR_FAILED_TO_RETRIEVE, message = StringConstants.FAILED_TO_CALC_SUBJECT_BACKLOG_COUNT_CURR_SEM),
            @ApiResponse(code = StatusCodes.NOT_FOUND_IN_DB_OR_FAILED_TO_RETRIEVE, message = StringConstants.FAILED_TO_CALC_SUBJECT_BACKLOG_COUNT_PREV_SEM),
            @ApiResponse(code = StatusCodes.NOT_FOUND_IN_DB_OR_FAILED_TO_RETRIEVE, message = StringConstants.FAILED_TO_CALC_SUBJECT_RANK_CURR_SEM),
            @ApiResponse(code = StatusCodes.NOT_FOUND_IN_DB_OR_FAILED_TO_RETRIEVE, message = StringConstants.FAILED_TO_CALC_SUBJECT_RANK_PREV_SEM),
            @ApiResponse(code = StatusCodes.SERVER_ERROR, message = StringConstants.SERVER_ERROR)})
    @RequestMapping(value = "/{branchAbb}/{subjectCode}", method = RequestMethod.GET)
    public ResponseEntity<Response> subjectStats(@PathVariable String subjectCode,@PathVariable String branchAbb) throws Exception {
        try {
            return subjectService.subjectStats(subjectCode,branchAbb);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    @ApiOperation(value = "Get Subject Details required for subject view(Card view)")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Branch Abbreviation", value = "Add branch abbreviation to get information of a particular subject", required = true, dataType = "string", paramType = "pathparam"),
            @ApiImplicitParam(name = "SubjectCode", value = "Add subject code to get information of a particular subject", required = true, dataType = "string", paramType = "pathparam")
    })
    @ApiResponses(value = {@ApiResponse(code = StatusCodes.SUCCESS, message = StringConstants.SUBJECT_STATS),
            @ApiResponse(code = StatusCodes.REQUIRED_DATA_NOT_FOUND, message = StringConstants.REQUIRED_DATA_NOT_FOUND),
            @ApiResponse(code = StatusCodes.NOT_FOUND_IN_DB_OR_FAILED_TO_RETRIEVE, message = StringConstants.NO_SUBJECT),
            @ApiResponse(code = StatusCodes.NOT_FOUND_IN_DB_OR_FAILED_TO_RETRIEVE, message = StringConstants.NO_SUBJECT_STATS_FOR_CODE),
            @ApiResponse(code = StatusCodes.NOT_FOUND_IN_DB_OR_FAILED_TO_RETRIEVE, message = StringConstants.FAILED_TO_RETRIEVE_SUBJECT_RANK_WRT_COLLEGE),
            @ApiResponse(code = StatusCodes.NOT_FOUND_IN_DB_OR_FAILED_TO_RETRIEVE, message = StringConstants.FAILED_TO_RETRIEVE_SUBJECT_RANK_WRT_SEMESTER),
            @ApiResponse(code = StatusCodes.NOT_FOUND_IN_DB_OR_FAILED_TO_RETRIEVE, message = StringConstants.FAILED_TO_RETRIEVE_SUBJECT_RANK_WRT_BRANCH),
            @ApiResponse(code = StatusCodes.SERVER_ERROR, message = StringConstants.SERVER_ERROR)})
    @RequestMapping(value = "/details/{branchAbb}/{subjectCode}", method = RequestMethod.GET)
    public ResponseEntity<Response> subjectDetails(@PathVariable String subjectCode,@PathVariable String branchAbb) throws Exception {
        try {
            return subjectService.subjectDetails(subjectCode,branchAbb);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }


}
