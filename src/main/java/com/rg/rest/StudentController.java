package com.rg.rest;

import com.rg.service.intf.StudentService;
import com.rg.uibeans.student.PlacementCriteriaBean;
import com.rg.utils.Response;
import com.rg.utils.StatusCodes;
import com.rg.utils.StringConstants;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Api(value = "/student/", description = "Student related API's are listed below")
@RestController
@RequestMapping(StringConstants.BASE_API_URL+"student/")
public class StudentController {

    @Autowired
    private StudentService studentService;

    @Autowired
    Environment env;


    @ApiOperation(value = "Fetch Result API <from VTU server>", nickname = "get results")
    @RequestMapping(value = "/fetchResult/{collegeId}", method = RequestMethod.GET)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "collegeId", value = "College ID to fetch results from", required = true, dataType = "string", paramType = "pathvariable")
    })
    @ApiResponses(value = {@ApiResponse(code = StatusCodes.SUCCESS, message = StringConstants.RESULTS_SAVED_IN_DB,response = Response.class),
            @ApiResponse(code = StatusCodes.REQUIRED_DATA_NOT_FOUND, message = StringConstants.NO_STUDENT),
            @ApiResponse(code = StatusCodes.SERVER_ERROR, message = StringConstants.SERVER_ERROR)})
    public ResponseEntity<Response> fetchResult(@PathVariable String collegeId) throws Exception {
        try {
            return studentService.fetchResult(collegeId);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }


    @ApiOperation(value = "Student stats", nickname = "student statistics")
    @RequestMapping(value = "/studentStat/{studentName}", method = RequestMethod.GET)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "studentName", value = "Name of the student", required = true, dataType = "string", paramType = "pathvariable")
    })
    @ApiResponses(value = {@ApiResponse(code = StatusCodes.SUCCESS, message = StringConstants.STUDENT_STATS),
            @ApiResponse(code =  StatusCodes.REQUIRED_DATA_NOT_FOUND, message = StringConstants.REQUIRED_DATA_NOT_FOUND),
            @ApiResponse(code =  StatusCodes.NOT_FOUND_IN_DB_OR_FAILED_TO_RETRIEVE, message = StringConstants.NO_STUDENT),
            @ApiResponse(code = StatusCodes.SERVER_ERROR, message = StringConstants.SERVER_ERROR)})
    public ResponseEntity<Response> getStudentStat(@PathVariable String studentName) throws Exception {
        try {
            return studentService.getStudentStat(studentName);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }


    @RequestMapping(value = "/search/{querystring}", method = RequestMethod.GET)
    @ApiOperation(value = "Search student based on name/USN", nickname = "student search")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "querystring", value = "Student you want to search based on either their name or USN", required = true, dataType = "string", paramType = "pathvariable")
    })
    @ApiResponses(value = {@ApiResponse(code = StatusCodes.SUCCESS, message = StringConstants.STUDENTS_JSON),
            @ApiResponse(code =  StatusCodes.REQUIRED_DATA_NOT_FOUND, message = StringConstants.REQUIRED_DATA_NOT_FOUND),
            @ApiResponse(code = StatusCodes.SERVER_ERROR, message = StringConstants.SERVER_ERROR)})
    public ResponseEntity<Response> searchStudent(@PathVariable("querystring") String querystring) throws Exception {
        try {
            return studentService.searchStudent(querystring);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    @RequestMapping(value = "/{usn}", method = RequestMethod.GET)
    @ApiOperation(value = "Student view Data")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "usn", value = "Enter USN of the student whose statistics is required", required = true, dataType = "string", paramType = "pathvariable")
    })
    @ApiResponses(value = {@ApiResponse(code = StatusCodes.SUCCESS, message = StringConstants.STUDENT_VIEW_DATA),
            @ApiResponse(code = StatusCodes.NOT_FOUND_IN_DB_OR_FAILED_TO_RETRIEVE, message = StringConstants.NO_STUDENT),
            @ApiResponse(code = StatusCodes.NOT_FOUND_IN_DB_OR_FAILED_TO_RETRIEVE, message = StringConstants.NO_STUDENT_STATS),
            @ApiResponse(code = StatusCodes.NOT_FOUND_IN_DB_OR_FAILED_TO_RETRIEVE, message = StringConstants.NO_STUDENT_OVERALL_STATS),
            @ApiResponse(code = StatusCodes.NOT_FOUND_IN_DB_OR_FAILED_TO_RETRIEVE, message = StringConstants.FAILED_TO_RETRIEVE_BRANCH_RANK_PREV_SEM),
            @ApiResponse(code = StatusCodes.NOT_FOUND_IN_DB_OR_FAILED_TO_RETRIEVE, message = StringConstants.FAILED_TO_RETRIEVE_SEM_AGGREGATE_PREV_SEM),
            @ApiResponse(code = StatusCodes.NOT_FOUND_IN_DB_OR_FAILED_TO_RETRIEVE, message = StringConstants.FAILED_TO_CALC_COLLEGE_RANK_CURR_SEM),
            @ApiResponse(code = StatusCodes.NOT_FOUND_IN_DB_OR_FAILED_TO_RETRIEVE, message = StringConstants.FAILED_TO_CALC_COLLEGE_RANK_PREV_SEM),
            @ApiResponse(code = StatusCodes.NOT_FOUND_IN_DB_OR_FAILED_TO_RETRIEVE, message = StringConstants.FAILED_TO_CALC_BACKLOG_PREV_SEM),
            @ApiResponse(code = StatusCodes.SERVER_ERROR, message = StringConstants.SERVER_ERROR)})
    public ResponseEntity<Response> getStudent(@PathVariable String usn) throws Exception {
        try {
            return studentService.getStudent(usn);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    @RequestMapping(value = "/ranking/{year}/{semester}", method = RequestMethod.GET)
    @ApiOperation(value = "Get students with their ranks with respect to given year and semester")
    @ApiResponses(value = {@ApiResponse(code = StatusCodes.SUCCESS, message = StringConstants.STUDENTS_JSON),
            @ApiResponse(code = StatusCodes.SERVER_ERROR, message = StringConstants.SERVER_ERROR)})
    public ResponseEntity<Response> getStudentsWithRankAcrossBranches(@PathVariable String year,@PathVariable String semester) throws Exception {
        try {
            return studentService.getStudentsWithRankAcrossBranches(year,semester);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    @RequestMapping(value = "/placements", method = RequestMethod.POST)
    @ApiOperation(value = "Get students who satisfy the placement criteria")
    @ApiResponses(value = {@ApiResponse(code = StatusCodes.SUCCESS, message = StringConstants.STUDENTS_JSON),
            @ApiResponse(code = StatusCodes.SERVER_ERROR, message = StringConstants.SERVER_ERROR)})
    public ResponseEntity<Response> getStudentsForPlacements(@RequestBody PlacementCriteriaBean placementCriteriaBean) throws Exception {
        try {
            return studentService.getStudentsForPlacements(placementCriteriaBean);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }






//    @RequestMapping(value = "/student/dashboard/", method = RequestMethod.GET)
//    @ApiOperation(value = "Get total number of distinction students in the college for the current running semesters")
//    @ApiResponses(value = {@ApiResponse(code = StatusCodes.SUCCESS, message = StringConstants.DISTINCTION_STUDENTS_COUNT),
//            @ApiResponse(code = StatusCodes.SERVER_ERROR, message = StringConstants.SERVER_ERROR)})
//    public ResponseEntity<Response> distinctionStudentCount(@PathVariable float distinctionThreshold) throws Exception {
//        try {
//            return studentService.distinctionStudentCount(distinctionThreshold);
//        } catch (Exception e) {
//            e.printStackTrace();
//            throw e;
//        }
//    }
//









//  @ApiOperation(value = "Student stats", nickname = "student statistics")
//  @RequestMapping(value = "/search/{text}", method = RequestMethod.GET)
//  @ApiImplicitParams({
//      @ApiImplicitParam(name = "Text you want to search(StudentName/USN)", value = "Name/USN of the student", required = true, dataType = "string", paramType = "pathvairable")
//    })
//  @ApiResponses( value = { @ApiResponse( code = 200,  message ="Student Object" ) ,
//  		@ApiResponse( code = 444,  message ="Required parameters not found" ),
//  		@ApiResponse( code = 500,  message ="Internal server error" )} )
//  public ResponseEntity<Response> search(@PathVariable String text) throws Exception {
//      try {
//          return studentService.search(text);
//      } catch (Exception e) {
//          e.printStackTrace();
//          throw e;
//      }
//  }
//
//  @RequestMapping(value = "/names", method = RequestMethod.GET)
//  public ResponseEntity<Response> autoCompleteFeature() throws Exception {
//      try {
//          return studentService.autoCompleteFeature();
//      } catch (Exception e) {
//          e.printStackTrace();
//          throw e;
//      }
//  }

}
