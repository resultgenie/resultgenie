package com.rg.rest;

import com.rg.service.intf.CollegeService;
import com.rg.utils.Response;
import com.rg.utils.StatusCodes;
import com.rg.utils.StringConstants;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * College related API's are listed below
 * Created by shreeshan on 28/05/17.
 */
@Api(value = StringConstants.BASE_API_URL + "college/", description = "College related API's are listed below")
@RestController
@RequestMapping(StringConstants.BASE_API_URL + "college/")
public class CollegeController {

    @Autowired
    CollegeService cService;


    /**
     * This API returns the college of the logged in user
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "This API returns the college of the logged in user")
    @ApiResponses(value = {@ApiResponse(code = StatusCodes.SUCCESS, message = StringConstants.COLLEGE_JSON),
            @ApiResponse(code = StatusCodes.SERVER_ERROR, message = StringConstants.SERVER_ERROR)})
    @RequestMapping(value = "/get", method = RequestMethod.GET)
    public ResponseEntity<Response> getCollege() throws Exception {
        try {
            return cService.getCollege();
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }


}
