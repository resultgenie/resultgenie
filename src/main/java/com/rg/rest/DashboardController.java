package com.rg.rest;

import com.rg.service.intf.DashboardService;
import com.rg.utils.Response;
import com.rg.utils.StatusCodes;
import com.rg.utils.StringConstants;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by shreeshan on 16/06/17.
 */
@Api(value = StringConstants.BASE_API_URL + "dashboard/", description = "Data for the Dashboard is supplied with these API's")
@RestController
@RequestMapping(StringConstants.BASE_API_URL + "dashboard/")
public class DashboardController {

    @Autowired
    DashboardService dService;


    /**
     * API that serves data for highlighters in dashboard
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "API that serves data for highlighters in dashboard")
    @ApiResponses(value = {@ApiResponse(code = StatusCodes.SUCCESS, message = StringConstants.DASHBOARD_HIGHLIGHTER_DATA),
            @ApiResponse(code = StatusCodes.NOT_FOUND_IN_DB_OR_FAILED_TO_RETRIEVE, message = StringConstants.NO_STUDENTS),
            @ApiResponse(code = StatusCodes.NOT_FOUND_IN_DB_OR_FAILED_TO_RETRIEVE, message = StringConstants.FAILED_TO_CALCULATE_COLLEGE_PERFORMANCE_CURRENT_SEM),
            @ApiResponse(code = StatusCodes.NOT_FOUND_IN_DB_OR_FAILED_TO_RETRIEVE, message = StringConstants.FAILED_TO_CALCULATE_COLLEGE_PERFORMANCE_PREV_SEM),
            @ApiResponse(code = StatusCodes.NOT_FOUND_IN_DB_OR_FAILED_TO_RETRIEVE, message = StringConstants.FAILED_TO_CALC_RG_SCORE_CURRENT_SEM),
            @ApiResponse(code = StatusCodes.NOT_FOUND_IN_DB_OR_FAILED_TO_RETRIEVE, message = StringConstants.FAILED_TO_CALC_RG_SCORE_PREV_SEM),
            @ApiResponse(code = StatusCodes.NOT_FOUND_IN_DB_OR_FAILED_TO_RETRIEVE, message = StringConstants.FAILED_TO_CALC_DISTINCTION_STUDENTS_COUNT),
            @ApiResponse(code = StatusCodes.NOT_FOUND_IN_DB_OR_FAILED_TO_RETRIEVE, message = StringConstants.FAILED_TO_CALC_DISTINCTION_STUDENTS_COUNT_PREV_SEM),
            @ApiResponse(code = StatusCodes.SERVER_ERROR, message = StringConstants.SERVER_ERROR)})
    @RequestMapping(value = "/highlighter", method = RequestMethod.GET)
    public ResponseEntity<Response> getDashboardHighlighterData() throws Exception {
        try {
            return dService.getDashboardHighlighterData();
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }




    /**
     * API that returns list of all possible branches
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "API that returns list of all possible branches", nickname = "branches list")
    @ApiResponses(value = {@ApiResponse(code = StatusCodes.SUCCESS, message = StringConstants.BRANCH_LIST),
            @ApiResponse(code = StatusCodes.SERVER_ERROR, message = StringConstants.SERVER_ERROR)})
    @RequestMapping(value = "/branches/list", method = RequestMethod.GET)
    public ResponseEntity<Response> getBranchList() throws Exception {
        try {
            return dService.getBranchList();
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }



    /**
     * Gets the count of branches
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "Gets the count of branches")
    @ApiResponses(value = {@ApiResponse(code = StatusCodes.SUCCESS, message = StringConstants.BRANCH_LIST),
            @ApiResponse(code = StatusCodes.SERVER_ERROR, message = StringConstants.SERVER_ERROR)})
    @RequestMapping(value = "/branches/count", method = RequestMethod.GET)
    public ResponseEntity<Response> getBranchCount() throws Exception {
        try {
            return dService.getBranchCount();
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }


    /**
     * How students are split across FCD,First Class,Second Class and Fail categories in the latest semester results
     * Returns branch wise result count for different categories(FCD,First Class,Second Class and Fail) of the latest semester results
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "Returns branch wise result count for different categories(FCD,First Class,Second Class and Fail) of the latest semester results")
    @ApiResponses(value = {@ApiResponse(code = StatusCodes.SUCCESS, message = StringConstants.BRANCH_RESULTS),
            @ApiResponse(code = StatusCodes.SERVER_ERROR, message = StringConstants.SERVER_ERROR)})
    @RequestMapping(value = "/branchwise/categories/count", method = RequestMethod.GET)
    public ResponseEntity<Response> getBranchWiseStudentResultCountsAcrossCategories() throws Exception {
        try {
            return dService.getBranchWiseStudentResultCountsAcrossCategories();
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }


    /**
     * Returns what is the pass percentages of each running(latest result) semester across different branches
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "Returns what is the pass percentages of each running(latest result) semester across different branches")
    @ApiResponses(value = {@ApiResponse(code = StatusCodes.SUCCESS, message = StringConstants.BRANCH_RESULTS),
            @ApiResponse(code = StatusCodes.SERVER_ERROR, message = StringConstants.SERVER_ERROR)})
    @RequestMapping(value = "/branchwise/pass", method = RequestMethod.GET)
    public ResponseEntity<Response> getBranchWisePassPercentages() throws Exception {
        try {
            return dService.getBranchWisePassPercentages();
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

}
