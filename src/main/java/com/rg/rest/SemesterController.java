package com.rg.rest;

import com.rg.service.intf.SemesterService;
import com.rg.utils.Response;
import com.rg.utils.StatusCodes;
import com.rg.utils.StringConstants;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by shreeshan on 26/04/17.
 */
@Api(value = StringConstants.BASE_API_URL+"semester/", description = "Semester related API's are listed below")
@RestController
@RequestMapping(StringConstants.BASE_API_URL+"semester/")
public class SemesterController {

    @Autowired
    private SemesterService sService;

    /**
     * API that serves complete students result for a given branch and a semester
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "API that serves complete students result for a given branch and a semester")
    @ApiResponses(value = {@ApiResponse(code = StatusCodes.SUCCESS, message = StringConstants.CLASS_RESULTS),
            @ApiResponse(code = StatusCodes.SERVER_ERROR, message = StringConstants.SERVER_ERROR)})
    @RequestMapping(value = "/class/{year}/{branchAbb}/{semesterValue}", method = RequestMethod.GET)
    public ResponseEntity<Response> getWholeClassResults(@PathVariable String year,@PathVariable String branchAbb,@PathVariable String semesterValue) throws Exception {
        try {
            return sService.getWholeClassResults(year,branchAbb,semesterValue);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }


    /**
     * This API serves the combination of available year,semester and branches
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "This API serves the combination of available year,semester and branches")
    @ApiResponses(value = {@ApiResponse(code = StatusCodes.SUCCESS, message = StringConstants.AVAILABLE_SEMESTERS_YEARS_BRANCHES),
            @ApiResponse(code = StatusCodes.SERVER_ERROR, message = StringConstants.SERVER_ERROR)})
    @RequestMapping(value = "/getcomb/{includeBranch}", method = RequestMethod.GET)
    public ResponseEntity<Response> getCombinationOfSemestersYearsBranches(@PathVariable Boolean includeBranch) throws Exception {
        try {
            return sService.getCombinationOfSemestersYearsBranches(includeBranch);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }







}
