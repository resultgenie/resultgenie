drop database if exists rg_tenant_credentials;
create database rg_tenant_credentials;
use rg_tenant_credentials;

drop table if exists `tenant`;
create table tenant(
  t_id varchar(255),
  t_url varchar(255),
  t_username varchar(255),
  t_password varchar(255),
  t_login_username varchar(255),
  t_login_password varchar(255),
  t_key varchar(255),
  PRIMARY KEY (t_id)
);

drop table if exists `branches`;
create table `branches` (
  `branch_id` int(11) NOT NULL,
  `branch_fullname` varchar(255) NOT NULL,
  `branch_abb` varchar(65) NOT NULL,
  PRIMARY KEY (`branch_id`)
);

INSERT INTO `branches` VALUES (1,'Ceramics and Cement Technology','cc'),(2,'Civil Engineering','cv'),(3,'Environmental Engineering','ev'),(4,'Aeronautical Engineering','ae'),(5,'Mechanical Engineering','me'),(6,'Mining Engineering','mi'),(7,'Electronics and Communication Engineering','ec'),(8,'Telecommunication Engineering','te'),(9,'Electrical and Electronics Engineering','ee'),(10,'Computer Science and Engineering','cs'),(11,'Information Science and Engineering','is'),(12,'Industrial and Production Engineering','ip'),(13,'Industrial Engineering and Management','im'),(14,'Instrumentation Technology','it'),(15,'Medical Electronics','ml'),(16,'Biotechnology','bt'),(17,'Chemical Engineering','ch'),(18,'Textile Technology','tx'),(19,'Automobile Engineering','au'),(20,'BioMedical Engineering','bm'),(21,'Construction Technology and Management','ct'),(22,'Mechatronics Engineering','mt'),(23,'Architecture','at');


drop table if exists `user`;
create table `user` (
  `u_user_id` int(11) NOT NULL AUTO_INCREMENT,
  `u_password` varchar(255) DEFAULT NULL,
  `u_email` varchar(255) DEFAULT NULL,
  `u_name` VARCHAR(45) NOT NULL ,
  `u_enabled` TINYINT NOT NULL DEFAULT 1,
  `u_username` varchar(255) DEFAULT NULL,
  `u_frn_t_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`u_user_id`),
  FOREIGN KEY (`u_frn_t_id`) REFERENCES `tenant` (`t_id`)
);

drop table if exists `role`;
create table `role`(
  `r_role_id` int(11) NOT NULL AUTO_INCREMENT,
  `r_role` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`r_role_id`)
);


INSERT INTO `role` values(1,'USER'),(2,'ADMIN');

drop table if exists `user_role`;
create table `user_role`(
  `u_user_id` int(11) NOT NULL,
  `r_role_id` int(11) NOT NULL ,
  FOREIGN KEY (`u_user_id`) REFERENCES `user` (`u_user_id`),
  FOREIGN KEY (`r_role_id`) REFERENCES `role` (`r_role_id`)
);