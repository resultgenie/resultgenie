drop table if exists `branch`;
create table `branch`(
  `b_id` int(11) NOT NULL AUTO_INCREMENT,
  `b_branch_value` varchar(255) DEFAULT NULL,
  `b_branch_intake` varchar(255) DEFAULT NULL,
  `b_branch_abbreviation` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`b_id`)
);

drop table if exists `college`;
create table `college`(
  `c_id` int(11) NOT NULL AUTO_INCREMENT,
  `c_college_code` varchar(255) DEFAULT NULL,
  `c_college_name` varchar(255) DEFAULT NULL,
  `c_college_abbreviation` varchar(255) DEFAULT NULL,
  `c_college_email` varchar(255) DEFAULT NULL,
  `c_college_description` varchar(255) DEFAULT NULL,
  `c_college_region` varchar(255) DEFAULT NULL,
  `c_registered_datetime` bigint DEFAULT NULL,
  `c_key` varchar(255) DEFAULT NULL,
  PRIMARY KEY(`c_id`)
);

drop table if exists `college_branch`;
create table `college_branch`(
  `bc_frn_c_id` int(11) NOT NULL,
  `bc_frn_b_id` int(11) NOT NULL,
  FOREIGN KEY(`bc_frn_c_id`) REFERENCES `college`(`c_id`),
  FOREIGN KEY(`bc_frn_b_id`) REFERENCES `branch`(`b_id`)
);

drop table if exists `semester`;
create table `semester`(
  `sm_id` int(11) NOT NULL AUTO_INCREMENT,
  `sm_semester_value` varchar(255) DEFAULT NULL,
  PRIMARY KEY(`sm_id`)
);

drop table if exists `subject`;
create table `subject`(
  `s_id` int(11) NOT NULL AUTO_INCREMENT,
  `s_subject_code` varchar(255) DEFAULT NULL,
  `s_subject_name` varchar(255) DEFAULT NULL,
  `s_is_lab` int(11) DEFAULT NULL,
  `s_is_regular_subject` int(11) DEFAULT NULL,
  `s_external_max_marks` int(11) DEFAULT NULL,
  `s_internal_max_marks` int(11) DEFAULT NULL,
  `s_is_required_for_aggregate_calc` int(11) DEFAULT NULL,
  `s_frn_b_id` int(11) DEFAULT NULL,
  `s_frn_sm_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`s_id`),
  FOREIGN KEY (`s_frn_sm_id`) REFERENCES `semester` (`sm_id`),
  FOREIGN KEY (`s_frn_b_id`) REFERENCES `branch` (`b_id`)
);

drop table if exists `branch_subject`;
create table `branch_subject`(
  `bs_frn_s_id` int(11) NOT NULL,
  `bs_frn_b_id` int(11) NOT NULL,
  FOREIGN KEY(`bs_frn_b_id`) REFERENCES `branch`(`b_id`),
  FOREIGN KEY(`bs_frn_s_id`) REFERENCES `subject`(`s_id`)
);

drop table if exists `student`;
create table `student`(
  `st_id` int(11) NOT NULL AUTO_INCREMENT,
  `st_student_usn` varchar(255) DEFAULT NULL,
  `st_student_name` varchar(255) DEFAULT NULL,
  `st_joining_year` VARCHAR(255) DEFAULT NULL,
  `st_is_active` int(11) DEFAULT NULL,
  `st_frn_b_id` int(11) DEFAULT NULL,
  `st_frn_c_id` int(11) DEFAULT NULL,
  PRIMARY KEY(`st_id`),
  FOREIGN KEY(`st_frn_b_id`) REFERENCES `branch`(`b_id`),
  FOREIGN KEY(`st_frn_c_id`) REFERENCES `college`(`c_id`)
);

drop table if exists `student_result`;
create table `student_result`(
  `sr_id` int(11) NOT NULL AUTO_INCREMENT,
  `sr_backlog_count` int(11) DEFAULT NULL,
  `sr_result_class` varchar(255) DEFAULT NULL,
  `sr_sem_aggregate` FLOAT DEFAULT NULL,
  `sr_total_score` int(11) DEFAULT NULL,
  `sr_result_month` bigint DEFAULT NULL,
  `sr_total_max_marks` int(11) DEFAULT NULL,
  `sr_frn_sm_id` int(11) DEFAULT NULL,
  `sr_frn_st_id` int(11) DEFAULT NULL,
  PRIMARY KEY(`sr_id`),
  FOREIGN KEY(`sr_frn_sm_id`) REFERENCES `semester`(`sm_id`),
  FOREIGN KEY(`sr_frn_st_id`) REFERENCES `student`(`st_id`)
);


drop table if exists `subject_wise_ranking`;
create table `subject_wise_ranking`(
  `swr_id` int(11) NOT NULL AUTO_INCREMENT,
  `swr_subject_name` VARCHAR(255) DEFAULT NULL,
  `swr_branch` varchar(255) DEFAULT NULL,
  `swr_subject_code` varchar(255) DEFAULT NULL,
  `swr_subject_performance` FLOAT DEFAULT NULL,
  `swr_total_subjects_in_college` int(11) DEFAULT NULL,
  `swr_rank_in_college` int(11) DEFAULT NULL,
  `swr_total_subjects_in_semester` int(11) DEFAULT NULL,
  `swr_rank_in_semester` int(11) DEFAULT NULL,
  `swr_total_subjects_in_branch` int(11) DEFAULT NULL,
  `swr_rank_in_branch` int(11) DEFAULT NULL,
  PRIMARY KEY(`swr_id`)
);

drop table if exists `subject_stats`;
create table `subject_stats`(
  `sbs_id` int(11) NOT NULL AUTO_INCREMENT,
  `sbs_highest_marks` int(11) DEFAULT NULL,
  `sbs_least_marks` int(11) DEFAULT NULL,
  `sbs_change_in_highest_marks` FLOAT DEFAULT NULL,
  `sbs_change_in_least_marks` FLOAT DEFAULT NULL,
  `sbs_frn_s_id` int(11) DEFAULT NULL,
  `sbs_frn_st_id_highest_scorer` int(11) DEFAULT NULL,
  `sbs_frn_st_id_least_scorer` int(11) DEFAULT NULL,
  `sbs_frn_b_id` int(11) DEFAULT  NULL,
  PRIMARY KEY(`sbs_id`),
  FOREIGN KEY (`sbs_frn_s_id`) REFERENCES `subject`(`s_id`),
  FOREIGN KEY (`sbs_frn_st_id_highest_scorer`) REFERENCES `student`(`st_id`),
  FOREIGN KEY (`sbs_frn_st_id_least_scorer`) REFERENCES `student`(`st_id`)
);

drop table if exists `student_stats`;
create table `student_stats`(
  `ss_id` int(11) NOT NULL AUTO_INCREMENT,
  `ss_aggregate` FLOAT DEFAULT NULL,
  `ss_current_ranking` int(11) DEFAULT NULL,
  `ss_current_year` int(11) DEFAULT NULL,
  `ss_student_zone` varchar(255) DEFAULT NULL,
  `ss_best_score` FLOAT DEFAULT NULL,
  `ss_worst_score` FLOAT DEFAULT NULL,
  `ss_frn_s_id_best_score_subject` int(11) DEFAULT NULL,
  `ss_frn_b_id` int(11) DEFAULT NULL,
  `ss_frn_sr_id_current` int(11) DEFAULT NULL,
  `ss_frn_sm_id` int(11) DEFAULT NULL,
  `ss_frn_st_id` int(11) DEFAULT NULL,
  `ss_frn_s_id_worst_score_subject` int(11) DEFAULT NULL,
  PRIMARY KEY (`ss_id`),
  FOREIGN KEY (`ss_frn_s_id_best_score_subject`) REFERENCES `subject` (`s_id`),
  FOREIGN KEY (`ss_frn_b_id`) REFERENCES `branch` (`b_id`),
  FOREIGN KEY (`ss_frn_st_id`) REFERENCES `student` (`st_id`),
  FOREIGN KEY (`ss_frn_sr_id_current`) REFERENCES `student_result` (`sr_id`),
  FOREIGN KEY (`ss_frn_s_id_worst_score_subject`) REFERENCES `subject` (`s_id`),
  FOREIGN KEY (`ss_frn_sm_id`) REFERENCES `semester` (`sm_id`)
);

drop table if exists `student_overall_stats`;
create table `student_overall_stats`(
  `sos_id` int(11) NOT NULL AUTO_INCREMENT,
  `sos_overall_best_score` FLOAT DEFAULT NULL,
  `sos_overall_worst_score` FLOAT DEFAULT NULL,
  `sos_change_in_worst_score` FLOAT DEFAULT NULL,
  `sos_change_in_best_score` FLOAT DEFAULT NULL,
  `sos_frn_s_id_overall_best_score_subject` int(11) DEFAULT NULL,
  `sos_frn_st_id` int(11) DEFAULT NULL,
  `sos_frn_s_id_overall_worst_score_subject` int(11) DEFAULT NULL,
  PRIMARY KEY (`sos_id`),
  FOREIGN KEY (`sos_frn_s_id_overall_best_score_subject`) REFERENCES `subject` (`s_id`),
  FOREIGN KEY (`sos_frn_st_id`) REFERENCES `student` (`st_id`),
  FOREIGN KEY (`sos_frn_s_id_overall_worst_score_subject`) REFERENCES `subject` (`s_id`)
);

drop table if exists `theory_practical`;
create table `theory_practical` (
  `tp_id` int(11) NOT NULL AUTO_INCREMENT,
  `tp_external_score` int(11) DEFAULT NULL,
  `tp_internal_score` int(11) DEFAULT NULL,
  `tp_total_score` int(11) DEFAULT NULL,
  `tp_is_practical` tinyint(1) DEFAULT NULL,
  `tp_result_class` varchar(255) DEFAULT NULL,
  `tp_frn_b_id` int(11) DEFAULT NULL,
  `tp_frn_sm_id` int(11) DEFAULT NULL,
  `tp_frn_s_id` int(11) DEFAULT NULL,
  `tp_frn_sr_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`tp_id`),
  FOREIGN KEY (`tp_frn_sm_id`) REFERENCES `semester` (`sm_id`),
  FOREIGN KEY (`tp_frn_b_id`) REFERENCES `branch` (`b_id`),
  FOREIGN KEY (`tp_frn_sr_id`) REFERENCES `student_result` (`sr_id`),
  FOREIGN KEY (`tp_frn_s_id`) REFERENCES `subject` (`s_id`)
);


drop table if exists `year_data`;
create table `year_data` (
  `yd_id` int(11) NOT NULL AUTO_INCREMENT,
  `yd_year_value` int(11) DEFAULT NULL,
  PRIMARY KEY (`yd_id`)
);

DROP TABLE IF EXISTS `year_data_semester`;
CREATE TABLE `year_data_semester` (
  `year_data_yd_id` int(11) NOT NULL,
  `semester_sm_id` int(11) NOT NULL,
  FOREIGN KEY (`year_data_yd_id`) REFERENCES `year_data` (`yd_id`),
  FOREIGN KEY (`semester_sm_id`) REFERENCES `semester` (`sm_id`)
);